#!/bin/bash
set -euo pipefail

BASE=cosmo
# artifact for ICFP20:
#BASE=artifact11-source
# artifact for ICFP21:
BASE=icfp21-paper23-artifact-source

ARCHIVE=$BASE.tgz

rm -rf $ARCHIVE

# Anonymize our opam file.
# This sed command should work under both MacOS and Unix.
#sed -e '/^maintainer:/d ; /^authors:/d ; /http/d' -i.bak opam

# Create an archive.
find theories/ -type f -name '*.v' -exec \
  tar --transform "s,^,$BASE/," --show-transformed -czvf $ARCHIVE \
    Readme.md LICENSE opam Makefile _CoqProject {} \+

# Add the MD5 to the archive name.
MD5=( $(md5sum "$ARCHIVE") )
mv "$ARCHIVE" "${ARCHIVE%.tgz}-$MD5.tgz"

# Restore our opam file.
#mv opam.bak opam
