(*** Timer ********************************************************************)

module Time = struct

  (* nanoseconds since the beginning of the program *)
  type t = int

  let get () : t =
    int_of_float (Sys.time () *. 1e9)

  let pp out (t : t) =
    Printf.fprintf out "%.3gs" (float t /. 1.e9)

  let timed (f : unit -> 'a) : 'a =
    let t0 = get () in
    let res = f () in
    let t1 = get () in
    Printf.fprintf stderr "time: %a\n%!" pp (t1 - t0) ;
    res

end

(*** Backoff ******************************************************************)

module type BACKOFF = sig
  type t
  val make : ?min:float -> ?max:float -> unit -> t
  val backoff : t -> unit
end

(* NOTE: This datastructure is not thread-safe. *)
module ExponentialBackoff : BACKOFF = struct
  type t = {
    min : float ;
    max : float ;
    mutable cur : float ;
  }
  (* 2^{−1074} is the smallest positive number that a float can represent.
   * 2^{-10} seconds is about a millisecond. *)
  let make ?(min=0x1.p-1074) ?(max=0x1.p-10) () =
    assert (0.0 <= min && min <= max) ;
    { min ; max ; cur = min }
  let backoff bo =
    let cur = bo.cur in
    bo.cur <- min bo.max (cur +. cur) ;
    Domain.Sync.cpu_relax () ;
    Unix.sleepf (Random.float cur)
end

(*** Nullable reference *******************************************************)

(* The type ['a OptionRef.t] is equivalent to ['a option ref] but more 
 * efficient, as the stored option is unboxed.
 * Note that the reference itself is still boxed, of course (hence, using the 
 * type `'a OptionRef.t OptionRef.t` is safe). *)
module type OPTIONREF = sig
  type 'a t
  val make_unset : unit -> 'a t
  val make_set : 'a -> 'a t
  val make_opt : 'a option -> 'a t
  val unset : 'a t -> unit
  val set : 'a t -> 'a -> unit
  val set_opt : 'a t -> 'a option -> unit
  exception Not_set
  val get : 'a t -> 'a
  val get_opt : 'a t -> 'a option
end

(* NOTE: This implementation is thread-safe. It achieves a relaxed memory 
 * semantics: `get` only returns values which have been written at some point. 
 * Of course, atomics can still be used to share views of the datastructure 
 * between threads.
 * NOTE: This implementation has no memory leak: no references to former values 
 * are kept. *)
module OptionRef_optimized : OPTIONREF = struct
  (* We encode the None value by a dummy pointer; it is important not to use an
   * immediate value, such as `Obj.magic 0`, because:
   * (1) if the contained type is immediate, it may shadow a value of the
   *     contained type;
   * (2) if the contained type is boxed, the compiler/runtime will expect the
   *     dummy value to be a pointer and may try to prefetch its target. *)
  (*! let none : 'a. 'a = Obj.magic (ref ()) !*)
  (* TODO: See whether this is necessary/enough:
   * a more complicated dummy value which ensures that prefetching is safe
   * (meaning that it will not segfault) up to an arbitrary depth, as long as we
   * never try to read further than the 8th field of a block: *)
  let none : 'a. 'a =
    let rec dum = `Dummy (dum, dum, dum, dum, dum, dum, dum, dum) in
    Obj.magic dum
  type 'a t = 'a ref
  let make_unset () =
    ref none
  let make_set x =
    ref x
  let make_opt opt_x =
    begin match opt_x with
    | None   -> make_unset ()
    | Some x -> make_set x
    end
  let unset r =
    r := none
  let set r x =
    r := x
  let set_opt r opt_x =
    begin match opt_x with
    | None   -> unset r
    | Some x -> set r x
    end
  exception Not_set
  let get r =
    let x = !r in
    if x == none then
      raise Not_set
    else
      x
  let get_opt r =
    let x = !r in
    if x == none then
      None
    else
      Some x
end

module OptionRef = OptionRef_optimized

(* TODO:
 *   - Also implement OptionArray to optimize `'a option array (factorize 
 *     OptionRef and OptionArray using a module UnsafeUnboxedOption?)
 *   - Implement atomic versions of OptionRef and OptionArray.
 *)

(*** Thread-local storage *****************************************************)

module type THREADLOCAL = sig
  type 'a t
  val make : unit -> 'a t
  val unset : 'a t -> unit
  val set : 'a t -> 'a -> unit
  exception Not_set
  val get : 'a t -> 'a
end

module ThreadLocal_AssocList : THREADLOCAL = struct
  type 'a t = (int * 'a OptionRef.t) list Atomic.t
  let make () =
    Atomic.make []
  let get_ref loc =
    List.assoc (Domain.self () :> int) (Atomic.get loc)
  let unset loc =
    begin match get_ref loc with
    | r                   -> OptionRef.unset r
    | exception Not_found -> ()
    end
  let set loc =
    begin match get_ref loc with
    | r                   -> OptionRef.set r
    | exception Not_found ->
        fun x ->
        let pair = ((Domain.self () :> int), OptionRef.make_set x) in
        while
          let hd = Atomic.get loc in
          not @@ Atomic.compare_and_set loc hd (pair :: hd)
        do Domain.Sync.cpu_relax () done
    end
  exception Not_set = OptionRef.Not_set
  let get loc =
    begin match get_ref loc with
    | r                   -> OptionRef.get r
    | exception Not_found -> raise Not_set
    end
end

module ThreadLocal_Array : THREADLOCAL = struct
  (* FIXME: This constant is guessed, Multicore OCaml should expose one. *)
  let tid_max = 1024
  (* NOTE: Cells are boxed. This implies one more indirection, but it makes the 
   * shared array read-only, which avoids invalidating other threads’ cache when 
   * one thread updates its own value.
   * However, all cells are allocated by the same thread, the one which called 
   * `make`. With the current (2019-01) behavior of the garbage collector, this 
   * means that whenever one thread accesses its own cell, the cell will be 
   * moved to the shared memory (major heap). Ideally, each cell would be kept 
   * in the local memory (minor heap) of the thread to which it belongs.
   * A better solution would unbox cells and use padding, so that each cell 
   * falls into a separate cache line (which spans 64 bytes in practice). *)
  type 'a t = 'a OptionRef.t array
  let make () =
    Array.init tid_max (fun _tid -> OptionRef.make_unset ())
  let unset loc =
    OptionRef.unset (Array.get loc (Domain.self () :> int))
  let set loc =
    OptionRef.set (Array.get loc (Domain.self () :> int))
  exception Not_set = OptionRef.Not_set
  let get loc =
    OptionRef.get (Array.get loc (Domain.self () :> int))
end

(*! module ThreadLocal = ThreadLocal_AssocList !*)
module ThreadLocal = ThreadLocal_Array

(*** Lock *********************************************************************)

module type LOCK = sig
  type t
  val make : unit -> t
  val acquire : t -> unit
  val release : t -> unit
end

let make_critical (module Lock : LOCK) () : (unit -> 'a) -> 'a =
  let lock = Lock.make () in
  fun f ->
    Lock.acquire lock ;
    begin match f () with
    | res         -> Lock.release lock ; res
    | exception e -> Lock.release lock ; raise e
    end

let critical_timer : Time.t Atomic.t = Atomic.make 0

let make_critical_timed (module Lock : LOCK) () : (unit -> 'a) -> 'a =
  let lock = Lock.make () in
  fun f ->
    let t0 = Time.get () in
    Lock.acquire lock ;
    begin match f () with
    | res         ->
        Lock.release lock ;
        Atomic.fetch_and_add critical_timer (Time.get () - t0) |> ignore ;
        res
    | exception e ->
        Lock.release lock ;
        Atomic.fetch_and_add critical_timer (Time.get () - t0) |> ignore ;
        raise e
    end

(* the test-and-set lock:
 * the most basic spin lock, CPU-intensive and provokes many cache misses. *)
module TASLock : LOCK = struct
  type t = bool Atomic.t
  let make () =
    Atomic.make false
  let acquire lock =
    while Atomic.exchange lock true do Domain.Sync.cpu_relax () done
  let release lock =
    assert (Atomic.exchange lock false)
end

(* the test-and-test-and-set lock:
 * has a much better cache behavior with many threads. *)
module TTASLock : LOCK = struct
  type t = bool Atomic.t
  let make () =
    Atomic.make false
  let acquire lock =
    while Atomic.get lock do Domain.Sync.cpu_relax () done ;
    while Atomic.exchange lock true do
      while Atomic.get lock do Domain.Sync.cpu_relax () done
    done
  let release lock =
    assert (Atomic.exchange lock false)
end

(* test-and-test-and-set lock with exponential backoff:
 * is even better, but has time constants which need manual adjustments. *)
module BackoffLock (Backoff : BACKOFF) : LOCK = struct
  type t = bool Atomic.t
  let make () =
    Atomic.make false
  let acquire lock =
    let bo = Backoff.make () in
    while Atomic.get lock do Domain.Sync.cpu_relax () done ;
    while Atomic.exchange lock true do
      Backoff.backoff bo ;
      while Atomic.get lock do Domain.Sync.cpu_relax () done
    done
  let release lock =
    assert (Atomic.exchange lock false)
end

module QueueLock : LOCK = struct
  type flag = bool Atomic.t
  type t = {
    last_flag : flag Atomic.t ;
    my_flag : flag ThreadLocal.t ;
  }
  let make () =
    {
      last_flag = Atomic.make (Atomic.make true) ;
      my_flag = ThreadLocal.make () ;
    }
  let acquire lock =
    let my_flag = Atomic.make false in
    ThreadLocal.set lock.my_flag my_flag ;
    let prev_flag = Atomic.exchange lock.last_flag my_flag in
    while not @@ Atomic.get prev_flag do Domain.Sync.cpu_relax () done
  let release lock =
    let my_flag = ThreadLocal.get lock.my_flag in
    Atomic.set my_flag true ;
    ThreadLocal.unset lock.my_flag
end

module SignalLock : LOCK = struct
  type cell = Domain.id Atomic.t
  type t = {
    last_cell : cell Atomic.t ;
    my_cell : cell ThreadLocal.t ;
  }
  let null : 'a. 'a =
    Obj.magic (Some ())
  let is_null (x : 'a) : bool =
    x == null
  let make () =
    {
      last_cell = Atomic.make null ;
      my_cell = ThreadLocal.make () ;
    }
  (* TODO: check that notify/wait has release/acquire semantics with respect to
   * views; this is needed in order for the lock to provide this semantics. *)
  let acquire lock =
    let my_cell = Atomic.make null in
    ThreadLocal.set lock.my_cell my_cell ;
    let prev_cell = Atomic.exchange lock.last_cell my_cell in
    if not @@ is_null prev_cell then
      Domain.Sync.critical_section begin fun () ->
        Atomic.set prev_cell (Domain.self ()) ;
        Domain.Sync.wait ()
      end
  let release lock =
    let my_cell = ThreadLocal.get lock.my_cell in
    ThreadLocal.unset lock.my_cell ;
    if Atomic.compare_and_set lock.last_cell my_cell null then
      assert (is_null (Atomic.get my_cell))
    else begin
      (* this busy-wait loop should be short, with few contention: *)
      while is_null (Atomic.get my_cell) do Domain.Sync.cpu_relax () done ;
      Domain.Sync.notify (Atomic.get my_cell)
    end
end

(*** Shared initializable cell ************************************************)

module Initializable : sig
  type (-_, 'a) t
  type 'a rw_t = ([`r|`w], 'a) t
  type 'a ro_t =([`r], 'a) t
  val make : unit -> 'a rw_t
  val init : ([>`w], 'a) t -> 'a -> unit
  val get : ([>`r], 'a) t -> 'a
end
=
struct
  type (_, 'a) t = 'a option Atomic.t
  type 'a rw_t = ([`r|`w], 'a) t
  type 'a ro_t =([`r], 'a) t
  let make () =
    Atomic.make None
  let init cell x =
    (*! assert (Atomic.get cell = None) ; !*)
    Atomic.set cell (Some x)
  let rec get cell =
    begin match Atomic.get cell with
    | None   -> Domain.Sync.cpu_relax () ; get cell
    | Some x -> x
    end
end

(*** Lock-free stack **********************************************************)

module type LOCKFREESTACK = sig
  type 'a t
  val make : unit -> 'a t
  val push : 'a t -> 'a -> unit
  val pop : 'a t -> 'a option
  val peek : 'a t -> 'a option
  val iter : 'a t -> f:('a -> unit) -> unit
end

module TreiberStack : LOCKFREESTACK = struct

  type 'a t = 'a list Atomic.t

  let make () =
    Atomic.make []

  let push st x =
    let bo = ExponentialBackoff.make () in
    let rec push st x =
      let hd = Atomic.get st in
      if not @@ Atomic.compare_and_set st hd (x :: hd) then begin
        ExponentialBackoff.backoff bo ;
        push st x
      end
    in
    push st x

  let pop st =
    let bo = ExponentialBackoff.make () in
    let rec pop st =
      begin match Atomic.get st with
      | [] ->
          None
      | (x :: tl) as hd ->
          if Atomic.compare_and_set st hd tl then
            Some x
          else begin
            ExponentialBackoff.backoff bo ;
            pop st
          end
      end
    in
    pop st

  let peek st =
    begin match Atomic.get st with
    | []      -> None
    | x :: tl -> Some x
    end

  let iter st ~f =
    List.iter f (Atomic.get st)

end

module MyStack : LOCKFREESTACK = struct

  type 'a nodes =
  | Nil
  | Cons of {
      element : 'a ;
      next : 'a nodes Initializable.ro_t ;
    }

  type 'a t = 'a nodes Atomic.t

  let make () =
    Atomic.make Nil

  let rec push st element =
    let next = Initializable.make () in
    let new_hd = Cons { element ; next = (next :> _ Initializable.ro_t) } in
    let old_hd = Atomic.exchange st new_hd in
    Initializable.init next old_hd

  let pop st =
    let bo = ExponentialBackoff.make () in
    let rec pop st =
      begin match Atomic.get st with
      | Nil ->
          None
      | Cons node as hd ->
          if Atomic.compare_and_set st hd (Initializable.get node.next) then
            Some node.element
          else begin
            ExponentialBackoff.backoff bo ;
            pop st
          end
      end
    in
    pop st

  let peek st =
    begin match Atomic.get st with
    | Nil       -> None
    | Cons node -> Some node.element
    end

  let iter st ~f =
    let rec iter_nodes nodes =
      begin match nodes with
      | Nil       -> ()
      | Cons node -> f node.element ; iter_nodes (Initializable.get node.next)
      end
    in
    iter_nodes (Atomic.get st)

end

(*** Queue ********************************************************************)

module type CONDITION = sig
  type t
  val wait : t -> unit
  val signal : t -> unit
  val signal_all : t -> unit
end

module type GENERICCONDITION = sig
  include CONDITION
  type lock
  val make : lock -> t
end

module GenericCondition (Lock : LOCK) : GENERICCONDITION with type lock := Lock.t = struct
  type t =
    {
      lock : Lock.t ;
      mutable wait_queue : Domain.id list ;
    }
  let make lock =
    { lock ; wait_queue = [] }
  let wait cond =
    (*! assert (Lock.owns cond.lock) ; !*)
    let tid = Domain.self () in
    cond.wait_queue <- tid :: cond.wait_queue ;
    Domain.Sync.critical_section begin fun () ->
      Lock.release cond.lock ;
      Domain.Sync.wait () ;
    end ;
    Lock.acquire cond.lock
  let signal cond =
    (*! assert (Lock.owns cond.lock) ; !*)
    begin match cond.wait_queue with
    | [] ->
        ()
    | tid :: queue ->
        cond.wait_queue <- queue ;
        Domain.Sync.notify tid
    end
  let signal_all cond =
    (*! assert (Lock.owns cond.lock) ; !*)
    let tids = cond.wait_queue in
    cond.wait_queue <- [] ;
    List.iter Domain.Sync.notify tids
end

module Condition = GenericCondition (QueueLock)

(*
module type CONDITIONLOCK = sig
  include LOCK
  module Condition : CONDITION
  val make_cond : t -> Condition.t
end

module ConditionLock : CONDITIONLOCK = struct
  type t = unit
end
*)

module type QUEUE = sig
  type 'a t
  val make : capacity:int -> dummy:'a -> 'a t
  val try_enqueue : 'a t -> 'a -> bool
  val enqueue : 'a t -> 'a -> unit
  val try_dequeue : 'a t -> 'a option
  val dequeue : 'a t -> 'a
end


module UnboundedQueue : QUEUE = struct

  (*! module Lock = QueueLock !*) (* ← segfault because of this impl. (+ wrong impl. of ThreadLocal) *)
  module Lock = TTASLock
  module Condition = GenericCondition (Lock)

  type 'a node =
  | Nil
  | Cons of {
      element : 'a ;
      next : 'a node ref ;
    }

  type 'a t =
    {
      mutable head : 'a node ref ;
      mutable tail : 'a node ref ;
      head_lock : Lock.t ;
      tail_lock : Lock.t ;
      not_empty_cond : Condition.t ;
    }

  let make ~capacity ~dummy =
    let head_lock = Lock.make () in
    let tail_lock = Lock.make () in
    let not_empty_cond = Condition.make head_lock in
    let head = ref Nil in
    let tail = head in
    { head ; tail ; head_lock ; tail_lock ; not_empty_cond }

  let try_enqueue q x =
    assert false

  let enqueue q x =
    let must_wake_dequeuers = ref false in
    let new_tail = ref Nil in
    let node = Cons { element = x ; next = new_tail } in
    Lock.acquire q.tail_lock ;
    let old_tail = q.tail in
    must_wake_dequeuers := (!old_tail = Nil) ;
    old_tail := node ;
    q.tail <- new_tail ;
    Lock.release q.tail_lock ;
    if !must_wake_dequeuers then begin
      Lock.acquire q.head_lock ;
      Condition.signal_all q.not_empty_cond ;
      Lock.release q.head_lock ;
    end

  let try_dequeue q =
    Lock.acquire q.head_lock ;
    begin match !(q.head) with
    | Nil ->
        Lock.release q.head_lock ;
        None
    | Cons { element ; next } ->
        q.head <- next ;
        Lock.release q.head_lock ;
        Some element
    end

  let dequeue q =
    Lock.acquire q.head_lock ;
    let rec cond_loop () =
      begin match !(q.head) with
      | Nil ->
          Condition.wait q.not_empty_cond ;
          cond_loop ()
      | Cons { element ; next } ->
          (element, next)
      end
    in
    let (x, new_head) = cond_loop () in
    q.head <- new_head ;
    Lock.release q.head_lock ;
    x

end


module UnboundedQueue2 : QUEUE = struct

  (*! module Lock = QueueLock !*) (* ← segfault because of this impl. (+ wrong impl. of ThreadLocal) *)
  module Lock = TTASLock
  module Condition = GenericCondition (Lock)

  type 'a node =
  | Nil
  | Cons of {
      element : 'a ;
      next : 'a node Atomic.t ;
    }

  type 'a t =
    {
      mutable head : 'a node Atomic.t ;
      mutable tail : 'a node Atomic.t ;
      head_lock : Lock.t ;
      tail_lock : Lock.t ;
      not_empty_cond : Condition.t ;
    }

  let make ~capacity ~dummy =
    let head_lock = Lock.make () in
    let tail_lock = Lock.make () in
    let not_empty_cond = Condition.make head_lock in
    let head = Atomic.make Nil in
    let tail = head in
    { head ; tail ; head_lock ; tail_lock ; not_empty_cond }

  let try_enqueue q x =
    assert false

  let enqueue q x =
    let must_wake_dequeuers = ref false in
    let new_tail = Atomic.make Nil in
    let node = Cons { element = x ; next = new_tail } in
    Lock.acquire q.tail_lock ;
    let old_tail = q.tail in
    must_wake_dequeuers := (Atomic.get old_tail = Nil) ;
    Atomic.set old_tail node ;
    q.tail <- new_tail ;
    Lock.release q.tail_lock ;
    if !must_wake_dequeuers then begin
      Lock.acquire q.head_lock ;
      Condition.signal_all q.not_empty_cond ;
      Lock.release q.head_lock ;
    end

  let try_dequeue q =
    Lock.acquire q.head_lock ;
    begin match Atomic.get q.head with
    | Nil ->
        Lock.release q.head_lock ;
        None
    | Cons { element ; next } ->
        q.head <- next ;
        Lock.release q.head_lock ;
        Some element
    end

  let dequeue q =
    Lock.acquire q.head_lock ;
    let rec cond_loop () =
      begin match Atomic.get q.head with
      | Nil ->
          Condition.wait q.not_empty_cond ;
          cond_loop ()
      | Cons { element ; next } ->
          (element, next)
      end
    in
    let (x, new_head) = cond_loop () in
    q.head <- new_head ;
    Lock.release q.head_lock ;
    x

end



module BufferQueue : QUEUE = struct

  type 'a cell =
    {
      timestamp : int Atomic.t ;
      mutable element : 'a ;
    }

  type 'a t =
    {
      buf : 'a cell array ;
      mask : int ;
      head : int Atomic.t ;
      tail : int Atomic.t ;
    }

  let make ?(capacity=64) ~dummy =
    assert (capacity >= 2) ;
    (* check that the capacity is a power of two (not required by the algorithm,
     * but it allows more efficient array lookups): *)
    assert (capacity land (capacity - 1) = 0) ;
    {
      buf = Array.init capacity (fun i ->
          {
            timestamp = Atomic.make i ;
            element = dummy ;
          }
        ) ;
      mask = capacity - 1 ; (* i.e. 0b111111 if capacity = 0b1000000 *)
      head = Atomic.make 0 ;
      tail = Atomic.make 0 ;
    }

  let try_enqueue q x =
    let bo = ExponentialBackoff.make () in
    let rec try_enqueue () =
      let head = Atomic.get q.head in
      let cell = q.buf.(head land q.mask) in
      let ts = Atomic.get cell.timestamp in
      if ts = head && Atomic.compare_and_set q.head head (head+1) then begin
        (* cell is available and we got it first: write our value *)
        cell.element <- x ;
        Atomic.set cell.timestamp (head+1) ;
        true
      end
      else if ts < head then
        (* cell is still in use in previous round: fail here *)
        false
      else begin
        (* either ts > head, or the CAS failed; in either case,
        * another enqueuer got the cell before us: we try again *)
        ExponentialBackoff.backoff bo ;
        try_enqueue ()
      end
    in
    try_enqueue ()

  let enqueue q x =
    while not @@ try_enqueue q x do () done

  let try_dequeue q =
    let bo = ExponentialBackoff.make () in
    let rec try_dequeue () =
      let tail = Atomic.get q.tail in
      let cell = q.buf.(tail land q.mask) in
      let ts = Atomic.get cell.timestamp in
      if ts = tail+1 && Atomic.compare_and_set q.tail tail (tail+1) then begin
        (* cell is available and we got it first: read its value *)
        let x = cell.element in
        (*! cell.element <- dummy ; !*) (* TODO for garbage collection *)
        Atomic.set cell.timestamp (tail + q.mask+1) ;
        Some x
      end
      else if ts < tail+1 then
        (* no element has been enqueued in this cell yet: fail here *)
        None
      else begin
        (* either ts > tail+1, or the CAS failed; in either case,
        * another dequeuer got the cell before us: we try again *)
        ExponentialBackoff.backoff bo ;
        try_dequeue ()
      end
    in
    try_dequeue ()

  let rec dequeue q =
    begin match try_dequeue q with
    | None   -> dequeue q
    | Some x -> x
    end

end

(* Benchmarks *****************************************************************)

let run_n (n : int) ~(thread : int -> 'a) ~(init : 'b) ~(collect : 'b -> 'a -> 'b) : 'b =
  let threads = Array.init n (fun i -> Domain.spawn (fun () -> thread i)) in
  Array.fold_left (fun acc th -> collect acc (Domain.join th)) init threads

let printf_lock = TTASLock.make ()
let my_fprintf out =
  Printf.ksprintf begin fun s ->
    TTASLock.acquire printf_lock ;
    Printf.fprintf out "%s" s ;
    TTASLock.release printf_lock ;
  end
let my_printf fmt = my_fprintf stdout fmt

(*! module Queue = UnboundedQueue !*)
module Queue = UnboundedQueue2
(*! module Queue = BufferQueue !*)


(** Functional correctness test *)

let nmax = 2400
let nb_consumers = 12
let nb_per_consumer =
  assert (nmax mod nb_consumers = 0) ;
  nmax / nb_consumers
let channel = Queue.make ~capacity:2 ~dummy:42

let producer () =
  for n = 1 to nmax do
    my_printf "enqueue %i…\n" n ;
    Queue.enqueue channel n ;
    my_printf "enqueued\n" ;
  done

let consumer () =
  let sum = ref 0 in
  for _ = 1 to nb_per_consumer do
    my_printf "    dequeue…\n" ;
    let n = Queue.dequeue channel in
    sum := !sum + n ;
    my_printf "    dequeued %i\n" n ;
  done ;
  !sum

let () =
  let sum =
    run_n (1 + nb_consumers)
      ~thread:(fun tid -> if tid = 0 then (producer () ; 0) else consumer ())
      ~init:0
      ~collect:(+)
  in
  my_printf "sum = %i\n" sum ;
  assert (sum = nmax * (nmax + 1) / 2) ;
  my_printf "done\n"

let () =
  exit 0


(** Performance test *)

let nmax = 10_000
let nb_consumers = 10
let channel = Queue.make ~capacity:4 ~dummy:(42,None)

let producer () =
  let primes = ref [] in
  for n = 1 to nmax do
    let elt =
      begin match List.find (fun p -> n mod p = 0) !primes with
      | p                   -> (n, Some p)
      | exception Not_found -> if n <> 1 then primes := n :: !primes; (n, None)
      end
    in
    Queue.enqueue channel elt
  done

let consumer () =
  let primes = ref [] in
  for _ = 1 to nmax / nb_consumers do
    let (n, opt_p) = Queue.dequeue channel in
    (*let s =
      begin match opt_p with
      | None   -> Printf.sprintf "%u is prime\n" n
      | Some p -> Printf.sprintf "%u is dividible by %u\n" n p
      end
    in
    Printf.printf "%s" s*)
    begin match opt_p with
    | None   -> primes := n :: !primes
    | Some p -> ()
    end
  done ;
  List.rev !primes

(*
let () =
  let finished = Atomic.make false in
  Domain.spawn begin fun () ->
    while not @@ Atomic.get finished do
      Unix.sleep 1 ;
      Printf.printf "%!" ;
    done
  end ;
  run_n (1 + nb_consumers)
    ~thread:(fun tid -> if tid = 0 then producer () else consumer ())
    ~init:()
    ~collect:(fun () () -> ()) ;
  Atomic.set finished true ;
  Printf.printf "done\n%!"
*)

let () =
  let finished = Atomic.make false in
  ignore @@ Domain.spawn begin fun () ->
    while not @@ Atomic.get finished do
      Unix.sleep 1 ;
      Printf.printf "%!" ;
    done
  end ;
  let primes =
    run_n (1 + nb_consumers)
      ~thread:(fun tid -> if tid = 0 then (producer () ; []) else consumer ())
      ~init:[]
      ~collect:(@)
  in
  ignore (Sys.opaque_identity primes) ;
  Atomic.set finished true ;
  Printf.printf "done\n%!"
