\subsection{The memory model}
\label{sec:mocaml:model}

\input{fig-model-objects}

In this section, we replicate Dolan \etal.'s definition of the \mocaml memory model
\citep{dolan-18} and extend it with memory allocation, CAS and blocks,
which do not appear in Dolan \etal.'s paper.
%(see~\sref{sec:mocaml:model:cmp-dolan} for a discussion of these extensions).
%
The formalization presented here is a small-step \emph{operational model}, 
contrasting with the axiomatic models that describe the memory models of Java 
(\sref{sec:java}) and C11 (\sref{sec:c11}).
Even though \mocaml can also be given an axiomatic model \citep{dolan-18},
in this dissertation we are interested in an operational semantics because
it enables reasoning about the execution of a program step by step,
in a program verification framework such as Iris;
indeed, in a later chapter (\chref{sec:llog})
we will instantiate Iris with the operational semantics that we present now.
%
We first define a number of semantic objects (\fref{fig:model:objects}), then
describe the behavior of the memory subsystem (\fref{fig:model:memsem}).
We then draw comparisons between the described model and those of Java and C11.

At this point, we assume a set~$\typeVal$ of values, which is defined later on
(\sref{sec:mocaml:lang}).
% TODO: actually define \typeVal later?

\subsubsection{Locations}

A \emph{location}~$\loc$ represents the address of a contiguous block of memory.
We assume a countably infinite set of locations~$\typeLoc$.

\subsubsection{Stores, blocks and cells}

A \emph{store}~$\store$ is a mathematical object that represents the entirety of the allocated shared memory.
It maps a finite set of block locations to block contents.
%
A \emph{block} represents a contiguous allocated area of memory, supporting efficient random accesses.
It has a \emph{length}~$\nbelems \in \N$ and comprises that number of \emph{cells}.
%
The contents of a block is the list of the contents of its cells, in order.
The shape of a cell’s contents depends on the access mode of that cell.

\subsubsection{Access modes}

Each cell is rigidly ascribed an \emph{access mode}~$\accmode$,
either \emph{atomic}~($\accmode = \accmodeAT$)
or \emph{non-atomic}~($\accmode = \accmodeNA$).
%
The memory subsystem described later (\fref{fig:model:memsem}) enforces that
this access mode is chosen when allocating the block
and remains constant throughout execution.%
%\todojh{
%    Ce qui est important, ce n'est pas tant que le mode d'accès reste constant,
%    mais plutôt que les accès le respectent. On peut donc plutôt écrire quelque
%    chose du genre:
%
%    [...] this access mode is chosen when allocating the block, and accesses to
%    the block must use the access mode chosen at allocation. If an access does
%    not fulfill this restriction, then it is considered undefined behavior. In
%    the Multicore OCaml implementation, the type system ensures that this rule
%    is followed.
%}
\footnote{%
  In the actual \mocaml language, typing enforces this discipline statically.
}

As far as the memory model is concerned, cells live entirely independent lives:
it is not required in principle that two cells of a given block have the same access mode%
\footnote{%
  However, merely for simplicity of presentation,
  the allocation primitives of the memory subsystem presented later
  enforce this property.
  %\todofp{
  %    Il faut en dire plus. En particulier, il faut dire que dans
  %    l'implémentation, les atomiques sont toujours dans des blocs de taille 1.
  %    Du coup, je propose de mettre cette note dans le texte principal.
  %}
}
and no synchronization is implied between two cells of a given block.

\subsubsection{Cell addresses}

Following a standard approach to ruling out aliasing issues,
blocks at different locations are disjoint, so that
an individual cell is uniquely identified by the pair~$\pair\loc\offset$
of its enclosing block’s address~$\loc$
and of the cell’s offset~$\offset$ within that block;
the offset ranges from~$0$ to~$\nbelems-1$ where $\nbelems$ is the length of the block.
%
In this dissertation, we denote this pair as $\locoff\loc\offset$,
and we use $\cell$, $\nacell$, $\atcell$ as conventional names
for such a ``cell address'';
%
we write $\nacell$ (respectively, $\atcell$)
to mean that it is the address of a non-atomic (respectively, atomic) cell.

By uncurrying, a store~$\store$, mapping locations to lists of cell contents,
can also be seen as mapping cell addresses to cell contents.
%
In that regard, given a cell~$\cell = \locoff\loc\offset$,
we allow ourselves to write $\storeapp\store\cell$
for $\storeapp\store\loc_\offset$,
that is, the element of index~$\offset$ in the list~$\storeapp\store\loc$.
%
Likewise, we write $\mapupdate\store\cell{x}$ for
$\mapupdate\store\loc{\mapupdate{\storeapp\store\loc}\offset x}$,
that is, for the store~$\store$ where the contents of cell~$\cell$ has been updated to~$x$.

% The definition `Loc → List CellContents` imposes more structure on the domain
% (= cell addresses).

\subsubsection{Histories}

As said, a store maps cell addresses to cell contents, whose shape depends on the access mode.
The contents of a non-atomic cell is
a history of all write events at this cell.
%
A \emph{history}~$\hist$ is a finite map of timestamps to values.
%
If desired, a history can also be viewed as a set of pairs of a timestamp
and a value, that is, a set of write events, under the constraint that no two
events have the same timestamp.

\subsubsection{Timestamps}

% fp: les trois orthographes time stamp, time-stamp, timestamp existent.

We use \emph{timestamps}~$\timest$ to describe the behavior of non-atomic accesses.
They belong to $\Q_{\geq 0}$, the set of the nonnegative rational numbers.
This set is chosen for a combination of properties:
\begin{itemize}%
  \item it is infinite: write operations cannot run short of timestamps;
  \item it is totally ordered:
    the operational semantics of the memory subsystem relies on comparing timestamps;
  \item its order is dense, that is, a timestamp can be found in between any two timestamps;
    this requirement, originally introduced by~\citet{kang-al-17} in their promising semantics
    (their operational semantics for the C11 model),
    is not actually used in our own proofs,
    but it adds no complexity by contrast with using $\typeTime \eqdef \N$
    and it sticks more closely to the memory model devised by~\citet{dolan-18};
  \item it admits a minimum element~$\timeMin$:
    this timestamp is used for initial writes, and only for initial writes.
\end{itemize}%
%
It is worth noting that the meaning of timestamps is purely ``per cell'':
that is, the timestamps associated with two write events at two distinct
cells are never compared.

\subsubsection{Views}
\label{sec:mocaml:model:views}

A \emph{view}~$\view$, referred to as a ``frontier'' by \citeauthor{dolan-18},
is a total mapping of cell addresses to timestamps
which is \emph{almost everywhere zero} (``aez'' for short),
that is, whose value is zero everywhere except at a finite set of cell 
addresses.
A timestamp is meaningful only for non-atomic cells;
the timestamp of atomic cells are not used by the semantics.
%(this is not an issue: atomic cells can be ascribed timestamp~$\timeMin$).
%
% Another definition for views might then be:
% \[
%   \typeView  \eqdef  \typeNACell \maparrow (\typeTime - \{\timeMin\}) \\
% \]

% evolves/increases
Each thread has a view, which increases over time.
This view gives, for each cell, the timestamp of the most
recent write event at this cell that the thread is aware of.
%
% Note: a view *cannot* be seen as a set of write events.
%   Indeed, future write events can receive an old timestamp
%   and thus become part of an existing view.
%
The view of the active thread imposes a constraint on the behavior of reads and
writes at non-atomic cells (\sref{sec:mocaml:lang:semantics}).

The order on timestamps gives rise to a partial order on views:
by definition,
the view inequality
$\view_1 \viewleq \view_2$
holds if and only if,
for every cell~$\cell$,
the timestamp inequality
$\viewapp{\view_1}\cell \timeleq \viewapp{\view_2}\cell$ holds.
%
Equipped with this order, views form a bounded join semilattice.
% $(\typeView, \viewleq, \viewjoin, \emptyview)$.
%
Its minimum element is the \emph{empty view}~$\emptyview$,
which maps every memory cell to the timestamp $\timeMin$.
%
Its join operation is the pointwise maximum: that is,
$\view_1 \viewjoin \view_2$
is $\Lam \cell. \max(\viewapp{\view_1}\cell, \viewapp{\view_2}\cell)$.

This order can be thought of as an \emph{information} order:
if $\view_1 \viewleq \view_2$ holds,
then a thread with view $\view_2$ has more information
(is aware of more write events) than a thread with view~$\view_1$.
%
As will be seen when presenting the memory subsystem,
a non-atomic access issued by a thread with view~$\view_2$
has \emph{fewer} permitted behaviors
than the same instruction issued by a thread with view~$\view_1$.
%
Indeed, the knowledge of a write event ``masks''
older (according to their timestamps) write events at the same non-atomic cell:
they cannot be read from anymore,
and new write events can only be inserted after the latest currently-known one.
% Glen says:
%   Think of a view as a mask.
%   $\view_1 \viewleq \view_2$ means $\view_1$ allows more reads,
%   $\view_1$ is a weaker mask, $\view_1$ is less restrictive a mask.
%   The empty view $\emptyview$ allows all reads.
%
%\todofp{define ``data race'' for \mocamls op.sem. -> OR move/delete this}
%In a correctly synchronized program (in the sense of \sref{sec:java}), threads 
%have exchanged enough information so that their views allow only one behavior 
%when reading or writing.

\subsubsection{Contents of atomic cells}

The contents of an atomic cell is a pair of a value and a view.
% notation: $\mkval\val\view$
Indeed, atomic cells in \mocaml serve two purposes, which are
conceptually independent.

On the one hand, each atomic cell stores a value
with a sequentially consistent semantics.
In other words, all threads agree at all times
on the current value of an atomic cell;
hence, it suffices to keep track of that value, as opposed to a complete history.

On the other hand, each atomic cell acts as a synchronization medium
with a ``release/acquire'' semantics.
%
As explained by \citet{dolan-18}, ``non-atomic writes made by one thread can
become known to another by communicating via an atomic cell.''
%
Formally speaking, each atomic cell stores a view,
that is, a certain amount of information about the non-atomic store.
This view is obtained and updated by accesses to that atomic cell.

\subsubsection{The memory subsystem}

\input{fig-model-memsem}

To complete the definition of the \mocaml memory model, there remains to give
an operational description of the behavior of the memory subsystem. This is
done via a labeled transition system, that is, a relation
$\store; \thisview \memReduces{\smash\someMemEvent} \store'; \thisview'$, which
describes how the shared global store~$\store$ and the view~$\thisview$ of the
active thread evolve through a memory event~$\someMemEvent$, yielding an
updated store~$\store'$ and an updated thread view~$\thisview'$.
%
The syntax of memory events is as follows,
where $\accmode$ is an access mode,
$\loc$ is a location, $\cell$ is a cell, $\nacell$ is a non-atomic cell,
$\atcell$ is an atomic cell, $\val, \val'$ are values and $n$ is a natural
integer:
%
\[
  \someMemEvent ::=
    \noMemEvent \mid
    \memEventAllocANY\loc\nbelems\val \mid
    \memEventReadANY\cell\val \mid
    \memEventWriteNA\nacell\val \mid
    \memEventUpdate\atcell\val{\val'} \mid
    \memEventLength\loc\nbelems
\]
These memory events also appear in the definition of the semantics of \mocaml
expressions (\sref{sec:mocaml:lang:semantics}). Thus, they form a language by which
the expression and memory subsystems communicate.

The rules defining the relation
$\store; \thisview \memReduces{\smash\someMemEvent} \store'; \thisview'$
appear in~\fref{fig:model:memsem}
(under the form of inference rules, where the premises and conclusion of a rule
are written above and below a line, respectively);
they can be briefly described as follows.

% These rules are taken from the PLDI paper, except that we added
% allocations and CAS.

When a fresh block of non-atomic cells is allocated (\memnaalloc)
with initial value~$\val$,
their respective histories consist of a single write event of that value
at timestamp~$\timeMin$.
%
This guarantees that a read of these cells will succeed, even if the reading thread
has not synchronized with the thread
where allocation was performed%
---which may happen in a racy program that would communicate the newly-allocated
address through a non-atomic cell without proper synchronization.
% fp: ce qui sous-entend alors que l'adresse de cette cellule a été
%     transmise via une lecture non-atomique?
%     Mai-z-alors, nous sommes en fait en train de discuter d'un programme
%     qui n'est pas vérifiable en Cosmo, car il exploite une data race
%     sur un nonatomique. C'est bien ça?
%
% jh: oui c'est exactement ça. Mais il faut que ce soit pris en compte
%     dans la sémantique (e.g., pour prouver la sûreté du typage). Et
%     puis, on pourrait imaginer que le mapsto fourni au moment de
%     l'allocation est objectif jusqu'à la première écriture.

Naturally, a read instruction at a non-atomic cell must read from
one of the write events in the history of this cell, and a write
instruction must extend the history of this cell with a write event at a
previously-unused timestamp. In either case, which timestamp may be chosen
is constrained by the ``view'' of the active thread.

A read instruction at cell~$\nacell$ (\memnaread) cannot read from an
outdated write event, therefore must read from an event whose
timestamp~$\timest$ is no less than $\viewapp\thisview\nacell$. Both
$\viewapp\thisview\nacell=\timest$ and $\viewapp\thisview\nacell\timelt\timest$ are
permitted: the latter case allows a thread to read from a write event of which
it is not yet aware. A non-atomic read instruction does \emph{not} update
the view of the active thread.

A~write instruction at cell~$\nacell$ (\memnawrite) cannot produce an
outdated write event, therefore must use a timestamp~$\timest$ greater than
$\viewapp\thisview\nacell$. The new timestamp must be fresh (that is, not in
the domain of $\hist$), but there is no requirement for~$\timest$ to be greater
than every timestamp in the domain of $\hist$. The history of cell~$\nacell$
and the view of the active thread are updated so as to include the new write
event.

Allocating a fresh block of atomic cells (\mematalloc) causes them to be
initialized with the value~$\val$ provided by the allocation instruction and
with the view $\thisview$ of the active thread.

When reading an atomic cell (\mematread) the view~$\view$ stored in
this cell is merged into the
view of the active thread, reflecting the idea that the active thread acquires
information via this read operation.

Updating an atomic cell (\mematreadwrite) overwrites the value~$\val$
stored at this cell with the new value~$\val'$ and updates the view
$\view$ stored at this cell by merging the view~$\thisview$ of the active
thread into~it, reflecting the idea that the active thread releases
information via this write operation.
Also, in the same atomic step, the active thread acquires the stored view, as when reading.

As \mocaml is equipped with a garbage collector, there is no deallocation event.

Lastly, the length of a block can be requested (\memlength), without affecting
the store nor the view of the active thread.

\subsubsection{Extensions to Dolan \etal.’s memory model}
\label{sec:mocaml:model:cmp-dolan}

The memory model proposed by \citeauthor{dolan-18} does not address memory 
allocation. We thus extend their model with suitable memory events and reduction 
rules. It is important to keep in mind that \mocaml ensures memory safety. This 
implies that uninitialized memory must not be observable. As a consequence, an 
allocation event is conflated with an initial write to the newly-allocated
block.
%
The fact that no thread can read an older state is ensured by the structure of 
our semantic objects: for a given non-atomic cell, any view gives 
at least the minimum timestamp~$\timeMin$ which corresponds to the initial 
write.
%
The initial write is immediately known to all threads. Implementation-wise, this
implies that the runtime system of \mocaml must perform some amount of
synchronization at some point---either when allocating, or when publishing the
address of a block to the shared memory, or when reading from such an address.

\citeauthor{dolan-18} omits CAS instructions, but adding them to the model is 
straightforward. Since an atomic write in their model combines the effects of an 
``acquire'' read and that of a ``release'' write, the same memory event 
$\memEventUpdate\atcell\val{\val'}$ can be used to model both an atomic write 
and a CAS instruction. By contrast with the original model, that event mentions 
the overwritten value~$\val$. It is ignored by atomic writes but matters for CAS 
instructions (the difference will become apparent later on 
in~\fref{fig:lang:langsem:expr:impure}).

\citeauthor{dolan-18}’s original paper features only mutable \emph{references},
that is, blocks of length~one.
We extend their model with blocks of arbitrary lengths.
Our extension permits efficient random accesses in the programming language,
which is needed for realistic implementations of some data~structures.
%
%To the same effect, rather than baking mutable blocks into the model, we could
%have equipped the language with immutable tuples supporting random accesses,
%while keeping mutable references of both access modes.
%By separating the concerns of mutability and random accesses,
%that alternative extension would have spared us
%the burden of modifying \citeauthor{dolan-18}’s memory model.
%This setting is less realistic, though,
%since one more indirection would be required for implementing a mutable block,
%as an immutable tuple of mutable references.
%
In practice, the present-day implementation of \mocaml{} has atomic references,
non-atomic references and
%mutable
blocks (``arrays'') of non-atomic cells.
It would not be hard to add language support for
%mutable
blocks of atomic cells,
or for records with mixed atomic and non-atomic fields,
as already supported by our model.

\subsubsection{Comparison with the memory model of Java}

The definition just given of the memory model of \mocaml is operational, that 
is, it consists of a (small-step) reduction relation with an interleaving 
semantics---but where the shared state has a more intricate mathematical 
structure than it would have in a sequentially consistent setting.
%
By contrast, the memory model of Java is traditionally described axiomatically
(as done in~\sref{sec:java}):
a ``candidate execution'' takes the form of a set of memory events and relations 
between them, and an actual execution is a candidate execution that respects 
certain conditions, such as the acyclicity of certain relations.
%
In fact, \citeauthor{dolan-18} also give an axiomatic account of their model, 
and provide a paper proof of the equivalence between their operational and 
axiomatic definitions.

The memory model of \mocaml is most similar to that of Java.
Both provide two access modes---``non-atomic'' and ``atomic'' in \mocaml terms,
or ordinary and ``volatile'' in Java terms---%
and a given memory cell must only be accessed with one mode
(a constraint that both languages enforce thanks to their typing discipline).
While non-atomic variables have a relaxed behavior,
atomic variables perform synchronization which allows the message-passing,
or ``release-acquire'', pattern.

To quote~\citeauthor{dolan-18}, both models ``bound data~races in space'';
that is, a race on one cell does not compromise accesses to other regions of memory.
This matches the memory safety guarantees offered by the Java and \mocaml languages,
in which some catastrophic failures---such as segmentation faults---%
must not happen even in the event of a race.
By contrast, in unsafe languages such as~C,
the presence of any race in a program
makes the behavior of the entire program undefined.

The memory model of \mocaml differs from that of Java on certain aspects.
Contrary to Java, \mocaml also ``bounds data~races in time'',
in the sense that a race on a given memory cell
does not affect earlier accesses to that cell,
nor does it compromise future accesses for good:
by performing enough synchronization, a program may recover from a racy state.
\citeauthor{dolan-18} give the following two examples.

The first example illustrates a lack of coherence in Java, which results in
a race that compromises later accesses even if subsequent synchronization is done.
Consider the following program,
with two shared cells $x$ and $y$ where only $y$ is atomic.
%
\begin{gather*}
\begin{array}{c}
      \text{$x$ is a non-atomic cell}
  \\  \text{$y$ is an atomic cell}
  \\  \text{initially $[x] = 0$ and $[y] = \False$}
\end{array}
\\\begin{array}{l||l}%
    \relax  [x] \gets 1     & [x] \gets 2
  \\\relax  [y] \gets \True & a \gets [y]
  \\\relax                  & b \gets [x]
  \\\relax                  & c \gets [x]
  \\\relax                  & \rlap{$\texttt{assert }(a = \False \lor b = c)$}
\end{array}\end{gather*}%
%
This program exhibits a race between the two writes to~$x$.
However, since atomic accesses perform synchronization,
a programmer might expect that,
if the second thread has read $\True$ in the atomic cell~$y$,
then it has observed all the writes to~$x$ and thus,
at that point, reading~$x$ twice would yield the same value%
---which one would be read depends on how the conflicting writes
would have been ordered when synchronizing.
In other words, when $a = \True$, either $(b,c) = (1,1)$ or $(b,c) = (2,2)$.

This expectation is violated in the memory model of Java, which also allows
$(b,c) = (1,2)$ and $(b,c) = (2,1)$.
Indeed, Java might optimize the first read of~$x$ to the constant~$2$
without doing likewise for the second read (if, for example, aliasing
hides from the compiler the fact that the cell to be read is~$x$).

On the other hand, the expectation holds in \mocaml;
its axiomatic model ensures it via an additional \emph{coherence} property.
In the operational model presented earlier, after it has read $\True$ in~$y$,
the right thread has updated its view to the maximum of the timestamps
of both writes to~$x$, hence the following reads are in fact not racy:
they can only read from the last write.
The race on~$x$ has been resolved thanks to synchronization.

The second example illustrates \emph{load buffering},
which is allowed by the Java model,
but is hard to reason about and can lead to arguably surprising results:
with load buffering, reads can be made inconsistent
because of \emph{future} data~races.
Consider the following example, where $x$ is a shared non-atomic cell.
%
\begin{gather*}
\begin{array}{c}
      \text{$x$ is a non-atomic cell}
  \\  \text{initially $x$ contains some valid address}
\end{array}
\\\begin{array}{l||l}%
    \relax  y \gets \AllocNA 0  & y \gets [x]
  \\\relax  [y] \gets 1         & [y] \gets 2
  \\\relax  a \gets [y]         &
  \\\relax  [x] \gets y         &
  \\\relax  \texttt{assert }(a = 1) &
\end{array}
\end{gather*}%
%
This program exhibits a data~race on~$x$.
Still, a programmer might want to reason about an execution before the race occurs;
one might expect that $a$ receives the value~$1$ when reading~$y$,
because at that point cell~$y$ has just been allocated
and is not shared yet, so no other thread could possibly have written to it.
This expectation is violated in Java:
because the read of~$y$ and the write to~$x$ in the left thread
operate on distinct cells, Java can reorder both operations,
which can lead to $a$ reading the value~$2$.

One of the design goals of the memory model of \mocaml
was to forbid load buffering; in the example above,
\mocaml fulfills the programmer’s expectation.

Reasoning about load buffering operationally is a challenging problem;
\citet{kang-al-17} devise the promising semantics for tackling it
in the context of C11.
The axiomatic model of \mocaml carefully avoids load buffering,
consequently its operational semantics does not need promises
and is simpler than that of \citeauthor{kang-al-17}.
%
The drawback, as can be seen, is that \mocaml permits fewer optimizations than Java.
\citeauthor{dolan-18}~(\S8) assess the performance penalty of this stricter model.

These authors’ claim that \mocaml ``bounds data races in space and time'' is 
made formal in a \emph{local data-race-freedom (local DRF) theorem} 
\citep[\S4--5]{dolan-18}, which is a stronger variant of Java’s DRF theorem 
(\sref{sec:java}), involving a notion of ongoing data races.

\subsubsection{Comparison with the memory model of C11}

It is less straightforward to compare the memory models of C11 (\sref{sec:c11}) 
and of \mocaml.

A C11 non-atomic location has no counterpart in \mocaml,
because data races on such a location have undefined behavior
according to the ``catch-fire'' model of C11,
whereas the model of \mocaml enforces memory safety, type safety,
and gives well-defined semantics to all memory accesses.
%
For this reason, a \mocaml non-atomic cell is rather comparable
to a C11 atomic location with relaxed accesses.
The latter is slightly stronger though:
indeed, in the axiomatic model of C11, there is a ``coherence order'' that 
relates all accesses to a given atomic location \citep[\S3]{rc11},
while in \mocaml, non-atomic reads are not necessarily ordered.
This is visible in the operational semantics, specifically in rule~\memnaread:
reading a non-atomic cell does not update the thread’s current view~$\thisview$, 
hence it is possible that a thread reads a write with some timestamp~$\timest$, 
then reads another write with an \emph{earlier} timestamp~$\timest' < \timest$.
This can only occur, though, when the reads are involved in a data race.
%
For instance, for the racy program below, the semantics of \mocaml allows the 
outcome $(a, b) = (1, 0)$, which violates the assertion.
%
\begin{gather*}
\begin{array}{c}
      \text{$x$ is a non-atomic cell}
  \\  \text{initially $[x] = 0$}
\end{array}
\\\begin{array}{l||l}%
    \relax  a \gets [x]         & [x] \gets 1
  \\\relax  b \gets [x]         &
  \\\relax  \texttt{assert }(a \leq b) &
\end{array}
\end{gather*}%
%
By contrast, the analogous program in C11 with relaxed atomic accesses 
is guaranteed to pass the assertion.

A \mocaml atomic cell is comparable to a C11 atomic location whose access mode 
would be a middle ground between SC accesses and release/acquire accesses. This 
can be analyzed by separating the two features provided by a \mocaml atomic 
cell: the value it stores and the synchronization it performs.

%\begin{enumerate}%
%  \item
%    An atomic cell in \mocaml is stronger than
%    an atomic location with release/acquire accesses in C11,
%    in that the former stores a single value at all times, seen identically by 
%    all threads, whereas the latter has a history of values and can be observed 
%    in different states by different threads, in the event of a data race.
%  \item
%    On the other hand, an atomic cell in \mocaml is weaker than
%    an atomic location with SC accesses in C11,
%    because C11 imposes a global ordering among all SC accesses 
%    to all locations whereas, in \mocaml, ordering is per cell.
%\end{enumerate}%

\begin{enumerate}%
  \item
    A \mocaml atomic cell stores a single value at all times, that is seen 
    identically by all threads.
    In this respect, it is similar to a C11 atomic location with SC accesses, 
    and is stronger than one with release/acquire accesses.
    Indeed, the latter stores a history of values rather than a single value, 
    and can be observed in different states by different threads in the event 
    of a data race.
  \item
    A \mocaml atomic cell transmits knowledge about the \emph{rest} of the memory 
    from writers to readers of that cell, in a release/acquire fashion.
    There is no implied synchronization between accesses to distinct atomic cells.
    \mocaml atomic cells are thus weaker than a C11 atomic location with SC 
    accesses, because C11 enforces synchronization between all SC accesses to 
    \emph{all} atomic locations.
\end{enumerate}%
