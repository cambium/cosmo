\chapter{Background}
\label{sec:background}

We aim at verifying the functional correctness of \emph{concurrent} programs,
that is, programs in which several threads run over overlapping periods of time.
%
Threads then compete for shared resources such as computing power or memory.
%
This situation may arise because an operating system schedules several threads
to run in alternation on a given processor;
or because parallel hardware---such as a multicore processor---%
runs several threads truly simultaneously;
or a combination of both.

It has long been noted that
the possibility of interaction between concurrent threads---for instance via 
signals or shared memory---
adds complexity to the study of concurrent programs,
by comparison with that of sequential programs.
%
First, the order in which instructions pertaining to different threads are 
executed becomes relevant, and there is a considerable number of possible orderings.
%
Second, with concurrent interactions come new kinds of programming bugs,
such as deadlocks---a situation in which a set of threads are blocked because they are waiting for one another---%
and starvation---a situation in which a thread is constantly denied access to a shared resource.
%
Lastly, ensuring that threads see the shared resources consistently is costly 
performance-wise, 
%hence weaker notions of consistency have been developed to accommodate for 
%optimizations done in practice.
so that hardware and compiler have implemented optimizations which complicate 
significantly the notion of consistency used---giving birth to so-called weak 
models of consistency.

To be able to study programs written in a concurrent programming language,
we first need to model what behaviors are allowed,
that is, give a semantics to the said language.
%
It is worth noting that such a semantics
is given at the level of the source language,
and that the system which has the responsibility of implementing it
is the combination of all actors that play a role in the execution of a program,
from hardware up to the compiler of the source language.
Thus, designing a semantics is guided
by the kind of optimizations we want from processors and compilers:
a more permissive semantics allows for more optimizations,
at the expense of more complexity for the programmer.

\section{An overview of weak memory}
\label{sec:weakmem}

\begingroup % for scoping the macros \WriteX and friends

A traditional description of the behavior of concurrent systems is
the \emph{sequential consistency} model \citep{lamport-79}.
%
In this model, the behavior of the concurrent system
is some interleaving of the executions of its components.
%
Even this simple model proves challenging for verification,
as the number of possible interleavings is exponential
in the number of instructions of each thread.

For instance, below is a simple concurrent program with two threads.
The shared memory contains two cells $x$ and $y$, with initial value~$0$.
The left thread writes~$1$ to~$x$ then reads~$y$,
whereas the right thread writes~$1$ to~$y$ then reads~$x$.
The output of the program is the pair of integers~$(a, b)$.
%
\newcommand* \TA[1] {\textcolor{blue}{#1}}
\newcommand* \TB[1] {\textcolor{green!50!black}{#1}}
\newcommand* \WriteX {\TA{[x] \gets 1}}
\newcommand* \WriteY {\TB{[y] \gets 1}}
\newcommand* \ReadY {\TA{a \gets [y]}}
\newcommand* \ReadX {\TB{b \gets [x]}}
\newcommand* \Outcome[2] {\text{output is $(#1, #2)$}}
%
\[\begin{array}{l||l}%
      \multicolumn{2}{c}{\!\!\text{initially }[x] = [y] = 0}
  \\  \WriteX & \WriteY
  \\  \ReadY  & \ReadX
\end{array}\]%
%
In the sequentially consistent model,
since both the left and right threads have two instructions,
there are $\binom{2+2}{2,\,2} = 6$ possible interleavings,
producing varying outputs:
%
\[\begin{array}{ll}%
      \WriteX;\ \ReadY;\ \WriteY;\ \ReadX & \Outcome01
  \\  \WriteX;\ \WriteY;\ \ReadY;\ \ReadX & \Outcome11
  \\  \WriteX;\ \WriteY;\ \ReadX;\ \ReadY & \Outcome11
  \\  \WriteY;\ \WriteX;\ \ReadY;\ \ReadX & \Outcome11
  \\  \WriteY;\ \WriteX;\ \ReadX;\ \ReadY & \Outcome11
  \\  \WriteY;\ \ReadX;\ \WriteX;\ \ReadY & \Outcome10
\end{array}\]%
%
Thus the sequentially consistent model allows three possible outcomes:
$(0,1)$ or $(1,0)$ or $(1,1)$. The outcome $(0,0)$ is excluded.

Enforcing sequential consistency, however, has a cost:
it precludes many desirable optimizations.
If $x$ and $y$ are disjoint memory locations,
then there are no dependencies between the write of~$x$ and the read of~$y$,
and an optimizing compiler fed with the source code of the left thread
may be tempted to swap both instructions.
In a sequential context, this change does not alter
the observable behaviors of a program,
and it might result in a significant performance gain%
---be it by reducing memory latency
or by triggering further optimizations, such as redundant read elimination.
In the face of concurrency however, the change results in other behaviors
than those previously listed; for instance, $(0,0)$ becomes a possible outcome:
%
\[\begin{array}{ll}%
      \ReadY;\ \WriteY;\ \ReadX;\ \WriteX & \Outcome00
\end{array}\]%
%
This new behavior violates sequential consistency,
as it cannot be described as the outcome of any interleaving of the instructions
of the source code of both threads.

Yet we do not want to give up on optimizing sequential programs.
Even in a concurrent program, a significant part of the work of a thread
may consist in local state mutation that is unaffected by other threads.
Thus, rather than enforcing sequential consistency strictly,
it is often more pragmatic to allow for more optimizations,
which preserve semantics in a sequential context,
and to let the programmer restrict behaviors %in case of concurrency
by means of inter-thread synchronization idioms%
---such as locks or fences---when concurrency is involved.
%
Therefore, \emph{even without true parallelism},
optimization puts sequential consistency in peril.
Multicore architectures,
featuring cache layers, performing write buffering,
make violations of sequential consistency unavoidable.

The broad problem of consistency of a shared state has been attacked from various angles.
%
%There is thus a need for weaker models of consistency.
Historically, weaker models of consistency have been developed
in the context of distributed systems,
where several machines have different views of distributed data.
For instance,
whereas sequential consistency guarantees that there is a total order
on memory accesses on which all threads agree,
``causal consistency'' merely ensures order
within sets of causally-related accesses,
for some notion of causality.
% TODO: ``eventual consistency''
% \url{https://lamport.azurewebsites.net/pubs/time-clocks.pdf} (1978)

%\todo{off-topic?}
Transactional memory models are an alternative to lock-based synchronization
that originates from the world of distributed databases.
In such models,
blocks of operations called transactions are to be committed atomically:
they can perform multiple updates to the shared state,
but these effects only become visible to other processes all at once,
at the final commit point.
When a conflict is detected, a tentative transaction is aborted and re-tried.
This kind of models aims to achieve serializability,
a strong form of sequential consistency between transactions,
while preventing deadlocks
and relieving the programmer from manipulating locks.
By contrast with lock-based programming,
which is pessimistic in that it uses systematic synchronization,
transactional memory can reduce the cost of parallelism
when there is little contention.
However, even transactional memory does not evade the desire for optimization:
database systems often allow to relax the atomicity of transactions by reducing
their so-called ``isolation level'',
which breaks serializability and opens the door to inconsistent database accesses.

In the domain of programming languages, from the 1990s onward there has been 
a surge of interest for weak memory models. The most notable research efforts 
have been devoted to giving a memory model to Java (\sref{sec:java}), then to 
C/C++ (\sref{sec:c11}). Drawing from the experience of these predecessors, 
\mocaml also comes with its own weak memory model (\sref{sec:mocaml:model}).
A~comparison between \mocamls model and these two predecessors is sketched 
in~\sref{sec:mocaml:model}.

\endgroup % for scoping the macros \WriteX and friends

\input{background-java}
\input{background-c11}

\section{Multicore OCaml}
\label{sec:mocaml}

In this section, we present the \mocaml memory model as well as the syntax and
operational semantics of a core calculus that is representative of the \mocaml
programming language.
We start by describing interactions with the memory, isolated in a memory subsystem
(\sref{sec:mocaml:model}), then we present the reduction rules of the
programming language itself (\sref{sec:mocaml:lang}).

\input{background-mocaml-model}
\input{background-mocaml-lang}

\section{Program verification}
\label{sec:progverif}

Program verification is the act of ensuring that a program behaves as intended. 
%It can take many forms.
%\todo{a few sentences about software testing / model checking / abs.\ int.\ / …}
%
In this dissertation we are interested in proving the \emph{functional 
correctness} of programs, that is, proving that they produce expected outputs in 
relation with their inputs according to a given formal \emph{specification}.
%
An even more fundamental property is program \emph{safety}: a program~$\expr$ is 
safe, with respect to an operational semantics, if no reduction chain starting 
from~$\expr$ leads to a stuck program.
%that is, a program that neither is a value nor reduces.
A~stuck program can be regarded as a crash, an error that definitely must not happen.
%
%Safety does not subsume functional correctness, but it allows to encode it for 
%computable predicates.

%\subsection{Functional correctness, Hoare/WP reasoning rules}

A specification is typically expressed as a \emph{Hoare 
triple}~\citep{hoare1969axiomatic}, that is, a logical assertion of the 
following form:%
%
\footnote{%
  Note that the formalism we use takes several now-standard departures from 
  Hoare’s original logic~\citep{hoare1969axiomatic}: whereas Hoare logic was 
  axiomatic, ours is derived from our programming language’s operational 
  semantics and ensures program safety even for non-terminating programs. 
  Furthermore, to accommodate for a functional language, the subject of triples 
  are expressions rather than statements, and the postcondition features 
  a binder which captures the resulting value of that expression.
}
%
\[%
  \hoare P e {\Lam x. Q}
\]%
%
In this notation, $e$ is a program fragment---source code---and $P$~and~$Q$ are 
logical assertions which describe what should hold of the computer 
state---typically, the contents of the memory---before and after program~$e$ has run.
The syntax $\Lam x. Q$ binds the logical variable~$x$ in the formula~$Q$.
%
Such a triple is intuitively read as: if program~$e$ runs from a state that 
satisfies \emph{precondition}~$P$, then $e$ is safe; furthermore, if it 
terminates, then its return value~$x$ and the state it leaves the computer in 
satisfy \emph{postcondition}~$Q$.

In this reading, program termination is an assumption:
in the situation when $e$ does not terminate, only safety is guaranteed.
This is called \emph{partial} correctness.
\emph{Total} correctness is an alternative, stronger reading which guarantees 
termination rather than assuming it.
%
Proving program termination, however, is out of the scope of this dissertation,
as is proving deadlock-freedom or starvation-freedom---the latter being a liveness property.

To prove a functional specification,
we have at hand a set of reasoning rules about Hoare triples.
The first two rules in \fref{fig:hoare:struct} (ignoring \hoareslframe for now)
are fundamental rules, that do not depend on the actual programming language.
%
Rule~\hoareslcsq states that, from a specification,
we can deduce any weaker specification, that is,
one with a stronger precondition and a weaker postcondition.
%
Rule~\hoareslbind implies that, to prove the specification of a program,
we can chain a specification for its head reduction step
and a specification for the resultant program fragment;
this allows to reason about an execution step~by~step.
%
To this effect, in addition to these two structural rules,
one has rules that describe the behavior of each reduction step of the programming language.
These rules are guided by syntax and reflect the operational semantics.
For example, assuming that our language has Boolean values and an ``if-then-else'' construct,
one would typically have rules resembling these two:
%
\begin{mathpar}
  \inferrule
    {\hoare \prop {\expr_1} {\Lam \var. \propB}}
    {\hoare \prop {\IfThenElse \True {\expr_1} {\expr_2}} {\Lam \var. \propB}}

  \inferrule
    {\hoare \prop {\expr_2} {\Lam \var. \propB}}
    {\hoare \prop {\IfThenElse \False {\expr_1} {\expr_2}} {\Lam \var. \propB}}
\end{mathpar}

With an expressive enough logical language, one can describe a program’s 
contract very precisely. Hence Hoare-style functional correctness is one of the strongest forms of program 
verification, and requires a great deal of human intervention.
%
Thus it is a work \textit{per~se} to come up with a satisfying specification: 
one that both is provable and captures the important facts while, ideally, 
remaining simple enough to manipulate.

An important feature is \emph{composability} of specifications.
Indeed, software code bases are large
and rely extensively on re-use of conceptually independent components,
such as libraries and functions.
To scale verification to these code bases, it is of utmost importance that we 
are able to specify and verify an independent component in isolation, once and 
for all, and then re-use its specification when verifying each part of the code 
that use it.
%
In this work, we achieve composability thanks to (concurrent) separation logic, 
and also thanks to a mechanism of monotonic views of memory. We now introduce 
the former; an extensive presentation of the latter is done in \chref{sec:hlog}.
%
%\todo{composability provided by the monotonic view mechanism: can forget parts 
%of a view; allows to abstract from the global event graph = global state}

\input{fig-hoare-struct}

\input{background-csl}
\input{background-iris}
%\input{background-logat}
