\begin{otherlanguage}{french}

\newcommand{\EN}[1]{\foreignlanguage{english}{\it #1}}

\chapter
  %[Résumé substantiel en français]
  {Résumé substantiel dans la langue de Desnos}

\section*{Vérification de programmes concurrents en mémoire faible}

Ces vingt dernières années, les progrès technologiques concernant les
ordinateurs multicœurs ont accentué le besoin d’outils puissants permettant
d’écrire du code multicœur efficace et sûr. Cela comprend des langages de
programmation bien conçus, leurs compilateurs, des bibliothèques concurrentes
efficaces, ainsi que des logiques de programme expressives, qui permettent de
prouver la correction desdites bibliothèques et du code qui les utilise.
Quoique certains outils de vérification existent déjà, de nombreuses questions
de recherche subsistent quant à l’emploi de ces outils pour spécifier et
vérifier, modulairement, des bibliothèques réalistes de concurrence à grains fins.

Dans la plupart des langages de programmation multicœurs, les fils d’exécution
communiquent via une mémoire partagée. Pour des raisons de performance
cependant, les accès concurrents à cette mémoire partagée obéissent rarement
à une sémantique \emph{séquentiellement cohérente}~\citep{lamport-79}, selon
laquelle un programme se comporterait comme un entrelacement des actions de ses
fils, qui auraient tous la même vue de la mémoire à chaque instant.
%
Pour bénéficier d’optimisations agressives effectuées par le matériel
et permettre au compilateur d’en effectuer également,
de nombreux langages parmi les plus utilisés
adoptent des modèles plus relâchés de la mémoire, dit \emph{faibles},
dont la description est subtile.
%
Par exemple C, C++ et Rust suivent le modèle mémoire C11 \citep{c11mm,rc11};
Java dispose également d’un modèle mémoire faible \citep{jmm,lochbihler-jmm-12,bender-palsberg-19}.

\mocaml \citep{dolan-18,mocaml} s’inscrit dans cette tendance.
Il s’agit d’une extension du langage au chameau \citep{ocaml}
auquel elle ajoute un support de la concurrence à mémoire partagée.
\citeauthor{dolan-18} donnent une sémantique à \mocaml,
et en particulier un modèle mémoire.
Ce modèle mémoire est faible.
%
Vu la subtilité de tels modèles,
le besoin se fait sentir d’un ensemble de règles de raisonnement
---~autrement dit, d’une logique de programme~---
pour comprendre et vérifier du code concurrent écrit dans ce langage.
%
Une telle logique de programme doit permettre un raisonnement compositionnel:
une fois définie la spécification d’une bibliothèque~$B$,
la vérification de $B$ doit pouvoir se faire
indépendamment de la vérification d’un code qui utilise $B$.
Ainsi, un programmeur peut utiliser $B$ en boîte noire,
sans connaissance sur son implémentation.
%
De plus, la majorité du code qu’on écrit reste séquentiel et, lorsqu’on écrit du code concurrent, c’est souvent à travers l’emploi de structures de données concurrentes implémentées par des experts plutôt qu’en utilisant directement les primitives du langage.
Il est donc souhaitable que la logique permette d’ignorer les détails du modèle mémoire de \mocaml quand ceux-ci ne sont pas pertinents.
Un programme séquentiel doit pouvoir être vérifié, idéalement, comme si le modèle mémoire était séquentiellement cohérent.
Le comportement d’une structure de données concurrente doit pouvoir être spécifié indépendamment des primitives qui sont employées pour l’implémenter.

Toujours ces vingt dernières années,
afin de spécifier et de vérifier formellement des programmes concurrents à mémoire partagée,
se sont développées un grand nombre de \emph{logiques de séparation concurrentes}
dans le sillage des travaux de \citet{brookes-04} et \citet{ohearn-07}.
%
L’idée-clé de la logique de séparation est qu’une assertion de la logique
ne se contente pas d’affirmer un fait mathématique:
elle reflète la possession d’une \emph{ressource},
dont les zones de la mémoire sont l’archétype.
Dans sa forme la plus élémentaire, la possession d’une ressource est unique.
Cette idée, déjà fructueuse pour la vérification de programmes impératifs séquentiels,
se révèle particulièrement précieuse pour la vérification de programmes concurrents.
%
Des logiques de séparation d’ordre supérieur et de plus en plus expressives
ont émergé~\citep{brookes-ohearn-16},
à l’instar d’Iris \citep{iris-15,iris}.
Iris est mécanisé et prouvé sûr dans Coq,
supporte la concurrence à grains fins,
et peut décrire des protocoles concurrents complexes
grâce à deux mécanismes essentiels: l’\emph{état fantôme} et les \emph{invariants}.
L’état fantôme en Iris permet de réaliser la notion de ressource et est très expressif,
puisque sa structure peut être n’importe quel monoïde commutatif partiel, au choix.
Iris est paramétrable par le langage de programmation
mais suppose toutefois une sémantique séquentiellement cohérente.
%
Une nouvelle génération de logiques lève cette restriction pour divers modèles faibles:
iCAP-TSO~\citep{sieczkowski-15},
GPS~\citep{turon-vafeiadis-dreyer-14}, iGPS~\citep{kaiser-17} et
iRC11~\citep{dang-20}
supportent des fragments du modèle C11.

Dans cette thèse, nous proposons une logique de séparation pour \mocaml.
Comme iGPS et iRC11, le travail que nous présentons est construit sur Iris.
%
Toutes nos contributions
(implémentation des logiques \llog et \hlog,
preuve de leur sûreté,
et études de cas)
sont mécanisées en Coq avec l’infrastructure Iris.
%
Nos preuves sont consultables dans notre dépôt \citep{repo}.

%For instance,
%\citet{sieczkowski-15} propose iCAP-TSO, a variant of \CSL that is sound with
%respect to the TSO memory model. While iCAP-TSO allows explicit low-level
%reasoning about weak memory, it also includes a high-level fragment, the ``SC
%logic'', whose reasoning rules are the traditional rules of \CSL. These rules,
%which are independent of the memory model, require all primitive accesses to
%memory to be data-race-free. Therefore, they require synchronization to be
%performed by other means. A typical means of achieving synchronization is to
%implement a lock, a concurrent data structure whose specification can be
%expressed in the SC logic, but whose proof of correctness must be carried out
%at the lower level of iCAP-TSO.
%%
%As another influential example, \citet{kaiser-17} follow a similar approach:
%they instantiate Iris~\citep{iris} for a fragment of the C11 memory model.
%This yields a low-level ``base logic'', on top of which
%Kaiser et al.\ proceed to define several higher-level logics, whose reasoning
%rules are easier to use.

%Our aim, in this work, is analogous. We wish to allow the verification of a
%low-level concurrent data structure implementation, such as the implementation
%of a lock. Such a verification effort must take place in a program logic that
%exposes the details of the \mocaml memory model. At the same time, we would
%like the program logic to offer a high-level fragment that is independent of
%the memory model and in which data-race-free accesses to memory, mediated by
%locks or other concurrent data structures, are permitted.

\section*{Le modèle mémoire de Multicore OCaml}

Par rapport à d’autres modèles mémoire actuels, celui de \mocaml est relativement simple.
Il distingue deux types de cases mémoire, \emph{atomique} et \emph{non-atomique}.
%
L’ensemble des comportements possibles d’un programme est toujours bien défini.
Les courses de données sur des cases non-atomiques sont permises
et ont un « effet limité dans l’espace et dans le temps » \citep{dolan-18}.
Ceci différencie le modèle de \mocaml du modèle C11,
dans lequel toute course de données est un « comportement indéfini »
qu’une logique de programme sûre doit donc bannir.

\citeauthor{dolan-18} décrivent le modèle de \mocaml au moyen d’une sémantique opérationnelle (\fref{fig:lang:langsem}),
dans laquelle l’exécution d’un programme apparaît bel et bien comme un entrelacement des exécutions de chaque fil.
Cependant, les interactions de ces fils avec la mémoire partagée (\fref{fig:model:memsem})
ont un comportement plus complexe que celui qu’on aurait dans l’habituel cadre séquentiellement cohérent.
La description de ce comportement fait intervenir
un \emph{historique} pour chaque case non-atomique de la mémoire partagée,
et des \emph{vues} de cette mémoire partagée (\fref{fig:model:objects}).
%
L’historique~$\hist$ d’une case non-atomique est
l’ensemble des événements d’écriture qui ont eu lieu sur cette case,
ordonnés par des horodatages~$\timest$
(dont les valeurs sont arbitraires et propres à chaque case, seul l’ordre étant significatif).
%
Chaque fil d’exécution~$\mkexpr\expr\thisview$ possède sa propre vue~$\thisview$ de la mémoire partagée,
qui donne, pour chaque case non-atomique~$\nacell$,
l’horodatage~$\viewapp\thisview\nacell$ du plus récent événement d’écriture dont ce fil a connaissance.
%
Cette vue restreint les comportements possibles d’un accès à une case non-atomique.

Une lecture de la case non-atomique~$\nacell$
(\basenaread dans la \fref{fig:model:memsem})
ne peut pas lire un événement d’écriture obsolète,
et doit donc lire un événement
dont l’horodatage~$\timest$ est au moins $\viewapp\thisview\nacell$,
le plus récent horodatage connu du fil courant.
Les deux cas de figure
$\viewapp\thisview\nacell=\timest$ et $\viewapp\thisview\nacell\timelt\timest$
sont permis: le second cas autorise un fil à lire un événement d’écriture
dont il n’a pas encore été officiellement informé.
Dans ce cas, le fil ne met pas à jour sa vue courante.

Une écriture dans la case non-atomique~$\nacell$
(\basenawrite dans la \fref{fig:model:memsem})
ne peut pas produire un événement d’écriture obsolète,
et doit donc utiliser un horodatage~$\timest$ strictement supérieur à~$\viewapp\thisview\nacell$...
mais pas nécessairement supérieur à~$\max(\Dom\hist)$;
autrement dit, si le fil n’a pas connaissance du plus récent événement d’écriture,
le comportement n’est pas déterministe, car
le nouvel événement d’écriture peut s’intercaler soit avant soit après
les événements dont le fil n’a pas encore connaissance
(ce cas de figure correspond à une course de données sur~$\nacell$).
L’historique~$\hist$ de la case et la vue~$\thisview$ du fil sont mis à jour
pour prendre en compte ce nouvel événement d’écriture.
Cependant, la vue des autres fils reste inchangée:
les autres fils ne sont pas informés de ce nouvel événement d’écriture.

Contrairement aux cases non-atomiques,
les cases atomiques ont bien une valeur unique,
vue à l’identique par tous les fils.
Cependant, leur fonction ne s’arrête pas là:
les accès aux cases atomiques effectuent de la \emph{synchronisation}.
Cela se traduit par le fait qu’une case atomique~$\atcell$,
en plus de sa valeur~$\val$,
mémorise une vue~$\view$.
%
Un fil qui écrit dans~$\atcell$
(\baseatwrite dans la \fref{fig:model:memsem})
déverse sa connaissance~$\thisview$ dans~$\atcell$.
%
Par la suite, un fil qui lit cette même case~$\atcell$
(\baseatread dans la \fref{fig:model:memsem})
reçoit la connaissance emmagasinée dans~$\atcell$ et l’ajoute à sa propre vue.
%
Les cases atomiques sont ainsi le moyen primitif en \mocaml
par lequel les fils d’exécution s’échangent la connaissance de certains événements d’écriture.

\section*{\llog, une logique bas niveau pour \mocaml}

Dans cette thèse, on s’appuie sur la sémantique opérationnelle de \citeauthor{dolan-18}
pour construire une première logique de programme sûre et mécanisée dans Coq,
baptisée \llog,
qu’on obtient en instanciant Iris.
En substance, on fournit à Iris deux ingrédients \citep[\S7.3]{iris}.
%
\begin{itemize}
  \item La sémantique opérationnelle de notre langage,
    qui se présente comme une relation de réduction locale à un fil,
    de la forme suivante:
    \[
      \store; \mkexpr{\expr}{\thisview}
      \threadPoolReduces
      \store'; \mkexpr{\expr'}{\thisview'}, \pool
    \]%.
    Cette relation décrit, partant d’un certain état partagé~$\store$,
    la prochaine étape de calcul que peut effectuer un fil~$\mkexpr\expr\thisview$
    et la façon dont cette étape modifie l’état partagé et le fil considéré,
    créant éventuellement de nouveaux fils~$\pool$.
    Dans un modèle séquentiellement cohérent,
    un fil est simplement représenté par son code source (c’est-à-dire une expression du langage) restant à évaluer;
    cependant, dans \mocaml, un fil est en fait un couple $\mkexpr\expr\thisview$ où $\expr$ est une expression restant à évaluer et $\thisview$ est la vue courante du fil.
  \item Une « interprétation d’état »,
    c’est-à-dire un prédicat
    $\stateinterp : \typeStore\ra\typeIProp$
    (où $\typeStore$ est le type représentant la mémoire partagée,
    défini dans la \fref{fig:model:objects},
    et $\typeIProp$ est le type des assertions d’Iris).
    Cette interprétation d’état exprime un invariant global sur l’état partagé.
    En l’occurrence, on a besoin d’exprimer un invariant concernant les vues
    emmagasinées dans les cases atomiques:
    ces vues doivent être \emph{valides},
    c’est-à-dire que l’horodatage qu’elles donnent pour chaque case non-atomique~$\nacell$
    ne doit pas dépasser le plus récent événement d’écriture dans l’historique de~$\nacell$.
    L’interprétation d’état $\stateinterp(\store)$
    traduit l’état partagé~$\store : \typeStore$
    en un état fantôme dont on a soigneusement choisi la structure
    afin de pouvoir séparer chacune des cases de la mémoire
    en une ressource individuelle,
    représentée par une assertion \EN{points-to}
    qui satisfait certaines propriétés souhaitées.
\end{itemize}

On obtient une logique de séparation qui,
outre les fonctionnalités de base d’Iris~(\sref{sec:iris}),
inclut une assertion $\hoare\prop{\mkexpr\expr\thisview}\pred$.
Celle-ci représente le triplet de Hoare à propos du fil~$\mkexpr\expr\thisview$
ayant pour précondition~$\prop : \typeIProp$
et pour postcondition~$\pred : \typeVal \times \typeView \ra \typeIProp$.
Un tel triplet exprime la correction partielle d’un programme
vis-à-vis d’une spécification.
Ceci se formalise dans un théorème d’adéquation
qui dérive de celui fourni par Iris \citep[\S6.4, \S7.4]{iris}:
si un programme~$\expr$
satisfait un triplet $\hoare \TRUE {\mkexpr\expr\emptyview} \pred$
alors, d’une part, ce programme est sûr,
c’est-à-dire que son exécution ne mènera jamais à une configuration bloquée;
d’autre part, ses éventuelles configurations finales~$\mkexpr{\val'}{\thisview'}$
satisferont la postcondition~$\pred$.
%
Les triplets satisfont également des règles de raisonnement usuelles et indépendantes du langage
(\fref{fig:hoare:struct} et \sref{sec:iris}),
telles que
la règle de conséquence (\hoareslcsq),
la règle de séquence (\hoareslbind),
la règle de passage au contexte (\hoareslframe)
et la règle d’ouverture d’un invariant (\RULE{HoareInv}).

Pour compléter notre logique de programme pour \mocaml,
on définit alors, au sein de la logique obtenue,
des assertions spécifiques au langage.
Elles revêtent la forme traditionnelle d’assertions \EN{points-to}
avec des permissions fractionnaires
\citep{boyland-fractions-03,bornat-permission-accounting-05},
à la différence que les cases ne contiennent pas une simple « valeur ».
%
\begin{itemize}
  \item
    Une assertion $\histNA[q]\nacell\hist$ représente
    la possession d’une fraction~$q \in \Q \cap (0, 1]$ de la case non-atomique~$\nacell$,
    dont l’historique est $\hist$.
    La fraction~$1$ représente une possession unique.
  \item
    Une assertion $\pointstoAT[q]\atcell{\mkval\val\view}$ représente
    la possession d’une fraction~$q$ de la case atomique~$\atcell$,
    dont la valeur est $\val$ et qui a emmagasiné la vue $\view$ (au moins).
    La fraction~$1$ représente une possession unique.
\end{itemize}
%
Ces assertions satisfont entre autres les propriétés suivantes:
%
\begin{mathpar}
  % NA state interpretation:
  \infer{%
      \histNA[q]\nacell\hist  \IISEP
      \stateinterp(\store)
    }{%
      \storeapp\store\nacell = \hist  \IISEP
      0 < q \leq 1
    }

  % AT state interpretation:
  \infer{%
      \pointstoAT[q]\atcell{\mkval\val\view}  \IISEP
      \stateinterp(\store)
    }{%
      \Exists \altview.
      \storeapp\store\atcell = \mkval\val{\altview}  \IISEP
      \view \viewleq \altview  \ISEP
      0 < q \leq 1
    }

  % NA composition + agreement:
  \infer{}
    {%
      \histNA[q_1]\nacell{\hist_1}  \ISEP
      \histNA[q_2]\nacell{\hist_2}
      \IEQUIV
      \histNA[(q_1+q_2)]\nacell{\hist_1}  \ISEP
      \liftpure{\hist_1 = \hist_2}
    }

  % AT composition + agreement:
  \infer{}
    {%
      \pointstoAT[q_1]\atcell{\mkval{\val_1}{\view_1}}  \ISEP
      \pointstoAT[q_2]\atcell{\mkval{\val_2}{\view_2}}
      \IEQUIV
      \pointstoAT[(q_1+q_2)]\atcell{\mkval{\val_1}{\view_1 \viewjoin \view_2}}  \ISEP
      \val_1 = \val_2
    }
\end{mathpar}
%
Pour des raisons techniques, on a aussi besoin de définir une assertion
$\valid\view$ qui affirme que la vue~$\view$ est valide
%vis-à-vis de (l’interprétation~$\stateinterp(\store)$ de) l’état partagé~$\store$.
vis-à-vis de l’état partagé~$\store$,
via l’invariant global sur les vues,
évoqué précédemment
et garanti par l’interprétation~$\stateinterp(\store)$.

Enfin, on prouve un triplet de Hoare pour chaque étape élémentaire de calcul du langage,
et en particulier pour chaque opération d’accès à la mémoire
(\fref{fig:llog:rules:mem}).
Les triplets pour ces opérations comportent
en précondition et en postcondition
la possession (unique ou fractionnaire) de la case mémoire concernée.
Ces triplets reflètent fidèlement la sémantique opérationnelle et le modèle mémoire de \mocaml.

On obtient ainsi un ensemble de règles de raisonnement
permettant de vérifier un programme \mocaml étape par étape.
%
Utiliser cette logique de programme
plutôt que de travailler directement avec la sémantique opérationnelle
offre de clairs avantages.
%
\begin{itemize}
  \item
    On dispose d’un langage de spécification riche,
    qui contient la logique hôte (celle de Coq)
    et une infrastructure d’état fantôme expressive.
  \item
    Un système de preuve sûr ---~et mécanisé~--- nous permet de prouver
    la sûreté d’un programme et sa correction fonctionnelle partielle.
  \item
    Grâce à la logique de séparation concurrente,
    les spécifications sont réduites à leur empreinte minimale
    et leurs preuves sont composables:
    chaque morceau de programme peut ainsi être spécifié et vérifié en isolation.
    Il n’y a pas besoin de considérer les interférences causées par les autres morceaux de programme.
    En particulier, lorsqu’on vérifie un programme avec plusieurs fils d’exécution,
    il n’est pas nécessaire d’envisager les très nombreux entrelacements possibles
    des actions de chaque fil.
\end{itemize}

Cependant, cette logique de programme
%reflète fidèlement le modèle mémoire de \mocaml et en expose les moindres détails.
expose les moindres détails de \mocaml.
Elle est par conséquent de bas niveau et assez fastidieuse à utiliser.
Plusieurs critiques peuvent lui être faites.
%
\begin{itemize}
  \item
    La vue~$\thisview$ du fil courant doit être explicitée
    et il faut constamment s’assurer de sa validité,
    condition de bord qui apparaît dans la plupart des préconditions et postconditions
    de nos règles de raisonnement (\fref{fig:llog:rules:mem}).
    On aimerait rendre tout cela implicite.
  \item
    L’assertion $\histNA[q]\nacell\hist$ qui représente une case non-atomique~$\nacell$
    expose le fait que cette case contienne un historique~$\hist$ plutôt qu’une valeur unique.
    Les règles \basenaread et \basenawrite paraphrasent la sémantique opérationnelle
    dans toute sa technicité, qui fait intervenir la notion d’horodatage.
    Il reste ainsi difficile de raisonner à propos d’une case non-atomique.
    Pourtant, en l’absence de courses de données sur cette case,
    on aimerait pouvoir raisonner plus naturellement,
    comme si la case contenait une valeur unique.
\end{itemize}

\section*{\hlog, une logique de plus haut niveau}

On exauce maintenant ces deux souhaits
en construisant,
sur notre logique de bas niveau \llog,
une logique de plus haut niveau baptisée \hlog.
%
Dans \hlog, l’assertion qui représente une case non-atomique
revêt l’apparence traditionnelle
$\pointstoNA[q] \nacell \val$
comme dans un cadre séquentiellement cohérent.
%
Cette assertion garantit que lire $\nacell$ renverra la valeur~$\val$.
L’assertion signifie donc, intuitivement,
que la case~$\nacell$ contient la valeur~$\val$
\emph{aux yeux du fil courant}.
En termes plus techniques, l’assertion garantit que
le plus récent événement d’écriture dans~$\nacell$
est une écriture de la valeur~$\val$
et que le fil courant connaît cette écriture.

En cas de course de données sur une case non-atomique,
l’hypothèse sur la vue courante n’est pas vérifiée;
ainsi, représenter une case non-atomique par cette assertion simplifiée
empêche de raisonner à propos de certains programmes.
Nous croyons raisonnable de supposer que la plupart des programmes (corrects)
sont exempts de telles courses de données,
de sorte que cette perte de généralité constitue rarement un problème.

La signification de l’assertion $\pointstoNA[q] \nacell \val$
est relative au fil courant.
Plus précisément, elle dépend de sa vue, puisqu’elle affirme
que ladite vue contient un certain événement d’écriture.
%
Pour donner un sens à cette assertion simplifiée,
les assertions de notre logique doivent donc être paramétrées par une vue.
La vue à laquelle s’applique une assertion \hlog
est la vue du possesseur de cette assertion.
%
Ce changement de perspective résout les deux objections que nous avons formulées plus haut.

Un utilisateur de \hlog n’a pas besoin de savoir
comment cette logique est construite au-dessus de \llog:
ni l’implémentation des vues, ni celle des assertions de \hlog;
les règles de raisonnement de \hlog peuvent lui être présentées directement.
Toutefois, une preuve dans \hlog donne une preuve dans \llog,
il est donc aisé de combiner des efforts de preuve conduits dans les deux logiques,
si jamais \hlog n’était pas assez expressif.

\subsection*{Subjectivité}

La construction de \hlog par-dessus \llog est montrée dans la \fref{fig:hlog:assertions}.
%
Comme suggéré,
une assertion de \hlog est un prédicat (de type $\typeVProp$) qui,
à une vue, associe une assertion de \llog.
%
Entre autres pour garantir la sûreté de la règle de passage au contexte (\hoareslframe) dans \hlog,
on impose \citep{kaiser-17,dang-20}
que ce prédicat soit \emph{croissant},
les vues étant ordonnées point-à-point~$\viewleq$ par l’ordre sur les horodatages
(une vue plus grande est plus récente, ou mieux informée, et contraint davantage
les comportements possibles lors d’un accès non-atomique)
et les assertions de \llog par l’implication.
%
On porte dans \hlog tous les connecteurs logiques de \llog.
%
On a également un moyen simple de convertir une assertion~$\prop$ de \llog
en une assertion $\liftobj\prop$ de \hlog, qui consiste à ignorer la vue donnée en paramètre.
%
Ainsi, \hlog contient \llog.
%
Une assertion de la forme $\liftobj\prop$ est dite \emph{objective},
en ce que sa validité ne dépend pas de la vue courante.

L’apport de \hlog est justement de permettre de se référer à la vue courante.
Pour cela, on introduit une assertion de la forme $\seen\view$
qui affirme simplement que la vue courante, qui reste implicite, contient la vue~$\view$.
%
Cette assertion est donc éminemment \emph{subjective}.
%
On l’utilise par exemple pour définir notre assertion simplifiée
$\pointstoNA[q]\nacell\val$ (\fref{fig:hlog:assertions}).
%
En fait, plus généralement, les assertions de la forme $\seen\view$
suffisent à exprimer n’importe quelle assertion subjective.
En effet
(grâce à la croissance des assertions \hlog vis-à-vis de la vue courante),
pour affirmer un fait à propos de la vue courante,
il suffit d’en capturer une sous-vue, c’est-à-dire un~$\view$ tel que $\seen\view$,
et de formuler le fait désiré à propos de~$\view$
au moyen d’une assertion objective de \llog.
%
Cette idée est formalisée dans la règle de raisonnement \splitso (\fref{fig:hlog:rules:views}).
Pour écrire cette règle, on introduit une assertion de la forme
$\prop \opat \view$ qui,
étant donnée une assertion arbitraire~$\prop$ de \hlog et une vue~$\view$,
exprime l’assertion $\prop$
dans laquelle la vue courante a été remplacée par la vue~$\view$;
cette assertion ne dépend plus de la vue courante et est donc objective
(autrement dit, c’est une assertion de \llog).

En \hlog, plutôt que de cacher les vues,
on fait le choix d’en faire une notion de base de la logique.
On peut alors se demander ce qu’on y gagne par rapport à \llog.
%
Premièrement, en tant qu’utilisateur de la logique,
il n’est plus nécessaire de savoir qu’une vue est une fonction
des cases non-atomiques vers les horodatages;
la notion d’horodatage est escamotée.
Les vues peuvent être rendues abstraites du point de vue de l’utilisateur,
qui doit seulement savoir qu’elles sont munies d’une relation d’ordre
---~en fait, d’une structure de demi-treillis borné.
%
Deuxièmement, grâce à la croissance des assertions vis-à-vis de la vue courante,
on n’a jamais besoin de garder trace
de la vue \emph{exacte} d’un fil,
ou de la vue exacte qui est emmagasinée dans une case atomique:
on peut se contenter d’une sous-vue qui contient la connaissance pertinente.
%
On peut rapprocher cette idée de celle à l’origine de la logique de séparation:
en logique de séparation, grâce à la règle~\hoareslframe,
on peut limiter l’empreinte mémoire d’une spécification
à la portion pertinente de la mémoire,
et ne pas mentionner le reste,
ce qui offre un cadre plus modulaire pour la vérification.
%
De même en \hlog, on n’a pas à se soucier des événements d’écriture
hors des zones de la mémoire qui nous intéressent.
Dans le cas le plus extrême,
lorsqu’on n’est pas intéressé par la synchronisation entre fils
(par exemple lorsqu’on vérifie du code séquentiel),
on peut se contenter de vues vides~$\emptyview$,
ce qui revient à omettre complètement les vues,
et donne comme un cas particulier de \hlog
une logique de programme identique
à celle qu’on aurait dans un cadre séquentiellement cohérent.

\subsection*{Preuve de programme en \hlog}

Pour pouvoir spécifier et vérifier des programmes
dans cette logique de séparation avec vues,
on porte (\sref{sec:hlog:triples}) les triplets de \llog
en des triplets $\hoare\prop\expr\pred$,
où un fil est représenté simplement par son expression~$\expr$
et où les précondition et postcondition sont des assertions de \hlog
($\prop : \typeVProp$ et $\pred : \typeVal \ra \typeVProp$).
La définition rend implicite la vue courante~$\thisview$ du fil
et les conditions de bord concernant sa validité.
%
Les triplets dans \hlog satisfont encore un théorème d’adéquation
(\sref{sec:hlog:soundness}).
%
On peut ensuite porter dans \hlog
les spécifications de chaque opération du langage
qu’on avait établies dans \llog
(Figures~\ref{fig:llog:rules:nomem} et\ \ref{fig:llog:rules:mem}).
La \fref{fig:hlog:rules} montre les règles de raisonnement obtenues.
%
La vue du fil avant exécution ($\thisview$) et après ($\thisview'$)
n’est plus explicitée,
ni les conditions de validité associées.
Une assertion de la forme $\seen\view$ est employée
en précondition quand le fil déverse sa vue courante dans une case atomique,
ou en postcondition quand le fil ajoute à sa vue courante
de nouvelles informations obtenues en lisant une case atomique.

Les opérations sur les cases non-atomiques (\naread et \nawrite) sont spécifiées
avec l’assertion simplifiée $\pointstoNA[q]\nacell\val$
plutôt qu’avec l’assertion d’historique $\liftobj{\histNA[q]\nacell\hist}$.
Ces règles de raisonnement sont donc moins générales que celles de \llog{}
---~elles ne permettent pas de raisonner à propos de
courses de données sur les cases non-atomiques~---
mais, en contrepartie, semblent bien plus naturelle:
en effet, elles sont identiques en apparence
aux règles d’accès qu’on aurait dans un modèle séquentiellement cohérent.
La différence réside dans la nature de l’assertion \EN{points-to}:
en \hlog, l’assertion qui représente une case non-atomique est subjective.

Comme mentionné plus tôt, les règles de raisonnement se simplifient
quand on n’est pas intéressé par la synchronisation:
dans les règles gouvernant les opérations sur les cases atomiques
(\atread, \atwrite, \casfailure, \cassuccess),
on peut prendre pour chacune des vues mentionnées ($\view$ et $\view'$)
la vue vide~$\emptyview$,
et l’on obtient alors les règles simplifiées
montrées dans la \fref{fig:hlog:rules:memnoview}.
Ces règles sont exactement celles qui gouvernent
une case mémoire séquentiellement cohérente,
d’autant plus que l’assertion qui représente une case atomique,
elle, est bien objective.

La synchronisation effectuée par les accès atomiques,
telle que décrite par les règles non simplifiées,
est de type relâchement/acquisition (\EN{release/acquire}).
%
Elle permet la transmission d’une vue
depuis un premier fil vers un second.
En exploitant ce mécanisme conjointement à la règle \splitso
et à l’utilisation d’un invariant Iris,
on peut réaliser le transfert d’une assertion~$\prop$ arbitraire,
même lorsque l’empreinte de $\prop$ contient des cases non-atomiques:
l’invariant Iris
transfère le support objectif~$\prop \opat \view$ de l’assertion~$\prop$,
tandis que la case atomique
transmet la connaissance~$\seen\view$ des événements d’écriture utiles.
%
Il s’agit d’un mécanisme-clé, qu’on utilisera dans toutes nos études de cas.
%
Il faut noter que lorsque $\prop$ est subjectif,
on ne peut pas se contenter d’un invariant Iris
pour effectuer le transfert:
en effet, une ressource mise dans un invariant Iris
est rendue disponible pour tous les fils sans distinction,
et ne peut donc pas dépendre de la vue d’un fil donné;
dans \hlog les invariants Iris sont donc par nature
restreints aux assertions objectives.
On retrouve donc dans la logique
la nécessité d’effectuer une synchronisation.

\section*{Études de cas}

\subsection*{Verrous et exclusion mutuelle}

Pour éprouver notre logique de programme \hlog,
cette thèse s’attache ensuite à
vérifier quelques structures de données concurrentes simples:
%
un verrou tournant (\sref{sec:examples:spinlock}),
un verrou à ticket (\sref{sec:examples:ticketlock}),
les algorithmes d’exclusion mutuelle de Dekker (\sref{sec:examples:dekker})
et de Peterson (\sref{sec:examples:peterson}).

Chacune de ces études de cas met en œuvre de façon cruciale
la méthode décrite précédemment pour transférer une assertion
au moyen de la règle \splitso.
%
Les preuves de ces structures de données en \hlog
calquent les preuves qui auraient été faites dans un cadre séquentiellement cohérent,
auxquelles on ajoute des vues (abstraites)
qui matérialisent l’information transmise d’un fil à un autre,
lorsqu’il est nécessaire de préciser les synchronisations qui ont lieu.
Ainsi, nous croyons que \hlog offre une façon
à la fois expressive et naturelle
de vérifier des programmes \mocaml,
malgré la subtilité du modèle mémoire faible.

Ces exemples illustrent une autre force de \hlog:
on a déjà vu que la logique offre un fragment sans vues,
dans lequel le raisonnement est indépendant du modèle mémoire,
c’est-à-dire où les règles
sont celles de la Logique de Séparation Concurrente traditionnelle.
%
Ce fragment comporte les règles relatives aux cases non-atomiques.
%
Les structures de données concurrentes étudiées ici,
dont la correction est vérifiée avec \hlog,
ont des spécifications (Figures~\ref{fig:lock:spec} et \ref{fig:mutualexcl:spec})
qui s’inscrivent également dans ce fragment indépendant du modèle mémoire:
la spécification d’un verrou en \hlog (\fref{fig:lock:spec})
est la spécification traditionnelle en Logique de Séparation Concurrente
\citep{gotsman-aplas-07,hobor-oracle-08}. % buisse-11,sieczkowski-15
%
Ainsi, \hlog contient la Logique de Séparation Concurrente traditionnelle,
et permet de raisonner à propos de programmes concurrents à gros grains
où tous les accès à des objets partagés sont protégés par des verrous.
%
Ces verrous eux-mêmes sont implémentés en mémoire faible
et leur vérification en \hlog dépend du modèle mémoire,
mais leur utilisation en est indépendante.

Par ailleurs, l’exemple de l’algorithme de Peterson
illustre l’intérêt d’avoir réduit
la subjectivité des assertions de \hlog
à une classe simple d’assertions $\seen\view$ qui sont \emph{persistantes},
c’est-à-dire qui n’assument la possession d’aucune ressource.
Dans l’algorithme de Peterson en effet,
la synchronisation nécessaire pour transférer la ressource protégée par le verrou
se fait via l’une de deux cases atomiques distinctes,
mais on ne sait pas encore laquelle sera utilisée par la prochaine acquisition
au moment où la ressource est relâchée.

\subsection*{Une file concurrente multi-producteurs multi-consommateurs}

Afin de démontrer l’applicabilité de \hlog
à la vérification modulaire de bibliothèques plus complexes,
la dernière contribution de cette thèse est
la spécification et la vérification en \hlog
d’une structure de données concurrente non triviale:
une file multi-producteurs multi-consommateurs
(Chapitre~\ref{sec:queue}).
%
Spécifier une telle structure de données (\sref{sec:queue:spec})
soulève des questions intéressantes
et démontre l’expressivité de notre logique.

Pour commencer, la spécification indique que la file se comporte
comme si toutes ses opérations agissaient atomiquement sur un état commun
quand, en réalité, elles accèdent à des parties distinctes de la mémoire
et requièrent de nombreuses étapes de calcul.
On utilise pour cela le concept d’\emph{atomicité logique}
\citep{iris-15,jung-slides-2019,da2014tada}
transposé dans \hlog.
Il s’agit, à notre connaissance,
de la première utilisation de l’atomicité logique
dans un modèle mémoire faible.
Il faut noter que dans ce cadre, l’atomicité logique
n’implique pas la « linéarisabilité » au sens traditionnel:
elle implique bien
l’existence d’un historique linéaire des opérations de la structure de données,
mais ne garantit pas automatiquement
de synchronisation entre opérations successives.

Or, dans un modèle mémoire faible,
la spécification d’une structure de données
doit décrire non seulement le résultat de ses opérations,
mais également la façon dont celles-ci se synchronisent.
%
Cette information supplémentaire permet de raisonner
à propos d’accès à des zones de la mémoire
hors la structure de données elle-même.
%
Dans le cas d’une file,
qui peut être utilisée pour transférer une donnée en mémoire d’un producteur à un consommateur,
il est crucial que l’enfilage d’un élément se synchronise avec le défilage du même élément.
De façon intéressante, la spécification qu’on donne
(\fref{fig:queue:spec:weak})
garantit certaines synchronisations
---~indépendamment de l’implémentation~---
mais elle en laisse d’autres non spécifiées.
Par exemple, il n’est pas requis
qu’un défilage se synchronise avec un enfilage ultérieur.
%
Bien que toutes les opérations de la file
se comportent comme si elles étaient atomiques,
et s’ordonnent dans un historique linéaire,
on ne garantit pas
que tout couple d’opérations consécutives dans cet historique se synchronise.
%
Alors que dans une implémentation à gros grains,
le verrou induirait des synchronisations entre toutes les opérations,
notre spécification faible permet des implémentations à grains fins,
plus relâchées et donc plus efficaces.
%
Finalement, l’expressivité de \hlog permet d’écrire une spécification
plus lâche que celle qui aurait été écrite dans un formalisme par raffinement.
%
L’implémentation qu’on vérifie (\sref{sec:queue:impl})
tire parti de ce relâchement.

Par ailleurs, pour s’assurer que cette spécification faible
est suffisante pour un client de la file concurrente,
on vérifie une application simple
qui applique en parallèle une chaîne de traitements à un flux de données,
en utilisant une file pour transmettre les valeurs intermédiaires
d’un traitement au suivant
(\sref{sec:pipeline}).

Alors même que l’implémentation de la file
exploite le modèle mémoire faible de \mocaml
et que sa correction est vérifiée avec \hlog,
sa spécification n’est pas liée aux détails du modèle mémoire:
elle se suffit à elle-même.
%
Au moyen de vues abstraites
qui matérialisent la notion empirique de connaissance sur l’état partagé,
la spécification explique à l’utilisateur
les synchronisations effectuées par la file
sans qu’il soit besoin de connaître les primitives sous-jacentes de \mocaml,
ni le modèle mémoire qui s’y attache.

\end{otherlanguage}
