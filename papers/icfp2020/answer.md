First of all, we would like to thank the reviewers for their detailed, helpful
and indulgent reviews. In particular, we apologize for our mistake regarding
the page format. We discovered our mistake shortly after the deadline and
reformatted the paper without editing it, which explains the poor figure
placement and the missing related work. The reviewers' comments will greatly
help improve the paper.

Reviewer A points out that we follow the "Iris recipe", and asks about the
benefits of our work. Reviewer B asks about the conceptual contributions of
our work.

* Iris is still relatively new, so that instantiating it with languages with
  nonstandard features (such as a weak memory model) is still a challenge.
  Compared with previous work on other memory models, we claim that our
  abstractions are simpler, and easier to use. We discovered that some
  notions, which are not specific of the Multicore OCaml memory model, can be
  useful to an end-user of the high-level logic. These include the notion of
  objective assertion, the construct `P@V`, the rule `P <=> exists V.(P@V *
  ↑V)`, and objective invariants. These could be reused in logics for other
  memory models.

Reviewer A points out "a change in semantics (wrt races on nonatomic
locations and invariants)" and asks if we could comment on it.

* We should clarify that there is no change in semantics between BaseCosmo and
  Cosmo. Instead, Cosmo offers rules that are much simpler and closer to
  traditional CSL, at the cost of restricting one's attention to code that
  does race on nonatomic locations. Nevertheless, as Reviewer C points out, it
  is still possible to escape from Cosmo to BaseCosmo for the more subtle
  parts of the program. Finally, it is important to note that invariants are
  still available in Cosmo, though restricted to objective assertions.

Reviewer A points out that "the sequencing of concepts in Section 2 is
far from optimal".

*  We will try to provide a better explanation, following your
   suggestions.

Reviewer A asks: "why is it necessary for atomic locations to store a
view?"

* In the Multicore OCaml memory model, atomic locations have two mostly
  independent features: first, they exhibit SC behavior: this is reflected in
  the operational semantics by the fact that they store a single value.
  Second, they establish happens-before relationships, in the style of C11
  release-acquire accesses: this is reflect in by the fact that they store a
  view.

Reviewer B asks about the "design decisions, their motivation, or specific
challenges of Multicore OCaml." (S)he also asks "an explanation of
[the release/acquire semantics of reads/writes]", and for "an explanation of
the "SC" versions of the rules". Reviewer B notes that "Iris notation is hard
to understand". Reviewer C writes that the paper is "too technical to have
much to offer to [non-experts]."

* Thanks to the reviewers' remarks, we will do our best to include more
  non-technical content, including motivation for design decisions and
  challenges. Nevertheless, we point out that the apparent technicality of
  this paper is the price we pay for making the paper self-contained: that is,
  we have not relegated the technical content to an appendix, as is often the
  case of papers in this domain.

Reviewer B notes that "it would be nice to include other concurrent data
structures".

* We do agree, and we are currently working on more examples. However,
  this is still future work.

(This part of the response is 566 words. The rest is more technical and does not have to be read. Thank you!)

### Answers to some of the reviewers' technical questions

#### Questions from Reviewer A:

L157: Why do threads practically not return a value? Presumably because
threading model is not fork-join?

* Yes. (Fork-join can be implemented on top of this model, using atomic storage.)

L184: The almost everywhere is defined in terms of a finite set of locations,
but this is always true for an execution at any point in time. What is the
precise definition of a view?

* The set `LocNA` of nonatomic locations and the set `View` of views are
  defined prior to any execution trace. Hence the set `LocNA` of nonatomic
  locations must be infinite, even though an execution uses only a finite
  subset of it. This is required so that an a priori unlimited number of
  locations can be used.

  We define *views* as total functions from nonatomic locations to timestamps.
  Therefore, the domain of such a function is infinite. However, there is no
  point in allowing views to associate a non-zero timestamp to an infinite
  number of locations (those views would be unreachable).

  Hence we restrict our notion of views. We define the set `View` as follows:

      View := { V ∈ (LocNA → Time) | #{ a ∈ LocNA | V(a) ≠ 0 } < ∞ }

      View := { V ∈ (LocNA → Time) | ∃ S ⊂ LocNA, #S < ∞ ∧ V(LocNA \ S) = {0} }

  With this restriction, views are finite objects, which is of interest for doing
  constructive mathematics in Coq.

L210: View inequality and the semi-lattice definition seems to rely on the
fact that the set of locations is the same for any two views. If my
understanding is correct, how does this relate to location allocation?

* With the definition given above, all views are defined on the same domain
  `LocNA`.

Fig3, MEM-NA-ALLOC: […] since the view W remains the same due to the event,
I assume that W is defined such as to account for all possible locations that
ever come into existence. Is that correct?

* Indeed. As seen above, W associates the timestamp 0 to any location that has not
  yet been allocated. Recall that for a given location the minimum timestamp
  is 0, which corresponds to its initial write event. (This means we have no way
  of talking about a location being in an uninitialized state. Locations are
  initialized as soon as they are allocated, as per the operational semantics).

L262: "no requirement for t to be greater than all time stamps in the domain
of h" -> I find that really confusing, especially because of the remark on
L210 regarding the partial order on time stamps. How is this order then
defined?

* Timestamps are nonnegative rational numbers; the (total) order on timestamps
  is simply the order on numbers.

  The order on timestamps does not reflect the order on which writes
  physically happen in the hardware, nor in the interleaved execution trace.
  Rather, it defines which write event will override the others when all
  events are eventually propagated to all threads. In that sense, it is an "a
  posteriori" numbering of write events.

L268: Can you provide an intuition for why it is appropriate to do this merge?
Same for L270.

* A greater view (with respect to the ordering relation on views)
  means that the thread sees more write events,
  therefore has more information.
  The view-merge operations in the operational semantics can be
  understood as transfers from information (from a thread to an
  atomic memory location, when writing; and from a location to
  a thread, when reading).

#### Questions from Reviewer B

Finally, all the case studies in the paper are locks. It would be nice to
include other concurrent data structures, but also include some examples of
client code that is able to use the simple SC versions of rules.

* The ticket lock example uses some of the simpler "SC" rules. Indeed, as
  stated in that section, the view stored in the location `next` is
  irrelevant, hence all accesses to that location can be reasoned about using
  the simpler rules. It is true however that the proof is omitted from the
  paper.

In the SpinLock example, can you explain why it is okay for any thread to
release the lock, not just the one holding it?

* It is okay because the implementation of `release` consists in only one
  atomic operation, hence leaves no opportunity for data races on the lock
  itself. One can even call `release` when the lock is already unlocked, this
  is simply a no-op. It is very peculiar to the spin lock.

  Whoever calls `release` must still provide the lock invariant `P`, so for
  the lock to be released by some thread other than the one who acquired the
  lock, it is necessary that there exist several copies of the assertion `P`.
  In practice, this is rarely the case; `P` is usually precise.

I'm confused about the definition of `isTicketLock` in Fig 14. The last line
seem to say that it's either locked and the invariant holds, or someone owns
a ticket. But I thought the invariant is supposed to hold exactly when the
lock is available (i.e. not locked)?

* The confusion seems to come from the name of the token `locked`. When
  this token is owned by a thread, this means that the lock is locked,
  and this thread has permission to release it.
  However, when this token is owned by the lock itself, this means that
  no thread has taken the lock, so the lock is available.

#### Questions from Reviewer C

Page 12: In the postcondition of the base-at-read rule, I think
`A ~>_at (v, V)` was supposed to be `q . A ~>_at (v, V)`. […]

* Indeed.

Page 20, lines 909 and 910: If I am understanding properly, both
instances of (true, V) should be (false, V).

* Correct (on that same line, `↑V` should be `P @ V`).

Page 21: I didn't follow the discussion of the ticket lock algorithm:
The paper says "locked" represents the permission to release the lock,
and "ticket n" represents ownership of the ticket numbered n.  I don't
see how locked and ticket give you any useful information at all.
Served always contains something, so isn't it always the case that one
can have served contains fragmentary s for some s?  (Similarly for
ticket, at long as at least one ticket has been issued.)

* The ghost variable `issued` stores the set of all tickets issued so far. The
  composition law on this ghost variable is that of disjoint sets, so the
  predicate `ticket n`, being defined as a singleton fragment `{n}` of
  `issued`, effectively ensures that we are the unique holder of ticket number
  `n`.

  To acquire the lock, a thread has to switch the disjunction from the left
  branch (unlocked state) to the right branch (locked state); for that it must
  provide the ticket number `s`, where `s` is the number being served, and we
  have just seen that tickets are unique and unforgeable. So only the rightful
  ticket owner can claim the lock.

I also did not understand how one can rule out being in the second disjunct in
isTicketLock when wait finished.  Probably that's a result of my not
understanding locked and ticket.

* Conversely, when a thread holds ticket number `s`, then, because there is no
  other ticket by the same number, the thread can rule out the possibility
  that the second disjunct of the invariant is in force.

Page 21, line 965: What is Ex?

* This is Iris notation.

  When acquiring the lock you receive the token `locked`. It is also unique as
  it is defined as a fragment of the ghost variable `served`, which holds an
  "exclusive" number (an element of the "monoid" Ex(ℕ), ie. a number that cannot
  be split into smaller fragments).

  The invariant ties the value s of the memory location `served` with that of
  the ghost variable `served`, so updating the former can only be done while
  updating the latter at the same time. For that you need the token `locked`.
  Hence, you can only call `release` when you hold the token `locked`.

Page 23: There's something I didn't understand about Peterson's algorithm.
Why do you need a separate argument for the second loop exit at all?  (Line
1068.)  You already have `uparrow(V_{1-i})`; why do you lose it if you go on to
read the turn?

* (There is a typo on L1070, it should be `V_{1-i}`.)

  When reading `flag_{1-i}` in the first operand of `&&`, you get some view `VA`
  which, at that point, you know is equal to the view `V_{1-i}` of the invariant,
  which is stored in the atomic location `flag_{1-i}`.

  However, by the time you perform another operation (reading `turn`), another
  thread might have written to `flag_{1-i}` and changed the view `V_{1-i}` of
  the invariant. So you don't know anything anymore about `VA`. It has
  outlived its usefulness.
