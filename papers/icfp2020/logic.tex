\section{A Higher-Level Logic: \hlog}
\label{sec:logic:hi}

The program logic that we have just presented, \llog, is
definitely low-level, as it exposes the details of the \mocaml memory model.
Several aspects of it can be criticized:
%
\begin{enumerate}
\item
\label{enum:history}
The assertion $\histNA[q]\naloc\hist$ exposes the fact that a nonatomic memory
location stores a history~$\hist$, as opposed to a single value~$\val$. The
axioms \basenaread and \basenawrite paraphrase the operational semantics and
reveal the time stamp machinery. This makes it difficult to reason
% in a lightweight and natural manner
about nonatomic memory locations. Yet, at least
in the absence of data races on these locations, one would like to reason in a
simpler way. Is it possible to offer higher-level points-to assertions and
axioms, so that a nonatomic location appears to store a single value?
\item
\label{enum:explicitview}
The view~$\thisview$ of the current thread is explicitly named in every triple
$\hoare\prop{\mkexpr\expr\thisview}\pred$, and its validity is typically
explicitly asserted as part of every pre- and postcondition. This seems heavy.
Is it possible to make this view everywhere implicit by default, and to have a
way of referring to it only where needed?
\end{enumerate}

In this section, we answer these questions in the affirmative. On top of the
low-level logic \llog, we build a higher-level logic, \hlog, where the points-to
assertion for nonatomic locations takes the traditional form $\pointstoNA[q]
\naloc \val$, as in \CSL with fractional
permissions~\cite{boyland-fractions-03,bornat-permission-accounting-05}. This
assertion is strong enough to guarantee that reading $\naloc$ will yield the
value $\val$. Therefore, at an intuitive level, one can take it to mean that
``the location $\naloc$ currently contains $\val$'', or slightly more
accurately, ``in~the eyes of this thread, the location $\naloc$ currently
contains $\val$''. In more technical terms, this assertion guarantees that
\emph{the most recent write to the location~$\naloc$ is a write of the
  value~$\val$} and that \emph{this thread is aware of this write}.

The meaning of this simplified points-to assertion is relative to ``this
thread'', that is, to a certain thread about which one is presently reasoning.
More precisely, its meaning depends on ``this thread's view'', as the
assertion claims that a certain write event is part of this thread's view.
Therefore, to give meaning to this simplified points-to
assertion, we find that we must parameterize every assertion with a view: in
other words, a~\hlog assertion must denote \emph{a function of a view to a
  \llog assertion}. This change in perspective not only seems required in
order to address item~\ref{enum:history} above, but also addresses
item~\ref{enum:explicitview} at the same time.
%
% We could admittedly solve item~1 while keeping this view explicit;
% this would require parameterizing the ``simplified'' points-to
% predicate with this thread's view. But that would lead to a nightmare,
% as every points-to assertion at hand would need to be updated every
% time this thread's view changes.
%

Following \citet{kaiser-17} and \citet{dang-20}, we require every \hlog
assertion to denote a \emph{monotone} function with respect to the information
ordering~$\viewleq$ (\sref{sec:lang:model:views}).
%
This guarantees that, as new memory events become visible to this thread, the
validity of every assertion is preserved. This condition makes the frame rule sound at the level of \hlog.
% fp : I think it is the rule
% $\prop \isep \WP\expr\predB \entails \WP\expr{\prop\isep\predB}$.
% Do we prove this rule somewhere in Coq?
% Do we give it somewhere in the paper?
% jh : Il s'agit de wp_frame_l, prouvé dans weakestpre.v. Avec Glen,
% on pense que ça ne vaut pas la peine d'utiliser de la place pour
% ça. Tout le monde sait ce qu'est la règle frame !
In summary, the type $\typeVProp$ of \hlog assertions is defined as the type
of monotone functions of $\typeView$ to $\typeIProp$
(\fref{fig:hlog:assertions}).

In this section, we describe how \hlog is constructed on top of \llog. A~user
of \hlog need not be aware of this construction: the assertions and reasoning
rules of \hlog can be presented to her directly.
Nevertheless, a \hlog proof is a \llog proof, and it is
therefore easy to combine proofs carried in the two logics,
should \hlog alone not be expressive enough.

% ------------------------------------------------------------------------------

\subsection{\hlog: Syntax and Meaning of Assertions}
\label{sec:logic:hi:assertions}

\input{figure-hlog-assertions}

\fref{fig:hlog:assertions} defines a number of ways of constructing \hlog
assertions. The assertion $\seen\view$, pronounced ``\emph{I~have~$\view$}'',
asserts that this thread's view contains the view~$\view$. It is defined as
the function $\Lam\thisview. \view \viewleq \thisview$, where the formal
parameter $\thisview$ represents this thread's view. It is easy to see that
this is a monotone function of~$\view$. The assertion $\seen\view$ is the
archetypical example of a \emph{subjective} assertion, that is, an assertion
whose meaning depends on this thread's view.
%
% The assertion $\seen\view$ is persistent~\cite{iris}.
% fp: Nous n'avons pas défini ``persistent'' pour vProp.
%     Est-ce que P est persistent si quel que soit W, P@W est persistent?
%     On pourrait dire plus fort: $\seen\view$ est non seulement persistente,
%     mais pure.
It satisfies the first two axioms in \fref{fig:hlog:axioms}. \seenzero{}
states that to have nothing is the same as to have the empty view. \seentwo{}
states that to have two views $\view_1$ and~$\view_2$ separately is the same
as to have their join $\view_1 \viewjoin \view_2$. This implies that the
assertion $\seen\view$ is duplicable and that it is anti-monotone with respect
to~$\view$: to have a larger view implies to have a smaller view.

The nature of views is hidden from a user of \hlog. That
is, views can be presented to the user as an abstract type, equipped with a
distinguished element $\emptyview$ and with a join operation~$\viewjoin$ that is
associative, commutative, idempotent, and admits $\emptyview$ as a unit. This
means that the user need not (and must not) think of views as ``functions of
nonatomic locations to time stamps'', or as ``sets of write events''. Instead,
the user should think of a view as a certain amount of ``information'' about
the (nonatomic part of the) shared memory. The deduction rules of the logic
allow the user to reason abstractly about the manner in which this information
is acquired and transmitted
% e.g. via reads and writes of atomic locations
and about the places where it is needed.
% e.g. when P@V and \seen{V} must be combined to yield P (not yet explained)

The next four lines in \fref{fig:hlog:assertions} lift the standard
connectives of \SL from \llog up to \hlog. In the definition of the magic
wand, a universal quantification over a future view~$\view$ that contains
$\thisview$ is used so as to obtain a monotone function of $\thisview$; this
is a standard technique.

The following line defines how to lift a \llog assertion $\prop$ up to a \hlog
assertion $\liftobj\prop$. This provides a means of communication between
\llog and \hlog. The definition is simple: $\liftobj\prop$ is the constant
function $\Lam\thisview.\prop$. It is the archetypical example of
an \emph{objective} assertion, that is, an assertion whose meaning is
independent of this thread's view.
%
We often write just $\prop$ for $\liftobj\prop$.

The last line in \fref{fig:hlog:assertions} (left) defines $\prop \opat \view$ as
sugar for the function application $\prop(\view)$. This provides a means of
communication in the reverse direction: if $\prop$ is a \hlog assertion, then
$\prop \opat \view$ is a \llog assertion, which can be read as ``\emph{$\prop$
  holds at $\view$}''.
%
% fp: c'est dit plus bas:
% As per the previous paragraph, if desired, this assertion can be lifted back
% up to the level of \hlog: if $\prop$ is a \hlog assertion, then
% $\liftobj{\prop\opat\view}$ is also a \hlog assertion,
% usually written just $\prop\opat\view$.

Thanks to the constructs that have just been introduced, it is possible to
express the idea that every \hlog assertion~$\prop$ can be split into a subjective
component and an objective component. This is stated by the axiom \splitso{} in
\fref{fig:hlog:axioms}. When read from left to right, the axiom splits $\prop$
into a subjective component $\seen\view$ and an objective component
$\prop\opat\view$, for some view $\view$. (The witness for $\view$
is this thread's view at the time of splitting.) When read from right to left,
the axioms reunites these components and yields $\prop$ again.
% the axiom states that if $\prop$ holds in the eyes of whomever has observed
% the events in $\view$, and if this thread has observed all of the events in
% $\view$, then $\prop$ holds in the eyes of this thread.
% This relies on the fact that $\prop$ must be a monotone function of views to
% \llog assertions.

Here are several typical examples of \llog assertions that can be usefully
lifted up, yielding objective \hlog assertions. (In each case, we omit the
brackets $\liftobj\cdot$.)
%
\begin{itemize}
\item A \hlog assertion at a fixed view $\prop \opat \view$.
\item The fractional ownership of an atomic memory location
      $\pointstoAT[q] \atloc {\mkval\val\view}$.
\item The ownership of a piece of ghost state $\ownGhost{\gname}{m}$.
\item The knowledge of an invariant $\smash{\knowInv{}{I}}$.
\end{itemize}
%
% In the first case above, we find that $\liftobj{\prop \opat \view}$, usually
% written just $\prop \opat \view$, is a \hlog assertion. Let us emphasize that
% this is an objective assertion: because $\prop$ is explicitly evaluated at the
% view~$\view$, the meaning of $\prop \opat \view$ is independent of this
% thread's view, represented by the implicit parameter $\thisview$.
%
% Du coup, on a P@V1@V2 = P@V1, etc.
%
%

We emphasize that, in the last case above, $\knowInv{}{I}$ is an ordinary
\llog assertion. This means that the assertion~$I$ that appears inside the box
must also be a \llog assertion. That is, the logic \hlog is subject to a
restriction: \emph{in every invariant $\knowInv{}{I}$, the assertion $I$ must
  be objective.} A~subjective assertion, such as $\seen\view$, cannot appear
in an invariant. In short, because an invariant represents knowledge that is
shared by all threads, it cannot depend on some thread's view.

At first, this restriction might seem problematic: in general, an arbitrary
\hlog assertion~$\prop$ cannot appear in an invariant, therefore cannot be
transmitted from one thread to another. Fortunately, in many situations, it is
possible to work around this limitation. The idea is to
decompose $\prop$ into a subjective part and an objective part, via
\splitso{}; to let the objective part appear in the invariant, thereby
allowing it to be shared between threads; and to transmit the subjective part
via explicit synchronization operations, typically writes and reads of atomic
locations.
%
% The invariant must be of the form
% $\Exists\view.(\ldots \isep \prop\opat\view)$,
% where the assertion $\ldots$
% must somehow relate the view $\view$
% with a view that is stored in an atomic location...
% -- mais ça semble un peu trop compliqué pour l'expliquer ici.
%
Our spin lock implementation (\sref{sec:examples:spinlock})
offers a typical example of this idiom.

The last key definition is that of the nonatomic points-to assertion
(\fref{fig:hlog:assertions}, right). As announced earlier, this
assertion takes the traditional form $\pointstoNA[q] \naloc \val$, and means
that \emph{the most recent write to the location~$\naloc$ is a~write of the
  value~$\val$} and that \emph{this thread is aware of this write}. Its
definition, whose right-hand side is a conjunction of four assertions,
reflects this:
\begin{enumerate}
\item
  % The conjunct
  $\histNA[q]{\naloc}{\hist}$
  claims
  % ownership of
  a fraction $q$ of the location~$\naloc$
  and guarantees that its history is $\hist$.
\item
  % The conjunct
  $\timest = \max\;(\Dom\hist)$
  guarantees that $\timest$ is the time stamp of the most recent write event
  at~$\naloc$.
\item
  % The conjunct
  $\hist(\timest) = \val$
  indicates that $\val$ is the value written by this write event.
\item
  % The conjunct
  $\Exists \view.\;
    \liftobj{\viewapp\view\naloc = \timest} % pure, therefore lifted twice
    \isep
    \seen \view$
  guarantees that
  % this thread's view includes this write event.
  this write event is visible to this thread.
\end{enumerate}
%
Because $\seen\view$ is subjective,
the nonatomic points-to assertion
$\pointstoNA[q] \naloc \val$
is itself subjective.
% (The other conjuncts in the definition of the nonatomic points-to assertion
%  are objective.)
Therefore, it cannot appear in an invariant.
This is the price to pay for the apparent simplicity of this predicate.

% ------------------------------------------------------------------------------

\subsection{\hlog: Syntax and Meaning of Weakest-Precondition Assertions}
\label{sec:logic:hi:triples}

% In the previous subsection (\sref{sec:logic:hi:assertions}),
We have just presented the universe of \hlog assertions, which have type
$\typeVProp$, in contrast with \llog assertions, which have type $\typeIProp$.
%
We now wish to define a \hlog weakest-precondition assertion
$\WP\expr\predB$
whose postcondition $\predB$ is a function of a value
to a \hlog assertion.
%
This is in contrast with \llog's weakest-precondition assertion
$\WP{\mkexpr\expr\view}\pred$ (\sref{sec:logic:lo:assertions})
where $\pred$ is a function of a value and a view
to a \llog assertion.
We define the former on top of the latter,
as follows:
%
% Cf. wp_def in the file weakestpre.v.
%
\[\begin{array}{rcl}
  \WP\expr\predB % : \typeVProp
  & \eqdef &
    \Lam \thisview.
    \\&&\quad
    \All \view \viewgeq \thisview.
    \\&&\qquad
    \valid\view \wand
      \WP
        {\mkexpr\expr\view}
        {\Lam \mkexpr{\val'}{\view'}.
           \predB(\val')(\view')
           \isep
           \valid{\view'}
        }
\end{array}\]
%
Because $\WP\expr\predB$ must have type $\typeVProp$, it must be a monotone
function of this thread's view~$\thisview$. In order to make it monotone, we
quantify over a future view~$\view$ that contains~$\thisview$.
% A technique already used in the definition of the magic wand.
We use a \llog weakest-precondition assertion to require that, from the
view~$\view$, executing the expression~$\expr$ must be safe and must yield a
value and a view that satisfy~$\predB$. As a final touch, we
place validity assertions in the hypothesis and in the postcondition so as to
maintain the invariant that ``this thread's view is valid'', thus removing
from the user the burden of keeping track of this information.

The \hlog triple is derived from the \hlog weakest-precondition
assertion in the usual way: $\hoare\prop\expr\predB$ stands for
$\always(\prop \wand \WP\expr\predB)$.

% ------------------------------------------------------------------------------

\subsection{Soundness of \hlog}
\label{sec:logic:hi:soundness}

\hlog, equipped with the weakest-precondition assertion that was just defined,
is sound.
%
This follows straightforwardly from the soundness of \llog
(\sref{sec:logic:lo:soundness}).
%
% which has a more general conclusion and a weaker hypothesis.

\begin{theorem}[Soundness of \hlog]
  % For any expression~$\expr \in \typeExpr$ and any
  % pure predicate~$\predB : \typeVal \rightarrow \typename{Prop}$,
  If the entailment ${} \vdash \WP \expr {\TRUE}$ holds
  then the configuration $\mapempty; \mkexpr\expr\emptyview$
  is
  safe to execute.
  % adequate with respect to~$\Lam\mkexpr\val\view.\predB(\val)$.
\end{theorem}

% fp: amusant qu'on puisse affirmer (et démontrer) que la logique est
% sûre avant d'en avoir donné les règles...

% ------------------------------------------------------------------------------

\subsection{\hlog: Axioms}
\label{sec:logic:hi:axioms}

Each of the \llog axioms described in \sref{sec:logic:lo:axioms} can now be
used as a basis to establish a higher-level \hlog axiom, whose statement is
often simpler.
%
The resulting axioms appear in \fref{fig:hlog:rules}.

The first three axioms describe the operations of allocating, reading, and
writing nonatomic memory locations.
%
Allocation (\naalloc) requires nothing and produces the points-to assertion
$\pointstoNA \naloc \val$, which is sugar for $\pointstoNA[1] \naloc \val$,
and represents the exclusive ownership of the fresh memory location~$\naloc$.
%
Reading (\naread) requires $\pointstoNA[q] \naloc \val$, which represents
possibly-shared ownership of the memory location~$\naloc$, and preserves this
assertion.
%
Writing (\nawrite) requires $\pointstoNA \naloc \_$, which represents
exclusive ownership of~$\naloc$, and updates it to $\pointstoNA \naloc \val$.

\input{figure-hlog-axioms}
\input{figure-hlog-rules}
\input{figure-hlog-derived-rules}

We expect the reader to find these axioms unsurprising: indeed, they are
identical to the axioms that govern access to memory in \CSL with
fractional permissions \cite{bornat-permission-accounting-05}.
%
In the absence of a mechanism that allows a points-to assertion to be shared
between several threads,\footnote{%
  Sharing an assertion between several threads is usually permitted either
  via a runtime synchronization mechanism, such as a critical region
  % referred to as a ``resource'' by O'Hearn
  \cite[Section~4]{ohearn-07}
  or a lock~\cite{gotsman-aplas-07,hobor-oracle-08},
  or by a purely static mechanism,
  such as an invariant that can be accessed for the duration
  of an atomic instruction, as in some variants of \CSL~\cite{parkinson-bornat-ohearn-07}
  and in Iris \cite{iris}.%
}
%
these axioms forbid data races.
%
In \hlog, as explained earlier, a nonatomic points-to assertion cannot appear
in an invariant, as it is not an objective assertion. Therefore, \hlog forbids
data races on nonatomic memory locations. In other words, for a program to be
verifiable in \hlog, all accesses to nonatomic memory locations must be
properly synchronized via other means, such as reads and writes of atomic
memory locations.

The next five axioms describe the operations on atomic memory locations,
namely allocation, reading, writing, and CAS. They are analogous to
their \llog counterparts (\fref{fig:llog:rules}), yet
simpler, as the validity assertions have vanished, and this
thread's view is no longer named: instead, assertions of the form
$\seen\view$ are used to indicate partial knowledge of this thread's view.

These axioms deal with two aspects of atomic memory locations, namely the fact
that an atomic memory location holds a value, and the fact that an atomic
memory location holds a view. Fortunately, these two aspects are essentially
independent of one another. Furthermore, the second aspect can be ignored when
it is not relevant: indeed, from the axioms of
\fref{fig:hlog:rules}, one can easily derive a set of simplified axioms,
shown in \fref{fig:hlog:derived:rules}.
These derived axioms are the
standard axioms that govern access to memory in \CSL with fractional
permissions.
In \fref{fig:hlog:derived:rules}, we omit the view~$\view$ carried by atomic
locations since it is not relevent: we write $\pointstoAT[q]\atloc\val$ for
$\Exists\view.\pointstoAT[q]\atloc{\mkval\val\view}$, which is logically
equivalent to $\pointstoAT[q]\atloc{\mkval\val\emptyview}$.


In \hlog, because an atomic points-to assertion is objective, it \emph{can} be
shared between several threads, via an Iris invariant. Therefore, the
axioms of both \fref{fig:hlog:rules} and
\fref{fig:hlog:derived:rules} allow data races on atomic memory locations. By
using just the derived axioms of \fref{fig:hlog:derived:rules}, one can reason
in \hlog about atomic memory locations exactly in the same way as one reasons
in \CSL under the assumption of sequential
consistency~\cite{parkinson-bornat-ohearn-07}.

Allocating an atomic memory location (\atalloc) requires this thread to have
some view~$\view$, as witnessed by the precondition $\seen\view$. In the
postcondition, one obtains an atomic points-to assertion $\pointstoAT \atloc
{\mkval\val\view}$, which is sugar for $\pointstoAT[1] \atloc
{\mkval\val\view}$, and represents the exclusive ownership of the fresh
memory location~$\atloc$. This assertion states that the memory
location~$\atloc$ holds the value~$\val$ and a view that is at least as
good as the view~$\view$.
%
This axiom offers flexibility regarding the choice of~$\view$.
Indeed, $\view$ need not reflect all of the information that is currently
available to this thread: it can be a partial view. (The rule \seentwo,
exploited from right to left, weakens
$\seen (\view_1 \viewjoin \view_2)$ to~$\seen\view_1$.) In fact, if desired,
one can always take $\view$ to be the empty view. (The rule \seenzero allows
creating~$\seen\emptyview$ out of thin air.) This is how the derived axiom
\atallocsc is obtained.

Reading an atomic memory location (\atread) requires a fractional points-to
assertion $\pointstoAT[q] \atloc {\mkval\val\view}$ and preserves it. The
last conjunct of the postcondition, $\seen\view$, reflects the fact that the
view held at location~$\atloc$ becomes part of this thread's view: this is an
``acquire read''. The derived axiom \atreadsc is obtained by dropping this
information.

Writing an atomic memory location (\atwrite) requires an exclusive points-to
assertion $\pointstoAT \atloc {\mkval\_\view}$ as well as (possibly partial)
information about this thread's view $\seen \view'$. In the postcondition, the
points-to assertion is updated to $\pointstoAT \atloc {\mkval\val{\view
  \viewjoin \view'}}$, which reflects the fact that both the value $\val$ and the
view $\view'$ are written to the memory location~$\atloc$: this is a ``release
write''. Furthermore, the second conjunct of the postcondition, $\seen\view$,
indicates that the view $\view$ is acquired by this thread; indeed,
% in the \mocaml memory model,
an atomic write has both ``release'' and
``acquire'' effects. Again, it is possible to ignore these details when they
are irrelevant. In~particular, the rule \atwritesc is obtained by
letting $\view$ remain undetermined, by letting $\view'$ be the empty view,
and by dropping $\seen\view$ from the postcondition.

\casfailure and \cassuccess combine the axioms for reading and
writing an atomic location.
A failed CAS instruction does not affect the content of the
memory location, but still acquires a view~$\view$ from it. A successful CAS
instruction writes a value and a view to the memory location and acquires a
view from it. Again, if one does not care about these information transfers,
then one can use the simplified axioms \casfailuresc and \cassuccesssc in
\fref{fig:hlog:derived:rules}.

%%  LocalWords:  nonatomic pre postcondition parameterize archetypical rcl
%%  LocalWords:  duplicable runtime CAS
