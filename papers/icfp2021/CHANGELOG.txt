2021-06-04 (submitted revised version)

Changes which were required by the review summary:
- state the lesson to take home more explicitly (intro)
- add a bit more background about Iris and ghost state
- add background on Cosmo and views (sections 2.3 and 3.1)
- add background on the memory model of Multicore OCaml (section 3.1)
- rework and simplify existing background on logical atomicity (sections 2.2
  and 4.6)

Some changes which were not required explicitly by the review summary:
- rework section 4 in a way that we hope is more legible and less technical
  (present the ghost state of the queue more axiomatically: how we use it,
  rather than how we build it)
- shorten an unnecessarily technical section about contention (section 3.5)
- address some more reviewer remarks (e.g. mention that language is untyped,
  make it clear what theorem has been proven)
- various rewordings and fixes, some more citations

2021-03-01 (initial submission)
