Dear reviewers, thank you for your attentive reading and relevant remarks.

We acknowledge that the paper is somewhat tedious and technical, and will try to
improve that. We will make efforts to better introduce notations and terminology.
We note that we should better highlight what has been proven (the specifications
in Figures 3 and 8), and we will extend the related work based on the reviewers’
remarks.

# LESSON TO TAKE HOME

Reviewer D says:
> Little explanation of how these ideas generalize to a proof methodology for
> verification under weak memory.

Reviewer A says:
> this paper […] doesn't do a good job of articulating any lessons that were
> learned by this verification effort that can be transported to other settings.

The lesson that we aim to convey is that logical atomicity and the view transfer
mechanism are, we believe, powerful theoretical *and* practical tools for
describing data structures. By comparison with a proof in SC, Cosmo’s logic of
views required just the minimal additions that are necessary to account for the
weak model, so that, provided the right synchronization, we can easily turn
a proof in SC into a proof in the weaker model.

These tools are also relevant to other memory models, such as RC11, which are
also described in terms of views.

We will change the introduction to add focus on these points.

Reviewer B asks:
> A concurrent queue is one of the [simplest] concurrent data structures […].
> How well do you think this methodology will scale to more complex concurrent
> data structures?

It remains to be seen. However, the Iris folklore already has (not always
published) proofs in the SC model for more elaborate structures, e.g. ones using
the "helping" pattern. We believe that our methodology could be adapted to these
proofs.

# PREREQUISITES

Reviewer D says:
> An inadequate explanation of precisely what weak memory behaviour (i.e., the
> OCaml memory model) is being handled.

Indeed, we left the description of the memory model entirely to earlier papers:
[Dolan et al, PLDI 2018] and [Mével et al, ICFP 2019]. We note that it hinders
the clarity of ours, and will consider adding a brief introduction to the memory
model (first paper) and to Cosmo (second paper). For instance, Reviewer B asks:

> Lines 460-461 and 474-476: The non-atomic writes are placed before the atomic
> reads/writes. Does OCaml memory model thereby guarantee that another thread
> will see them in the proper order?

Yes. M-OCaml atomics support the message-passing pattern. We take Reviewer D’s
suggestion to include such an example in the paper, to illustrate our brief
introduction to the memory model.

Reviewer D says:
> Requires more than a passing familiarity with Iris and Cosmo to understand the
> details of the invariant structure and proof.

Reviewer B says:
> This paper […] is not easy to read for someone not intimately familiar with
> recent work on Concurrent Separation Logic, Logical Atomicity, Ghost
> Variables, Views, OCaml's weak memory model, etc.

Indeed, in Sections 2 and 4 we tried our best detailing the construction of the
invariant and introducing Iris notions from the ground when they are needed.
Iris cannot be presented faithfully in a tight page limit, however it has become
a well-established tool which no longer needs a lengthy presentation.

# MISC QUESTIONS

Reviewer A asks:
> Can the queue implementation store any values other than unit?

Since we are not interested in typing aspects, our idealized language is untyped.
We realize it may confuse the reader, and we’ll consider at least mention it, or
adapt the code to make it typecheck (a concrete implementation could either use
an option type or ask the client for a default value).

Reviewer A asks:
> Do we have to worry about integer arithmetic overflow in this model?

Integers in the idealized language are unbounded, so that proofs of correctness
miss that class of bugs. If wanting to lift this limitation, an option described
by [Mével et al, ESOP 2019] is to use time receipts and assume a running time
bounded by a limit which is short enough so that overflows don’t have time to
occur, but large enough so that we do not run into it in practice.

Reviewer A says:
> The fourth bullet (line 92) of the introduction seems to be the most
> compelling argument for novelty in this paper, and the description of the
> "happens before" aspects of the invariants described in Section 2.3 convey
> some intuitions about the differences obtained for specifications in weak vs.
> stronger memory models. I was still left wondering how, concretely, the
> distiction plays out -- is there some example client of the queue that would
> demonstrate the difference?

The specification of the queue in a strong (SC) memory model is of little use in
a weak memory setting. Indeed, first, the isQueue representation predicate is
difficult to share among threads unless we have stated that it is objective.
Second, with no happens-before relationship, the specification would not allow
transferring ownership of complex values (which include pieces of memory)
through the queue. In particular, the pipeline example would not be provable.

Reviewer B asks:
> Line 482: This implementation 'spins' […] until success. Can you comment on
> whether true blocking […] is possible in OCaml? If so, how would that enter
> into your formalization?"

M-OCaml does have an API for passive waiting, in the form of monitors. Monitors
have already been formalized in the literature (e.g. Hoare described them in
Hoare logic; [Amighi, Haack, Huisman, Hurlin, 2015] gave an account in
separation logic), so we do not expect difficulty in formalizing them in Cosmo,
building on already existing lock formalizations. However it remains to be done.

Reviewer B asks:
> Line 627-629, "non-blocking": is this a real concern? […] do you mean the
> thread could fail/abort/be killed for other reasons?

Yes, that is what we mean. It is an unlucky scenario, but one that can happen.

Reviewer C says:
> Around line 183: The sequential specification does not actually imply that
> dequeue cannot return when the queue is empty; rather, it leaves the behavior
> when the queue is empty unspecified […].

We believe what we wrote is correct: Figure 1 only states n ≥ 1 in the
postcondition, so that when n = 0 the precondition is still satisfiable, and the
postcondition implies falsehood.

Reviewer C says:
> Why do memory locations need to store arrays? Couldn't you just use
> consecutive locations?

We made this technical choice because we believe it more closely follows the
actual behavior of the OCaml heap. Indeed, in OCaml, one can query the length of
a memory block, and cannot build a pointer within a block.

Reviewer D says:
> I was unclear on how the definitions underlying the memory model definition
> (typically given in terms of relations like rf, fr, mo, co, etc.) manifest in
> the development of the specification.

Here we work with an operational description (small-step interleaving semantics)
of the memory model, which [PLDI 2018] has proven equivalent to an axiomatic
description (with relations like mo, rf, etc.).

Reviewer D asks:
> (E.g., does multicore OCaml's memory model corresponds to the C++11's
> release-acquire semantics […]?)

There is no direct mapping between the notions of C11 and that of M-OCaml.
However, in order to get some intuition, one can see M-OCaml "atomics" as a
weak form of C11 SC atomics, while M-OCaml "non-atomics" resemble C11 relaxed
atomics.

Reviewer D says:
> The explanation in the paper leads me to believe that the programmer must
> reason about weak memory at the level of individual operations when writing
> a specification, which would be impractical […].

We are not sure we correctly understand the concern of the reviewer. The
specification in weak memory (Figure 3) says few about the implementation.
Specifying the synchronizations that happen does not rely on knowing that an
implementation realizes them with e.g. atomic locations. Any specification of
a concurrent queue needs a happens-before relationship on each value
transferred, which in Cosmo translates as a view. The writer-reader
synchronization is not an implementation detail, it should be part of the spec
of any concurrent queue. It is true that the other two specified
synchronizations (writer-writer, reader-reader) are more
implementation-specific, but they are not hard to establish, and they only
strengthen the spec (i.e. from this spec we can derive a weaker one by dropping
views H and T).
