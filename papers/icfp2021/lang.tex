% ------------------------------------------------------------------------------

% Reminders on the programming language.

\subsection{Multicore OCaml}
\label{sec:lang}

\input{figure-lang-syntax}
\input{figure-lang-triples}

The syntax of our idealized version of \mocaml{} is presented
in~\fref{fig:syntax}.
It is untyped (contrary to the actual \mocaml{} language)
and equipped with a standard call-by-value,
left-to-right evaluation. The parts of interest are the memory operations.
Their semantics have been described by~\citet{mevel-jourdan-pottier-20};
however, in the present paper,
each location stores an \emph{array} of values, whose cells act as independent
memory cells with respect to the memory model.
%
Cells are rigidly ascribed an access mode~$\accmode$, which is either
``atomic''~($\accmode = \accmodeAT$) or
``non-atomic''~($\accmode = \accmodeNA$).
%
When~$0 \leq \nbelems$, the expression $\ArrayAllocANY\nbelems\val$ allocates
a block of $\nbelems$~cells whose access mode is~$\accmode$ and whose
initial value is $\val$.
%
Reading from a cell with access mode~$\accmode$ at offset~$\idx$ of
location~$\loc$ is written $\ArrayReadANY\loc\idx$.
%
Writing a value~$\val$ to a cell with access mode~$\accmode$ at offset~$\idx$ of
location~$\loc$ is written $\ArrayWriteANY\loc\idx\val$.
%
In addition, atomic cells support the usual compare-and-set operation:
%
$\ArrayCAS\loc\idx{\val_1}{\val_2}$
reads the atomic cell at offset~$\idx$ of location~$\loc$,
tests whether its value is equal to $\val_1$, overwrites it with $\val_2$ if that
is the case, and returns the Boolean result of the test; importantly, the
read and the write operations happen \emph{atomically}.
%
There is syntactic sugar for single-cell locations, or ``references'':
$\AllocANY\val$ allocates a location of length~one, $\ReadANY\loc$ reads at
offset~zero, $\WriteANY\loc\val$ writes at offset~zero and
$\CAS\loc{\val_1}{\val_2}$ performs compare-and-set at offset~zero.

Lastly, $\Fork\expr$ creates a new thread which executes $\expr$.
The expression $\Fork\expr$ returns the unit value~$\Unit$
without waiting for the completion of the new thread.

The memory model of \mocaml describes how the memory operations interact with the shared memory. \citet{dolan-18} give an operational account, which is out of the scope of the current paper. However, to give some intuition, we show in~\fref{fig:lang:triples} what this semantics translates to in the \hlog program logic. In the following paragraphs, we give an overview of these triples which are explained more thoroughly by~\citet{mevel-jourdan-pottier-20}.

There are different points-to assertions for atomic cells and for non-atomic cells. As is standard, these predicates assert unique ownership of the cells.

Operations on non-atomic cells obey the usual-looking Hoare triples, but an important departure from sequential consistency is that their associated points-to assertion $\nthpointstoNA \loc \idx \val$ is subjective: the knowledge of the latest value~$\val$ of a non-atomic cell~$\loc[\idx]$ is relative to the ambient view.%
  \footnote{%
    This single-value points-to assertion effectively forbids reasoning about races on non-atomic cells. The operational semantics of~\citet{dolan-18} is more general, as it gives a well-defined (albeit nondeterministic) semantics to racy uses of non-atomic cells.
  }

On the other hand, the points-to assertion $\nthpointstoAT \loc \idx {\mkval\val\view}$ that represents an atomic cell is objective: this implies that an atomic cell~$\loc[\idx]$ stores a single value~$\val$ on which all threads agree.
%This value is thus read and updated in a sequentially consistent fashion.
In addition, an atomic cell also stores a view~$\view$. As can be seen in the triples of atomic operations, this view accumulates the knowledge of all writers and transmits this knowledge to readers. This means that there is a happens-before relationship from a write operation to a read operation which reads from it (in a release-acquire fashion). In fact, atomic accesses are the prime means of inter-thread synchronization in \mocaml.

The last two triples say that a failed CAS operation behaves as an atomic read (returning $\False$), while a successful CAS operation behaves as the combination of an atomic read and an atomic write (returning $\True$).
