\input{header}
\input{bubbles} % TODO: clean this file and rename macros

\usepackage{svg}
% The package svg ignores directories in $TEXINPUTS, so we have to put SVG files
% *outside* of these directories (this is probably a bug in package svg):
\svgpath{{../common_svg/}}
% However the package graphicx does look into $TEXINPUTS, so this is not needed:
%\graphicspath{{../common/img/}}



% ------------------------------------------------------------------------------
% Headings.

\begin{document}

\title{Towards a separation logic for Multicore~OCaml}
\author[%
  Mével,
  Jourdan,
  Pottier%
]{%
  \textbf{Glen Mével},
  Jacques-Henri Jourdan,
  François Pottier%
}
\institute{%
  LRI \& Inria, Paris, France%
}
%\date{October~28-29, 2019 \\ Iris workshop, Århus}
%\date{November~18, 2019 \\ Gallium seminar, Inria, Paris}
\date{May~25, 2020 \\ PPS seminar, Paris}

\begin{frame}[plain,noframenumbering]
  \setcounter{framenumber}{0}
  \titlepage
\end{frame}



% ------------------------------------------------------------------------------
% Main slides.



\sectionframe{The weak memory model}



\begin{frame}
  \frametitle{Multicore OCaml}
  Extension of the OCaml language with \emph{multicore programming}.
  \br
  Research project at OCaml Labs (Cambridge), will be merged eventually.
  \br
  Strengths:
  \begin{itemize}%
    \item brings multicore abilities to a functional, statically typed,
      memory-safe programming language;
    \item (gives the programmer a simpler memory model than that of C11,
      hopefully;)
    \item limited performance drop for sequential code.
  \end{itemize}%
  \emph{Goals of this PhD:}
  \begin{itemize}%
    \item Build a proof system for Multicore OCaml programs.
    \item Prove interesting concurrent data structures.
  \end{itemize}
\end{frame}



\begin{frame}
  \frametitle{\alt<5->{A weaker memory model}{Sequential consistency}}
  Consider this concurrent program:
  \br
  \qquad\texttt{\begin{tabular}{l||l}%
    \multicolumn{2}{c}{x := 0} \\
    \multicolumn{2}{c}{y := 0} \\
    x := 1\tikzPoint{T11}  &  \tikzPoint{T21}y := 1  \\
    A := y\tikzPoint{T12}  &  \tikzPoint{T22}B := x  \\
  \end{tabular}}%
  \begin{tikzpicture}[remember picture, overlay, ->, ultra thick]
    \only<2> \draw[blue] (T11) -- (T12) -- (T21) -- (T22);
    \only<3> \draw[blue] (T21) -- (T22) -- (T11) -- (T12);
    \only<4> \draw[blue] (T11) -- (T21) -- (T12) -- (T22);
    \only<5->\draw[red]  (T12) -- (T22) -- (T21) -- (T11);
  \end{tikzpicture}%
  \br
  Possible outcomes (A, B):
    \textcolor<2>{blue}{(0, 1)},
    \textcolor<3>{blue}{(1, 0)},
    \textcolor<4>{blue}{(1, 1)}%
    \only<5->{, \textcolor{red}{(0, 0)}}.
  \\
  Observed {\tiny(M.OCaml on 2-core x86-64)}:
    \textcolor<2>{blue}{91\%},
    \textcolor<3>{blue}{9\%},
    \textcolor<4>{blue}{0.001\%}%
    \only<5->{, \textcolor{red}{0.1\%}}.
  \br
  \uncover<6->{\textcolor{red}{The compiler may reorder a write after a read.}} \\
  \uncover<6->{\textcolor{red}{(The processor too.)}}
\end{frame}



\begin{frame}[fragile]
  \frametitle{(Bonus: actual Multicore OCaml code)}
\begin{lstlisting}[language=camleff, basicstyle=\scriptsize\ttfamily]
let run2 f1 f2 =
  let t1 = Domain.spawn (fun () -> sleepf 0. ; f1 ()) in
  let y2 = sleepf 0. ; f2 () in
  let y1 = Domain.join t1 in
  (y1, y2)

let () =
  let count = [| ref 0 ; ref 0 ; ref 0 ; ref 0 |] in
  for _ = 1 to 100_000 do
    let x = ref 0 and y = ref 0 in
    let (a, b) = run2
        (fun () -> x := 1 ; !y)
        (fun () -> y := 1 ; !x) in
    incr count.(2*a + b)
  done ;
  printf "count 0 0 = %6u\n" !(count.(0)) ;
  printf "count 0 1 = %6u\n" !(count.(1)) ;
  printf "count 1 0 = %6u\n" !(count.(2)) ;
  printf "count 1 1 = %6u\n" !(count.(3)) ;
\end{lstlisting}
\end{frame}



%\begin{frame}
%  \frametitle{Weak memory models}
%  Architectures, from strongest to weakest:
%  \begin{itemize}%
%    \item sequentially consistent:
%      none;
%    \item “strongly consistent”:
%      x86-64 (most personal computers),
%      SPARC TSO (supercomputers);
%    \item “weakly consistent”:
%      ARM (mobile devices),
%      POWER (servers),
%      PowerPC (7\textsuperscript{th}~gen.\ video game consoles, Apple computers
%        until 2006).
%  \end{itemize}%
%  \ebr
%  Sequential consistency is unrealistic.
%  \br
%  We need \strong{weaker memory models}, where different threads have different
%  \emph{views} of the shared state.
%\end{frame}



\begin{frame}
  \frametitle{Weak memory models}
  Sequential consistency is unrealistic.
  \br
  We need a \emph{weaker memory model}, where different threads have different
  \emph{views} of the shared state.
  \br
  The model should be specific to our language. % and NOT to any architecture
  \br
  Existing works: Java (2000s), C11 (2010s; also Rust).
  \br
  \emph{Candidate model for Multicore OCaml:} \\
  \qquad{}Dolan, Sivaramakrishnan, Madhavapeddy. \\
  \qquad{}\textit{Bounding Data Races in Space and Time}. \\
  \qquad{}PLDI 2018.
  \br
  Two access modes: non-atomic, atomic.
\end{frame}



\begin{frame}
  \frametitle{An operational model for Multicore OCaml: non-atomics}
%
  \begin{center}
    \texttt{\begin{tabular}{l||l}%
      \color<4->{blue}\WriteNA x {\;{\color<4>{red}$x_1$}\tikzPoint{NA1}}
          &  \tikzPoint{NA2}\color<5->{blue}\WriteNA y {\;{\color<5>{red}$y_1$}}  \\
      \color<6->{blue}A := {\color<6>{ForestGreen}\ReadNA {\;y}}\tikzPoint{NA3}
          &  \tikzPoint{NA4}\color<7->{blue}B := {\color<7>{ForestGreen}\ReadNA {\;x}}  \\
    \end{tabular}}%
  \end{center}
%
  \begin{tikzpicture}[remember picture, overlay, ->, ultra thick, blue]
    \only<5>  \draw (NA1) -- (NA2);
    \only<6>  \draw (NA1) -- (NA2) -- (NA3);
    \only<7-> \draw (NA1) -- (NA2) -- (NA3) -- (NA4);
  \end{tikzpicture}%
%
  \br
  \begin{columns}
    \begin{column}{0.3\textwidth}
      \begin{overprint}%
        \onslide<1>  \includesvg[height=2.5cm]{store_na_blank}
        \onslide<2>  \includesvg[height=2.5cm]{store_na_0a}
        \onslide<3>  \includesvg[height=2.5cm]{store_na_0b}
        \onslide<4>  \includesvg[height=2.5cm]{store_na_1}
        \onslide<5>  \includesvg[height=2.5cm]{store_na_2}
        \onslide<6>  \includesvg[height=2.5cm]{store_na_3}
        \onslide<7-> \includesvg[height=2.5cm]{store_na_4}
      \end{overprint}
    \end{column}
    \begin{column}{0.7\textwidth}
      \uncover<2-3>{%
        Each \emph{non-atomic} location has a \strong{history}, \textit{i.e.} a map
        from timestamps to values (timestamps are per location).
      }
      \br
      \uncover<3>{%
        Each thread has its own \strong{view} of the non-atomic store,
        \textit{i.e.} a map from non-atomic locations to timestamps.
      }
    \end{column}
  \end{columns}
%
  \MzAbsoluteBubbles{%
    \MzAbsoluteRawArrowLessBubble{7cm}{2cm}{6.1cm}{non-atomic write}{%
      \begin{itemize}
        \item Timestamp must be fresh.
        \item Timestamp must be newer than current thread’s view.
        \item Current thread’s view is updated.
      \end{itemize}
    }{4-5}
%
    \MzAbsoluteRawArrowLessBubble{7cm}{2cm}{6.1cm}{non-atomic read}{%
      \begin{itemize}
        \item Returns \emph{any} value at least as recent as current thread’s view.
        \item Current thread’s view is unchanged.
          % NOTE: this is the difference with Relaxed in C11
      \end{itemize}
    }{6-}
  }
%
\end{frame}



\begin{frame}
  \frametitle{An operational model for Multicore OCaml: atomics}
  Non-atomic locations are useful for updating the state locally, but they don’t
  provide synchronization.
  \br
  Atomic locations allow the message-passing idiom.
\end{frame}



\begin{frame}
  \frametitle{An operational model for Multicore OCaml: atomics}
%
  \ebr
  \begin{columns}
    \begin{column}{0.3\textwidth}
    \end{column}
    \begin{column}{0.2\textwidth}
      \texttt{\begin{tabular}{l}%
        \color<4->{blue}\WriteNA x {\;{\color<4>{red}$x_1$}}
        \quad\tikzPoint{AT1} \\
        \color<5->{blue}\WriteAT a {\;{\color<5>{red}\True}}
        \tikzPoint{AT2} \\
        \\
        \\
      \end{tabular}}%
    \end{column}
    \vrule\,\vrule
    \begin{column}{0.5\textwidth}
      \texttt{\begin{tabular}{l}%
        \color<7->{blue}REPEAT \\
        \tikzPoint{AT3}\!\!
        \color<6->{blue}\quad{}C := {\color<6>{ForestGreen}\ReadAT {\;a}} \\
        \color<7->{blue}UNTIL C == \True \\
        \tikzPoint{AT4}\!\!
        \color<7>{blue}B := {\color<7>{ForestGreen}\ReadNA {\;x}} \\
      \end{tabular}}%
    \end{column}
    \quad
  \end{columns}
%
  \begin{tikzpicture}[remember picture, overlay, ->, ultra thick, blue]
    \only<5>  \draw (AT1) -- (AT2);
    \only<6>  \draw (AT1) -- (AT2) -- (AT3);
    \only<7-> \draw (AT1) -- (AT2) -- (AT3) -- (AT4);
  \end{tikzpicture}%
%
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{overprint}%
        %\onslide<1>  \includesvg[height=2.5cm]{store_at_blank}
        \onslide<1>  \includesvg[height=2.5cm]{store_at_0a}
        \onslide<2-3>\includesvg[height=2.5cm]{store_at_0b}
        \onslide<4>  \includesvg[height=2.5cm]{store_at_1}
        \onslide<5>  \includesvg[height=2.5cm]{store_at_2}
        \onslide<6>  \includesvg[height=2.5cm]{store_at_3}
        \onslide<7-> \includesvg[height=2.5cm]{store_at_4}
      \end{overprint}
      \[
        \texttt{a}:\quad{\uncover<1->{%
          \alt<-4>{%
            \False
          }{%
            \color<5>{red}\color<6>{ForestGreen}\True
          }
        }}
      \]
    \end{column}
    \begin{column}{0.6\textwidth}
      \uncover<2>{%
        Each \strong{atomic} location stores \emph{one} value, and \emph{one view}
        of the non-atomic store.
      }
    \end{column}
  \end{columns}
%
  \MzAbsoluteBubbles{%
    \MzAbsoluteRawArrowLessBubble{8.0cm}{2.0cm}{5.0cm}{atomic write}{%
      Merges the writer’s view into the atomic location’s view.
    }{5}
%
    \MzAbsoluteRawArrowLessBubble{8.0cm}{2.0cm}{5.0cm}{atomic read}{%
      Merges the atomic location’s view into the reader’s view.
    }{6}
  }
%
\end{frame}



\sectionframe{Our program logic}



%\begin{frame}
%  \frametitle{Features of the logic}
%  Inherited from Iris:
%  \begin{itemize}
%    \item affine separation logic
%    \item \emph{objective} invariants
%    \item Iris-style ghost state
%  \end{itemize}
%  We add:
%  \begin{itemize}
%    \item views
%    \item ownership predicate $\pointstoAT a {(v, \vv)}$ (objective)
%    \item ownership predicate $\pointstoNA x v$ (subjective)
%    \item weakest preconditions
%  \end{itemize}
%\end{frame}



\begin{frame}
  \frametitle{Rules of non-atomic locations}
  The predicate $\pointstoNA x v$ means that we own the non-atomic location~$x$
  and that we have \emph{seen} its latest value, which is $v$.
  \br
  \strong{Non-atomic write:}
  \[
    \hoareV%
      {\pointstoNA x v}%
      {\WriteNA x {v'}}%
      {\lambda ().~\pointstoNA x {v'}}%
  \]
  \strong{Non-atomic read:}
  \[
    \hoareV%
      {\pointstoNA x v}%
      {\ReadNA x}%
      {\lambda v'.~v' = v \ISEP \pointstoNA x v}%
  \]
\end{frame}



\begin{frame}
  \frametitle{Impact of the weak memory model on our CSL}
  \emph{Invariants} are the mechanism by which threads can share propositions in
  a Concurrent Separation Logic such as Iris:
  %\br
  \[
    \infer{%
      \knowInv{}{I} \vdash \hoare{P}{e}{Q}
    }{%
      \hoare{P \isep I}{e}{Q \isep I}
      & \text{$e$ atomic}
    }
  \]
  \br
  The proposition $\pointstoNA x v$ is \strong{subjective}: its truth depends on
  the thread’s view of memory.
  \br
  It is unsound to share it \textit{via} an invariant.
  \br
  Propositions which are true in all threads are called \strong{objective}:
  \begin{itemize}
    \item “pure” facts, such as $v = 5$
    \item ghost state, such as $\gamma \gmapsto \authfrag 5$
    \item atomic state, such as $\pointstoAT a {(5, \vv)}$
    \item invariants, such as $\knowInv{}{P}$
  \end{itemize}
  \emph{Only objective propositions can be put in an invariant.}
\end{frame}



\begin{frame}
  \frametitle{Rules of atomic locations\only<-2>{\ (simplified)}}
  The predicate $\pointstoAT a {\RevealViewAT<3->{v}{\vv}}$ means that we own the
  atomic location~$a$, which stores the value~$v$%
  \only<3->{\ and a view (at least)~$\vv$}.
  \br
  It is \emph{objective}.
  \br
  \strong{Atomic write:}
  \[
    \hoareV%
      {\pointstoAT a {\revealViewAT<3->{v}{{\color<4>{ForestGreen}\vv}}}
        \only<3->{{}\ISEP {\color{red}\seen \vv'}}
      }%
      {\WriteAT a {v'}}%
      {\lambda ().~%
        \pointstoAT a {\revealViewAT<3->{v'}{{\color<3->{red}\vv'} \only<4>{{}\sqcup \vv}}}
        \only<4>{{}\ISEP {\color{ForestGreen}\seen \vv}}
      }%
  \]
  \strong{Atomic read:}
  \[
    \hoareV%
      {\pointstoAT a {\revealViewAT<3->{v}{\color{ForestGreen}\vv}}}%
      {\ReadAT a}%
      {\lambda v'.~%
        v' = v
        \ISEP \pointstoAT a {\revealViewAT<3->{v}{\vv}}
        \only<3->{{}\ISEP {\color<3->{ForestGreen}\seen \vv}}
      }%
  \]
%
  \MzAbsoluteBubbles{%
    \MzAbsoluteRawArrowLessBubble{8.8cm}{3.5cm}{5.2cm}{views}{%
      Views are ordered by inclusion.
      %\[\!\!\!\!\!\!\!\!
      %  \vv_1 \sqsubseteq \vv_2 &\Longleftrightarrow
      %    \forall \ell.~\vv_1(\ell) \leq \vv_2(\ell)
      %\]
      \br
      The predicate $\seen \vv$ means “the \mbox{current} thread’s view includes~$\vv$”.
    }{2-}
  }
\end{frame}



\begin{frame}
  \frametitle{Propositions are monotonic}
  Subjective propositions are \strong{monotonic} w.r.t. the thread’s view.
  \br
  One reason: the frame rule:
  \[
    \hoareV%
      {%
        \pointstoAT a v \ISEP P
        \only<2>{\quad\text{\color{ForestGreen}this holds at the thread’s current view}}
      }%
      {\WriteAT a {v'}}%
      {\lambda ().~%
        \pointstoAT a {v'} \ISEP P
        \only<2>{\quad\text{\color{ForestGreen}this holds at the thread’s now extended view}}
      }%
  \]
\end{frame}



\begin{frame}
  \frametitle{The message passing idiom}
  \uncover<1>{%
    The objective proposition ``\(P \opat \vv\)\,'' is
    the subjective proposition~$P$ seen at a fixed view~$\vv$.
  }
  \begin{center} \(
    P \Longleftrightarrow \exists \vv.~(\seen \vv) \ISEP (P \opat \vv)
  \) \end{center}
  \begin{overprint}%
    \onslide<1>
      $(\Rightarrow)$ If $P$ holds now, then it holds at the current view. \\
      $(\Leftarrow)$ If $P$ holds at some earlier view, then it holds now.
    \onslide<2->
      \ebr
      \MzStartProgramPoints
      \begin{columns}
        \begin{column}{0.5\textwidth}
          \texttt{\begin{tabular}{l}%
            \uncover<2->{\hoareProp{\uncover<3->{\pointstoAT a {(\False, \emptyview)} \ISEP} P}} \\
            \uncover<4->{\hoareProp{\uncover<3->{\pointstoAT a {(\False, \emptyview)} \ISEP} P \opat \vv \ISEP \seen \vv}} \\
            \uncover<3->{\color{blue}\WriteAT a \True} \\
            \uncover<5->{\hoareProp{\pointstoAT a {(\True, \vv)} \ISEP P \opat \vv}}
            \tikzPoint{MsgPssW} \\
            \qquad\qquad\qquad\quad\MzProgramPoint\\
            \\
            \\
            \\
          \end{tabular}}%
        \end{column}
        \vrule\,\vrule
        \begin{column}{0.5\textwidth}
          \texttt{\begin{tabular}{l}%
            \\
            \\
            \\
            \\
            \tikzPoint{MsgPssR}\!\!
            \uncover<7->{\hoareProp{\pointstoAT a {(\True, \vv)} \ISEP P \opat \vv}} \\
            \uncover<3->{\color{blue}C := \ReadAT{a}} \\
            \uncover<8->{\hoareProp{\pointstoAT a {(\True, \vv)} \ISEP P \opat \vv \ISEP \seen \vv}} \\
            \uncover<2->{\hoareProp{\uncover<3->{\pointstoAT a {(\True, \vv)} \ISEP} P}} \\
          \end{tabular}}%
        \end{column}
      \end{columns}
      \begin{tikzpicture}[remember picture, overlay, ->, ultra thick, red]
        \only<7-> \path (MsgPssW) -- node[sloped] (MsgPssTxt) {inv} (MsgPssR);
        \only<7-> \draw (MsgPssW) -- (MsgPssTxt) -- (MsgPssR);
      \end{tikzpicture}%
      \MzStartBubbles
      \MzBubble{-0.5cm}{-1.5cm}{4.6cm}{}{%
        This proposition is objective:
        it can be put in an invariant.
      }{6-}
  \end{overprint}%
\end{frame}



\begin{frame}[fragile]
  \frametitle{Example: spin lock}
  A spin lock implements a lock using an atomic boolean variable.
  \begin{columns}[T] \begin{column}{0.5\textwidth}
\begin{lstlisting}[language=camleff]
let rec acquire lk =
  if CAS lk false true
  then ()
  else acquire lk
\end{lstlisting}
    \end{column}
    \begin{column}{0.5\textwidth}
\begin{lstlisting}[language=camleff]
let release lk =
  lk :={at} false
\end{lstlisting}
    \end{column}
  \end{columns}
  \ebr
  %\br
  \alt<1>%
    {The invariant in the sequentially consistent model is:}%
    {The invariant in the weak model is:}
  \begin{align*}
    \/& \textsf{lockInv}~\texttt{lk}~P \eqdef
    \\& \qquad\pointstoAT {\texttt{lk}} \True
        \;\;\lor\;\; \left(
          \uncover<2>{{\color{red}\exists \vv.}~}
          \pointstoAT {\texttt{lk}} {\RevealViewAT<2->{\False}{\color{red}\vv}}
          \ISEP P \only<2>{{\color{red} {}\opat\vv}}
        \right)
  \end{align*}
\end{frame}



\begin{frame}
  \frametitle{Example: ticket lock}
  A ticket lock implements a lock using two atomic integer variables.
  \br
  \alt<1>%
    {The invariant in the sequentially consistent model is:}%
    {The invariant in the weak model is:}
  \begin{align*}
    \/& \textsf{lockInv}~\texttt{turn}~\texttt{next}~\gamma~P \eqdef
    \\& \qquad\exists t, n\only<2>{, {\color{red}\vv}}.
    \\& \qquad\quad\begin{aligned}
          \/& \phantom{{}\ISEP{}}
              \pointstoAT {\texttt{turn}} {\revealViewAT<2->{t}{\color{red}\vv}}
          \\& \ISEP
              \pointstoAT {\texttt{next}} {\phantom{(} n}
          \\& \ISEP
              \left(
                \textsf{ticket}~\gamma~t
                \lor \left( \textsf{locked}~\gamma \ISEP P \only<2>{{\color{red} {}\opat\vv}} \right)
              \right)
          \\& \ISEP
            \gamma \gmapsto (\authfull \ldots)
        \end{aligned}
  \end{align*}
\end{frame}



\begin{frame}
  \frametitle{Example: Dekker’s mutual exclusion}
  Dekker’s algorithm solves the mutual exclusion problem using three atomic
  variables.
  \br
  \alt<1>%
    {The invariant and representation predicate in the SC model are:}%
    {The invariant and representation predicate in the weak model are:}
  \begin{align*}
    \/& \textsf{DekkerInv}~\texttt{turn}~\texttt{flag}_0~\texttt{flag}_1~\gamma~P \eqdef
    \\& \qquad\exists t, f_0, f_1, c_0, c_1\only<2>{{\color{red}, \vv_0, \vv_1}}.
    \\& \qquad\quad\begin{aligned}
          \/&
          %    \pointstoAT {\texttt{turn}} t
          %\\& \ISEP
              \left(
                \forall i\in\{0,1\}.~%
                \pointstoAT {\texttt{flag}_i} {\RevealViewAT<2->{f_i}{\color{red}\vv_i}}
                \ISEP \gamma_i \gmapsto \authfull c_i
                \only<2>{\ISEP {\color{red}\gamma'_i \gmapsto \authfull \vv_i}}
                \vphantom{x'}
              \right)
          \\& \ISEP
              \left(
                (\lnot c_0 \land \lnot c_1) \wand
                P \only<2>{{\color{red} {} \opat \left(\vv_0\sqcup\vv_1\right)}}
              \right)
          \\& \ISEP
              \ldots
        \end{aligned}
    \\& \textsf{isDekker}~i~\gamma \eqdef
    \\& \qquad\uncover<2>{{\color{red}\exists \vv.}}
          \gamma_i \gmapsto \authfrag \False
          \only<2>{%
            \ISEP {\color{red}\gamma'_i \gmapsto \authfrag \vv}
            \ISEP {\color{red}\seen \vv}
          }
  \end{align*}
\end{frame}



%%\sectionframe{Case study}
%\sectionframe{Examples}
%
%
%
%\begin{frame}[fragile]
%  \frametitle{Spinlock}
%  A spinlock implements a lock using an atomic boolean variable:
%  \begin{columns}[T]
%    \begin{column}{0.5\textwidth}
%\begin{lstlisting}[language=camleff]
%let rec acquire lk =
%  if CAS lk false true
%  then ()
%  else acquire lk
%\end{lstlisting}
%    \end{column}
%    \begin{column}{0.5\textwidth}
%\begin{lstlisting}[language=camleff]
%let release lk =
%  lk :={at} false
%\end{lstlisting}
%    \end{column}
%  \end{columns}
%  \ebr
%  It satisfies the interface:
%  \begin{columns}[T]
%    \begin{column}{0.5\textwidth}
%      \[
%        \hoareV%
%          {\textsf{isLock}~\texttt{lk}~P}%
%          {\texttt{acquire lk}}%
%          {P}%
%      \]
%    \end{column}
%    \begin{column}{0.5\textwidth}
%      \[
%        \hoareV%
%          {\textsf{isLock}~\texttt{lk}~P \ISEP P}%
%          {\texttt{release lk}}%
%          {\textsf{True}}%
%      \]
%    \end{column}
%  \end{columns}
%  \ebr
%  \begin{overprint}
%    \onslide<2->
%      \alt<2>%
%        {The invariant in the sequentially consistent model is:}%
%        {The (objective) invariant in the weak model becomes:}
%      % TODO: bubble "P is subjective"
%      \begin{align*}
%        \textsf{lockInv}~\texttt{lk}~P &\eqdef
%          \pointstoAT {\texttt{lk}} \True
%          %\lor \left( \pointstoAT {\texttt{lk}} \False \ISEP P \right)
%          \lor \left( \uncover<3>{\exists \vv.~}\pointstoAT {\texttt{lk}} {\RevealViewAT<3->{\False}{\vv}}
%          \ISEP \alt<-2>{P}{P \opat \vv} \right)
%        %\\
%        %\textsf{lockInv}~\texttt{lk}~P &\eqdef
%        %  \pointstoAT {\texttt{lk}} \True
%        %  \lor \left( \exists \vv.~\pointstoAT {\texttt{lk}} {(\False, \vv)} \ISEP P \opat \vv \right)
%      \end{align*}
%  \end{overprint}
%  \pause
%  \pause
%\end{frame}
%
%
%
%\begin{frame}[fragile]
%  \frametitle{Ticket lock}
%  A ticket lock implements a lock using two atomic integer variables:
%  \begin{columns}[T]
%    \begin{column}{0.52\textwidth}
%\begin{lstlisting}[language=camleff, basicstyle=\scriptsize\ttfamily]
%let rec wait_turn (turn,next) n =
%  if n <> !{at} turn
%  then wait_turn (turn,next) n
%
%let rec acquire (turn, next) =
%  let n = FAA next 1 in
%  wait_turn (turn, next) n
%\end{lstlisting}
%    \end{column}
%    \begin{column}{0.48\textwidth}
%\begin{lstlisting}[language=camleff, basicstyle=\scriptsize\ttfamily]
%let release (turn,next) =
%  turn :={at} !{at} turn + 1
%\end{lstlisting}
%    \end{column}
%  \end{columns}
%  %\br
%\end{frame}
%\begin{frame}
%  \frametitle{Ticket lock (cont.)}
%  It satisfies the interface: \vspace{-0.5cm}
%  \begin{columns}[T]
%    \begin{column}{0.45\textwidth}
%      \[
%        \hoareV%
%          {\textsf{isLock}~\texttt{lk}~\gamma~P \ISEP \textsf{ticket}~\gamma~n}%
%          {\texttt{wait\_turn lk n}}%
%          {P \ISEP \textsf{locked}~\gamma}%
%      \]
%      \[
%        \hoareV%
%          {\textsf{isLock}~\texttt{lk}~\gamma~P}%
%          {\texttt{acquire lk}}%
%          {P \ISEP \exists n.~\textsf{locked}~\gamma}%
%      \]
%    \end{column}
%    \begin{column}{0.55\textwidth}
%      \[
%        \hoareV%
%          {\textsf{isLock}~\texttt{lk}~\gamma~P \ISEP \textsf{locked}~\gamma \ISEP P}%
%          {\texttt{release lk}}%
%          {\textsf{True}}%
%      \]
%    \end{column}
%  \end{columns}
%  \ebr
%  \begin{overprint}
%    \onslide<2->
%      \alt<2>%
%        {The invariant in the sequentially consistent model is:}%
%        {The (objective) invariant in the weak model becomes:}
%      \begin{align*}
%        \textsf{lockInv}~\texttt{turn}~\texttt{next}~\gamma~P &\eqdef
%          \exists t, n, \vv.
%          \\& \phantom{\ISEP}
%          \pointstoAT {\texttt{turn}} {\RevealViewAT<3->{t}{\vv}}
%          \ISEP \pointstoAT {\texttt{next}} n
%          \\&
%          \ISEP \left(
%            \textsf{ticket}~\gamma~t
%            \lor \left( \textsf{locked}~\gamma \ISEP \alt<-2>{P}{P \opat \vv} \right)
%          \right)
%          \\&
%          \ISEP \textsf{own}~\gamma~(\authfull \ldots)
%      \end{align*}
%  \end{overprint}
%  \pause
%  \pause
%\end{frame}



\begin{frame}
  \frametitle{Model of the logic in Iris}
  \emph{Propositions are \only<2->{\textcolor{red}{monotonic} }predicates on views:}
  \begin{align*}
    \mathsf{vProp} &\eqdef \mathsf{view} \alt<1>{\longrightarrow}{{\color{red} {}\monarrow{}}} \mathsf{iProp} \\
    \seen \vv_0    &\eqdef \lambda \vv.~\vv_0 \sqsubseteq \vv \\
    P \isep Q      &\eqdef \lambda \vv.~P~\vv \isep Q~\vv \\
    P \wand Q      &\eqdef
      \lambda \Alt<1>{\vv}{{\color{red}\vv_1}}.~%
      \uncover<2->{{\color{red}\forall \vv \sqsupseteq \vv_1.}}~%
      P~\vv \wand Q~\vv
  \end{align*}
  \uncover<3>{%
  We equip a language-with-view with an operational semantics:
  \[
    \mathsf{exprWithView} \eqdef \mathsf{expr} \times \mathsf{view}
  \]
  Iris builds a WP calculus for $\mathsf{exprWithView}$ in $\mathsf{iProp}$.
  \br
  We derive a WP calculus for $\mathsf{expr}$ in $\mathsf{vProp}$ and prove adequacy:
  \[
    \mathsf{WP}~e~\phi \eqdef
      \lambda \Alt<1>{\vv}{{\color{red}\vv_1}}.~%
      \uncover<2->{{\color{red}\forall \vv \sqsupseteq \vv_1.}}~%
        \mathsf{WP}~\mkexpr{e}{\vv}~\left(\lambda \mkexpr{v}{\vv'}.~\phi~v~\vv'\right)
  \]
  where \(\phi : \mathsf{val} \rightarrow \mathsf{vProp}\)
}
\end{frame}



\begin{frame}
  \frametitle{Future work}
  Plans for the future:
  \begin{itemize}
    \item Prove more elaborate shared data structures
      \begin{itemize}
        \item \textit{e.g.} bounded queues with a circular buffer
      \end{itemize}
    \item Data races on non-atomics:
      \begin{itemize}
        \item How to allow them?
        \item What are they useful for?
      \end{itemize}
  \end{itemize}
\end{frame}



\end{document}
