\input{header}



% ------------------------------------------------------------------------------
% Beamer theme customization.

% Show authors and title in the footer (page number is already shown):
\setbeamertemplate{frame footer}{%
  \vspace{-3mm}%
  \insertshortauthor: \it\insertshorttitle%
}
% Set a background color for the footer line:
%\setbeamercolor{footline}{bg=teal!10}
% Appearance of the footer excl. the page number:
\setbeamercolor{frame footer}{fg=gray}
\setbeamerfont{frame footer}{size=\tiny,family=\normalfont}
% Apparance of the page number:
\setbeamercolor{footline}{fg=teal!75!black}
\setbeamerfont{page number in head/foot}{size=\normalsize,family=\bf}



% ------------------------------------------------------------------------------
% Headings.

\begin{document}

\title{Cosmo: a concurrent separation logic for Multicore~OCaml}
\author[%
  Mével,
  Jourdan,
  Pottier%
]{%
  \textbf{Glen Mével},
  Jacques-Henri Jourdan,
  François Pottier%
}
\institute{%
  LRI \& Inria, Paris, France%
}
%\date{August, 2020 \\ ICFP, ``New York''}
\date{September~21, 2020 \\ Nomadic Labs, Paris}

\begin{frame}[plain,noframenumbering]
  \setcounter{framenumber}{0}
  \titlepage
\end{frame}



% ------------------------------------------------------------------------------
% Main slides.



\begin{frame}%
  \frametitle{This talk}
  \emph{Our aim:}
  \begin{itemize}%
    \item Verifying
    \item fine-grained concurrent programs
    \item in the setting of Multicore OCaml’s memory model.
  \end{itemize}%
  \emph{Our contribution:}
  A concurrent separation logic with views.
\end{frame}%



\begin{frame}
  \frametitle{Multicore OCaml}
  \emph{Multicore OCaml}: OCaml language with multicore programming.
  \br
  \emph{Weak memory model for Multicore OCaml:} \\
  \begin{itemize}%
    \item Formalized in PLDI~2018.
    \item Two flavours of locations: ``atomic'', ``non-atomic''.
  \end{itemize}%
  \ebr
  \colorbox{black!15}{%
    (Also at ICFP~2020: \textit{Retrofitting Parallelism onto OCaml})
  }
\end{frame}



\begin{frame}%
  \frametitle{In traditional fine-grained concurrent separation logics...}
  We can assert ownership of a location and specify its value:
  \[
    \hoareV%
      {\pointstoSC x {42}\quad}%
      {\WriteSC x {44}}%
      {\pointstoSC x {44}}%
  \]
  Ownership can be shared between all threads via an invariant:
  \begin{align*}
    &\knowInv{}{\exists n \in \mathbb{N},\;\pointstoSC x n \ISEP \text{$n$ is even}} \vdash \\
    &\hoareV%
      {\TRUE\qquad}%
      {\WriteSC x {44}}%
      {\TRUE}%
  \end{align*}
\end{frame}%



\begin{frame}%
  \frametitle{The challenge: subjectivity}
  With weak memory, each thread has its own \emph{view} of memory.
  \br
  Some assertions are \emph{subjective}:
  \begin{itemize}
    \item Their validity relies on the thread’s view.
  \end{itemize}
  \ebr
  Invariants are \emph{objective}:
  \begin{itemize}%
    \item They cannot share subjective assertions.
  \end{itemize}%
  \ebr
  \strong{How to keep a simple and powerful enough logic?}
\end{frame}%



\begin{frame}%
  \frametitle{Key idea: modeling subjectivity with views}%
  A thread knows a subset~$\vv$ of all writes to the memory.
  \begin{itemize}%
    \item Affects how the thread interacts with memory.
    \item $\vv$ is the thread’s \strong{view}.
  \end{itemize}%
  \ebr
  New assertions:
  \begin{itemize}%
    \item
      \(\alert<1>{\seen\vv}\)
      :
      %the thread’s view contains~$\vv$.
      %the thread knows about all writes in~$\vv$.
      %the thread has seen~$\vv$, i.e.\ it knows about all writes in~$\vv$.
      we \emph{have seen}~$\vv$, i.e.\ we know all writes in~$\vv$.
    \item
      \(\alert<1>{\prop \opat \vv}\)
      :
      %$\prop$ holds in the eyes of any thread which has seen~$\vv$.
      %$\prop$ holds for any thread which has seen~$\vv$.
      %$\prop$ holds objectively for any thread which has seen~$\vv$.
      %$\prop$ holds \emph{objectively} if we have seen~$\vv$.
      %$\prop$ holds as long as the thread has seen~$\vv$.
      %$\prop$ holds as long as the memory is seen with view~$\vv$.
      having seen~$\vv$ is \emph{objectively} enough for $\prop$ to hold.
  \end{itemize}%
\end{frame}%



\begin{frame}%
  \frametitle{Key idea: decomposing subjective assertions}
  Decompose subjective assertions:
  \begin{center} \(
    \prop \Longleftrightarrow
    \exists \vv.~\underbrace{\prop \opat \vv}_\text{objective}
    \;\ISEP\!\!\! \underbrace{\seen \vv}_\text{subjective}
  \) \end{center}
  Share parts via distinct mechanisms:
  \begin{itemize}%
    \item
      \(\prop \opat \vv\)
      :
      via \emph{objective invariants}, as usual.
    \item
      \(\seen\vv\)
      :
      %transferred via \strong{``atomic'' accesses}.
      %via synchronization primitives of the language.
      via \emph{synchronization} offered by the memory model.
  \end{itemize}%
\end{frame}%



\sectionframe{Our program logic}



%\begin{frame}
%  \frametitle{Features of the logic}
%  Inherited from Iris:
%  \begin{itemize}
%    \item affine separation logic
%    \item \strong{objective} invariants
%    \item Iris-style ghost state
%  \end{itemize}
%  We add:
%  \begin{itemize}
%    \item views
%    \item ownership assertion $\pointstoAT x {(v, \vv)}$ (objective)
%    \item ownership assertion $\pointstoNA x v$ (subjective)
%    \item weakest preconditions
%  \end{itemize}
%\end{frame}



\begin{frame}
  \frametitle{Rules of atomic locations, simplified}
  $\pointstoAT x v$
  :
  the \emph{atomic} location~$x$ \strong{stores} the value~$v$.
  \begin{itemize}%
    \item \strong{Sequentially consistent}.
    \item \strong{Objective}.
    \item Standard rules:
  \end{itemize}%
  \begin{columns}
    \begin{column}{0.3\textwidth}
      %\emph{Atomic write:}
      \[
        \hoareV%
          {\pointstoAT x v}%
          {\WriteAT x {v'}}%
          {\lambda ().~\pointstoAT x {v'}}%
      \]
    \end{column}
    \begin{column}{0.5\textwidth}
      %\emph{Atomic read:}
      \[
        \hoareV%
          {\pointstoAT x v}%
          {\ReadAT x}%
          {\lambda v'.~v' = v \isep \pointstoAT x v}%
      \]
    \end{column}
  \end{columns}
\end{frame}



\begin{frame}
  \frametitle{Rules of non-atomic locations}
  $\pointstoNA x v$
  :
  \strong{we know} the latest value~$v$ of the \emph{non-atomic} location~$x$.%
  \hspace{-4cm}% hack to prevent a line break in the previous line
  \begin{itemize}%
    \item \strong{Relaxed}.
    \item \strong{Subjective} --- cannot appear in an invariant.
    \item Standard rules too!
  \end{itemize}%
  \begin{columns}
    \begin{column}{0.3\textwidth}
      %\emph{Non-atomic write:}
      \[
        \hoareV%
          {\pointstoNA x v}%
          {\WriteNA x {v'}}%
          {\lambda ().~\pointstoNA x {v'}}%
      \]
    \end{column}
    \begin{column}{0.5\textwidth}
      %\emph{Non-atomic read:}
      \[
        \hoareV%
          {\pointstoNA x v}%
          {\ReadNA x}%
          {\lambda v'.~v' = v \isep \pointstoNA x v}%
      \]
    \end{column}
  \end{columns}
\end{frame}



\begin{frame}%
  \frametitle{Example: transferring an assertion through a spin lock}
  \begin{columns}
    \begin{column}{0.1\textwidth}
    \end{column}
    \begin{column}{0.3\textwidth}
      \texttt{\begin{tabular}{l}%
        \uncover<2->{\hoareProp{\prop}} \\
        \uncover<4->{\hoareProp{\exists \vv.~%
          \alert<5>{\prop \opat \vv} \ISEP \alert<6->{\seen \vv}
        }} \\
        \uncover<1->{\codecomment{release lock:}} \\
        \uncover<1->{\WriteAT {lock} {\tikzPoint{SpinRel} \False}} \\
        \\
        \\
        \\
        \\
        \\
        \\
      \end{tabular}}%
    \end{column}
    \vrule\,\vrule
    \begin{column}{0.6\textwidth}
      \texttt{\begin{tabular}{l}%
        \\
        \\
        \\
        \\
        \uncover<1->{\codecomment{acquire lock:}} \\
        \uncover<-2>{\codekw{while} }%
        \tikzPoint{SpinAcq}%
        \uncover<1->{\CAS{lock}{\False}{\True}}%
        \uncover<-2>{\ = \False} \\
        \only<-2>{\codekw{do} () \codekw{done}}%
        \uncover<3->{\codecomment{CAS succeeds}} \\
        \uncover<4->{\hoareProp{\exists \vv.~%
          \alert<5>{\prop \opat \vv} \ISEP \alert<6->{\seen \vv}
        }} \\
        \uncover<2->{\hoareProp{\prop}} \\
      \end{tabular}}%
    \end{column}
  \end{columns}
  \begin{tikzpicture}[remember picture, overlay, ->, >=stealth', ultra thick, red]%
    \only<7-> \draw (SpinRel) to[out=-60,in=175] node[midway,below=2mm,left=0mm,fill=white,inner sep=0,outer sep=0] {happens before} (SpinAcq);
  \end{tikzpicture}%
  \begin{itemize}%
    \item<5>
      \(\prop \opat \vv\)
      :
      transferred via \emph{objective invariants}, as usual.
    \item<6->
      \(\seen\vv\)
      :
      transferred via \alt<6>
        {synchronization}
        {\strong{``atomic'' accesses}}.
  \end{itemize}%
\end{frame}



\begin{frame}
  \frametitle{Rules of atomic locations\only<1>{, simplified}}
  $\pointstoAT x {\RevealViewAT<2->{v}{\vv}}$
  :
  the atomic location~$x$ stores the value~$v$%
  \alt<2->{\\\ \strong{and a view} (at least)~$\vv$.}{.\\~}
  \begin{itemize}%
    \item Sequentially consistent behavior for~$v$.
    \item<2-> \strong{Release/acquire} behavior for~$\vv$.
    \item Objective\only<2->{\ (still)}.
    \item Rules:
  \end{itemize}%
  \begin{columns}
    \begin{column}{0.3\textwidth}
      %\emph{Atomic write:}
      \[
        \hoareV%
          {%
            \pointstoAT x {\revealViewAT<2->{v}{\vv}}
            \only<2->{{}\isep \alert{\seen \vv'%
              \tikzPoint{ReleaseFrom}%
            }}
          }%
          {\WriteAT x {v'}}%
          {\lambda ().~%
            \pointstoAT x {\revealViewAT<2->{v'}{\alert{\vv'%
              \tikzPoint{ReleaseTo}%
            }}}
          }%
      \]
    \end{column}
    \begin{column}{0.5\textwidth}
      %\emph{Atomic read:}
      \[
        \hoareV%
          {\pointstoAT x {\revealViewAT<2->{v}{\alert\vv%
              \tikzPoint{AcquireFrom}%
          }}}%
          {\ReadAT x}%
          {\lambda v'.~%
            v' = v
            \isep \pointstoAT x {\revealViewAT<2->{v}{\vv}
            \only<2->{{}\isep \tikzPoint{AcquireTo}%
              \alert{\seen \vv}%
            }}
          }%
      \]
    \end{column}
  \end{columns}
%
  \begin{tikzpicture}[remember picture, overlay, ->, >=stealth', ultra thick, red]%
    \only<3> \draw (ReleaseFrom) to[out=-60,in=30]  node[midway,right] {release} (ReleaseTo);
    \only<4> \draw (AcquireFrom) to[out=-60,in=135] node[midway,above] {acquire} (AcquireTo);
  \end{tikzpicture}%
\end{frame}



\sectionframe{Application: the spin lock}



\begin{frame}[fragile]
  \frametitle{The spin lock}
  A spin lock implements a lock using an atomic boolean variable:
  \begin{columns}[T]
    \begin{column}{0.1\textwidth}
    \end{column}
    \begin{column}{0.4\textwidth}
\begin{lstlisting}[language=camleff]
let release lk =
  lk :={at} false
\end{lstlisting}
    \end{column}
    \begin{column}{0.5\textwidth}
\begin{lstlisting}[language=camleff]
let rec acquire lk =
  if CAS lk false true
  then ()
  else acquire lk
\end{lstlisting}
    \end{column}
  \end{columns}
  \Alt<1>%
    {Interface:}%
    {\Alt<2>%
      {Invariant in traditional CSL:}%
      {Invariant in our logic (where $\prop$ is subjective!):}}%
  \begin{align*}
    &\knowInv{}{%
      \alt<1>{%
        \textsf{isLock}~\texttt{lk}~\prop
      }{%
        \pointstoAT {\texttt{lk}} \True
        \qquad\lor\qquad \left(
          \uncover<3->{\alert{\exists \vv.~}}
          \pointstoAT {\texttt{lk}} {\RevealViewAT<3->{\False}{\alert\vv}}
          \ISEP \prop \only<3->{\alert{{}\opat\vv}}
        \right)
      }
    }
    \vdash \\
    &\begin{cases}%
      \hoare%
        {\prop}%
        {\texttt{release lk}}%
        {\TRUE}%
      \\
      \hoare%
        {\TRUE}%
        {\texttt{acquire lk}}%
        {\prop}%
    \end{cases}%
  \end{align*}
  \begin{tikzpicture}[remember picture, overlay]%
    %\node<4> [cloud, cloud ignores aspect, cloud puffs=17,
    \node<4> [starburst, starburst points=17, starburst point height=7.5mm,
              fill=white, draw=orange, line width=2pt, rotate=-20, align=center]
      at ($(current page.south east)+(-30mm,20mm)$)
      {\strong{\!\!\!\!\!\!Sprinkle views over\!\!\!\!\!\!} \\
       \strong{\!\!\!\!\!\!your invariants!\!\!\!\!\!\!} };
  \end{tikzpicture}%
\end{frame}



\begin{frame}%
  \frametitle{Methodology}%
  More case studies:
  \begin{itemize}%
    %\item Spin lock
    \item Ticket lock
    \item Dekker mutual exclusion algorithm
    \item Peterson mutual exclusion algorithm
  \end{itemize}%
  \ebr
  Method for proving correctness under weak memory:%
  \begin{enumerate}%
    \item Start with the invariant under sequential consistency;
    \item Identify how information flows between threads;
      \begin{itemize}%
        \item i.e.\ where are the synchronization points;
      \end{itemize}%
    \item Refine the invariant with corresponding views.
  \end{enumerate}%
\end{frame}%



\sectionframe{Conclusion and roadmap}



\begin{frame}
  \frametitle{Conclusion}
  \emph{Key idea:} The logic of views enables concise and natural reasoning
  about how threads synchronize.
  \br
  Proofs are fully mechanized in Coq with the Iris framework. \coqstamp
  \br
  What has been done:
  \begin{itemize}%
    \item A high-level logic of views (in the paper).
    \item Model of the logic on top of a lower-level logic (in the paper).
    \item Case studies: locks, mutual exclusion (in the paper).
    \item Proof mode \textit{à la} Iris.
    \item Logical atomicity.
  \end{itemize}
  \ebr
  Future work:
  \begin{itemize}
    \item Formalize the \texttt{notify/wait} API.
    \item Verify more shared data structures.
    \item Allow data races on non-atomics.
  \end{itemize}
\end{frame}



\begin{frame}%
  \frametitle{Also done: logical atomicity}%
  We can open invariants around \strong{atomic} operations:
  \[
    \infer{%
      \knowInv{}{I} \vdash \hoare{\prop}{e}{\propB}
    }{%
      \hoare{\prop \isep I}{e}{\propB \isep I}
      & \text{$e$ atomic}
    }
  \]
  %\ebr
  Shortcoming: atomicity as a syntactical judgment.
  \begin{itemize}%
    \item \texttt{acquire lk} is not \strong{syntactically atomic}!
  \end{itemize}%
  %\ebr
  \emph{Done:} introduce a new kind of Hoare~triples such that:
  \[
    \infer{%
      \knowInv{}{I} \vdash \lahoare{\prop}{e}{\propB}
    }{%
      \lahoare{\prop \isep I}{e}{\propB \isep I}
    }
  \]
  We can now specify operations which are \strong{semantically atomic}, i.e.\ have a linearization point.
\end{frame}%



\begin{frame}%
  \frametitle{Ongoing work: more data structures}%
  \emph{In progress:} verify a concurrent FIFO with a circular buffer.
  \br
  Challenges:
  \begin{itemize}%
    \item Logical atomicity (done).
    \item Support for arrays (mostly done).
  \end{itemize}%
\end{frame}%



\begin{frame}%
  \frametitle{Future work: notify/wait}%
  Multicore~OCaml also provides blocking synchronization primitives:
  \begin{itemize}%
    \item[\texttt{\strong{notify}}] Wake a waiting thread.
    \item[\texttt{\strong{wait}}] Sleep until waked.
  \end{itemize}%
  Alternative to busy-waiting.
  \br
  \emph{To do:} support \texttt{notify/wait} in our logic.
  \br
  We expect the following semantics
  (not in the PLDI~2018 paper):
  \begin{itemize}%
    \item[\texttt{notify}] Akin to a release write.
    \item[\texttt{wait}] Akin to an acquire read.
  \end{itemize}%
\end{frame}%



\begin{frame}%
  \frametitle{Future work: data races}%
  Our logic does not support \strong{data~races} on non-atomics.
  \begin{itemize}%
    \item We cannot reason about racy programs.
  \end{itemize}%
  \ebr
  However they have an operational semantics (PLDI~2018).
  \br
  \emph{To do:} design a high-level logic supporting data~races on NAs.
  \br
  Broad idea: non-atomic cells should store \strong{monotonic} values.
  \br
  Case study: parallel graph searching.
  \begin{itemize}%
    \item Reuse the concurrent FIFO.
  \end{itemize}%
\end{frame}%



\begin{frame}[plain,noframenumbering]
  \begin{center}
    \Large \bfseries Questions?
  \end{center}
\end{frame}



\begin{frame}[plain,noframenumbering]
\end{frame}



% ------------------------------------------------------------------------------
% Backup slides.

\appendix



\begin{frame}%
  \frametitle{Verifying the spin lock}%
  \begin{columns}[T]
    \hspace{-10mm}
    \begin{column}{0.47\textwidth}
      \begin{tabular}{l}%
        \codecomment{release lk:} \\
        \hoareProp{\phantom{\exists\vv.~}\textsf{isLock}~\texttt{lk}~\prop
          \ISEP \prop} \\
        \hoareProp{\phantom{\exists\vv.~}\pointstoAT {\texttt{lk}} {\_}
          \quad\:\!\ISEP \prop} \\
        \!\hoareProp{\!\!\:\exists\vv.~\pointstoAT {\texttt{lk}} {\_}
          \quad\:\!\ISEP \overbrace{\seen \vv \ISEP \prop \opat \vv}\!} \\
        \WriteAT {\texttt{lk}} {\ \False} \\
        \hoareProp{\exists\vv.~\pointstoAT {\texttt{lk}} {(\False, \vv)}
          \ISEP \prop \opat \vv} \\
        \hoareProp{\phantom{\exists\vv.~}\textsf{isLock}~\texttt{lk}~\prop} \\
      \end{tabular}%
    \end{column}
    \vrule
    \begin{column}{0.53\textwidth}
      \begin{tabular}{l}%
        \codecomment{acquire lk:} \\
        \hoareProp{\textsf{isLock}~\texttt{lk}~\prop} \\
        \!\hoareProp{\!%
          \begin{aligned}%
            &\left( \exists\vv.~%
              \pointstoAT {\texttt{lk}} {(\False,\vv)} \ISEP \prop \opat \vv
            \right)
            \\\lor&\qquad\;\,\pointstoAT {\texttt{lk}} \True
          \end{aligned}%
        \!} \\
        \codekw{if} \CAS{\texttt{lk}}{\False}{\True} \\
        \codekw{then} \\
        \quad\!\hoareProp{\!\!\:\exists\vv.~\pointstoAT {\texttt{lk}} \True
          \ISEP \underbrace{\seen \vv \ISEP \prop \opat \vv}\!} \\
        \quad\hoareProp{\phantom{\exists\vv.~}\pointstoAT {\texttt{lk}} {\_}
          \quad\;\ISEP \prop} \\
        \quad\hoareProp{\phantom{\exists\vv.~}\textsf{isLock}~\texttt{lk}~\prop
          \,\ISEP \prop} \\
        \codekw{else} \\
        \quad\hoareProp{\pointstoAT {\texttt{lk}} \True} \\
        \quad\hoareProp{\textsf{isLock}~\texttt{lk}~\prop} \\
        \quad\texttt{acquire lk} \\
        \quad\hoareProp{\textsf{isLock}~\texttt{lk}~\prop
          \ISEP \prop} \\
      \end{tabular}%
    \end{column}
  \end{columns}
\end{frame}%



\begin{frame}
  \frametitle{Model of the logic in Iris}
  \emph{Assertions are \only<2->{\alert{monotonic} }predicates on views:}
  \begin{align*}
    \mathsf{vProp} &\eqdef \mathsf{view} \alt<1>{\longrightarrow}{\alert{{}\monarrow{}}} \mathsf{iProp} \\
    \seen \vv_0    &\eqdef \lambda \vv.~\vv_0 \sqsubseteq \vv \\
    P \isep Q      &\eqdef \lambda \vv.~P~\vv \isep Q~\vv \\
    P \wand Q      &\eqdef
      \lambda \Alt<1>{\vv}{\alert{\vv_1}}.~%
      \uncover<2->{\alert{\forall \vv \sqsupseteq \vv_1.}}~%
      P~\vv \wand Q~\vv
  \end{align*}
  We equip a language-with-view with an operational semantics:
  \[
    \mathsf{exprWithView} \eqdef \mathsf{expr} \times \mathsf{view}
  \]
  Iris builds a WP calculus for $\mathsf{exprWithView}$ in $\mathsf{iProp}$.
  \br
  We derive a WP calculus for $\mathsf{expr}$ in $\mathsf{vProp}$ and prove adequacy:
  \begin{align*}
    \mathsf{WP}~e~\phi &\eqdef
      \lambda \Alt<1>{\vv}{\alert{\vv_1}}.~%
      \uncover<2->{\alert{\forall \vv \sqsupseteq \vv_1.}}~%
      \\&\qquad
        {\color<3>{red}\mathop{\textsf{valid}}\vv} \wand
        \mathsf{WP}~\mkexpr{e}{\vv}~\left(
          \lambda \mkexpr{v}{\vv'}.~%
          {\color<3->{red}\mathop{\textsf{valid}}\vv'} \isep
          \phi~v~\vv'
        \right)
  \end{align*}
  where \(\phi : \mathsf{val} \rightarrow \mathsf{vProp}\)
\end{frame}



\begin{frame}
  \frametitle{Assertions are monotonic}
  Subjective assertions are \strong{monotonic} w.r.t.\ the thread’s view.
  \br
  One reason is the frame rule:
  \[
    \hoareV%
      {%
        \pointstoNA x v \ISEP \prop
        \only<2>{\text{\alert{\ --- holds at the thread’s current view}}}
      }%
      {\WriteNA x {v'}}%
      {\lambda ().~%
        \pointstoNA x {v'} \ISEP \prop
        \only<2>{\text{\alert{\ --- holds at the thread’s now extended view}}}
      }%
  \]
\end{frame}



\begin{frame}%
  \frametitle{Decomposition of subjective assertions}%
  This theorem allows us to decompose a subjective assertion~$\prop$:
  \begin{center} \(
    \prop
    \Longleftrightarrow
    \exists \vv.~\underbrace{\seen \vv}_\text{subjective}
    \ISEP \underbrace{\prop \opat \vv}_\text{objective}
  \) \end{center}
  We also have:
  \begin{center} \(
    \prop \opat \vv
    \Alt<1>{\>{}\Longrightarrow{}}{{}\Longleftrightarrow{}}
    {\color<1>{white}\objectively ({\color{black}\seen \vv \wand \prop})}
  \) \end{center}
  \pause
  where \(
    \objectively \propB
    \Longleftrightarrow
    (\forall \vv.~\propB \opat \vv)
    \Longleftrightarrow
    \propB \opat \emptyview
  \)
\end{frame}%



\end{document}
