\input{header}



% ------------------------------------------------------------------------------
% Beamer theme customization.

% Show authors and title in the footer (page number is already shown):
%\setbeamertemplate{frame footer}{%
%  \vspace{-3mm}%
%  \insertshortauthor: \it\insertshorttitle%
%}
% Set a background color for the footer line:
%\setbeamercolor{footline}{bg=teal!10}
% Appearance of the footer excl. the page number:
\setbeamercolor{frame footer}{fg=gray}
\setbeamerfont{frame footer}{size=\tiny,family=\normalfont}
% Apparance of the page number:
\setbeamercolor{footline}{fg=teal!75!black}
\setbeamerfont{page number in head/foot}{size=\normalsize,family=\bf}



% ------------------------------------------------------------------------------
% Headings.

\begin{document}

\title{%
  Specifying with separation logic \\
  a concurrent queue in weak memory \\[5mm]
  {\small (Spécification dans une logique de séparation \\
  d’une file concurrente en mémoire faible
  )}
}
\author[%
  Mével,
  Jourdan,
  Pottier%
]{%
  \textbf{Glen Mével},
  Jacques-Henri Jourdan,
  François Pottier%
}
\institute{%
  LMF \& Inria Paris%
}
\date{21~mai 2021 --- Journée Vocal}
%\date{July~6th, 2021 --- Nomadic Labs’ Research Seminar}

\begin{frame}[plain,noframenumbering]
  \setcounter{framenumber}{0}
  \titlepage
\end{frame}



% ------------------------------------------------------------------------------
% Main slides.



\sectionframe{Sequential queues}




\begin{frame}%
  \frametitle{Sequential specification}
  \[
    \hoareV
      {\TRUE}
      {\queuemake~\Unit}
      {\Lam \queue. \isQueueSEQ {[]}}
  \qquad
    \hoareV
      {\isQueueSEQ \elemList}
      {\enqueue~\queue~\elem}
      {\Lam \Unit. \isQueueSEQ {\elemListSnoc \elemList \elem}}
  \]
  \bigskip
  \[
    \hoareV
      {\isQueueSEQ \elemList}
      {\dequeue~\queue}
      {\Lam \elem. 1 \leq \nbelems \ISEP \elem = \elem_0 \ISEP \isQueueSEQ {\elemList<1>*}}
  \]
\end{frame}%



\sectionframe{Concurrent queues}



\begin{frame}%
  \frametitle{Concurrency}
  we now allow for several threads to run concurrently.
  \\
  for now, we assume \emph{sequential consistency}:
  the program behaves as an interleaving of the instructions of its threads.
  \\~\\
  how about the sequential spec?
  \pause
  valid, but…
  \\~\\
  $\isQueueSEQ \elemList$ is \emph{exclusive}
  \\ $\Longrightarrow$
  if we own it, we are the only one allowed to operate on~$\queue$
  %\\ $\Longrightarrow$
  %effectively no concurrent usage
\end{frame}%

\begin{frame}%
  \frametitle{Coarse grain concurrency}
  (Concurrent Separation Logic, O’Hearn, 2007)
  \\~\\
  idea: protect $\queue$ with a \emph{lock}
  \textcolor<7->{red}{(runtime operation)}:
  \[
    \pname{acquire}\;\ell\ ;\ 
    \enqueue\;\queue\;\elem\ ;\ 
    \pname{release}\;\ell
  \]
  the lock is associated with a logical assertion~$I$:
  \\~\\
  \Alt<1-2>{%
    \[
      \infer
        {%
          \hoare
            {\Alt<1>{\TRUE}{~~\prop}}
            {\pname{acquire}\;\ell}
            {\Alt<1>{\phantom{\prop\isep{}}I}{\prop \isep I}}
          \qquad\phantom{…}\qquad
          \hoare
            {\Alt<1>{I}{I \isep \propB}}
            {\pname{release}\;\ell}
            {\Alt<1>{\TRUE}{~\,\propB}}
        }{%
          \text{$\ell$ is a lock with assertion~$I$}
          \vphantom{\hoare{}{}{}}
        }
    \]
  }{%
    \[
      \infer
        {%
          \hoare
            {\prop}
            {\pname{acquire}\;\ell\ ;\ \expr\ ;\ \pname{release}\;\ell}
            {\propB}
        }{%
          \hoare{\prop \isep I}\expr{I \isep \propB}
        \qquad
          \text{$\ell$ is a lock with assertion~$I$}
        }
    \]
  }
  \\~\\
  \pause
  \pause
  \pause
  take \(
    I~\eqdef~\Exists \nbelems, \elemList[]. \isQueueSEQ \elemList
                                   \uncover<6->{\isep \propC\;\elemList}
  \)
  \pause
  \\ $\Longrightarrow$
  the lock itself owns \queue; anyone can get \queue{} by acquiring $\ell$
  \pause
  \\ $\Longrightarrow$
  the \emph{public state} $\elemList$ is only known in the lock
\end{frame}%



\begin{frame}%
  \frametitle{Fine grain concurrency}%
  (… Iris, 2015--present)
  \\~\\
  idea: share $\queue$ via an \emph{invariant} (no runtime component):
  \[
    \infer
      {%
        \hoare
          {\prop}
          {\expr}
          {\propB}
      }{%
        \hoare{\prop \isep I}\expr{I \isep \propB}
      \qquad
        \Alt<1>{\text{$I$ is an invariant}}{\qquad\quad\knowInv{}{I}}
      \qquad
        \textcolor<4->{red}{\text{$\expr$ is atomic}}
      }
  \]
  an invariant holds at all times.
  \\~\\
  \pause
  \pause
  \textcolor{gray}{take again \(
    I~\eqdef~\Exists \nbelems, \elemList[]. \isQueueSEQ \elemList
                                   \isep \propC\;\elemList
  \)}
  %\pause
  \\ $\Longrightarrow$
  the invariant owns \queue; anyone can get \queue{} by “opening” $I$
  %\pause
  \\ $\Longrightarrow$
  \textcolor{gray}{the public state $\elemList$ is only known in the invariant}
\end{frame}%



\begin{frame}%
  \frametitle{Logical atomicity}%
  (… Iris, 2015--present)
  \\~\\
  idea: define \emph{logically atomic triples} $\lahoare\cdot\cdot\cdot$
  as triples  $\hoare\cdot\cdot\cdot$ such that the following holds:
  \[
    \infer
      {%
        \lahoare
          {\uncover<4->{\var.} \prop}
          {\expr}
          {\propB}
      }{%
        \lahoare{\uncover<4->{\var.} \prop \isep I}\expr{I \isep \propB}
      \qquad
        \knowInv{}{I}
      }
    \uncover<5->{%
    \qquad\qquad
    \infer
      {%
        \Forall \var.
        \hoare
          {\prop}
          {\expr}
          {\propB}
      }{%
        \lahoare
          {\var. \prop}
          {\expr}
          {\propB}
      }
    }
  \]
  \pause
     $\Longrightarrow$
  tells that $\expr$ behaves atomically
  \pause
  \\ $\Longrightarrow$
  corresponds to \emph{linearisability}
  \\ \phantom{$\Longrightarrow$}
  intuition: $\expr$ has a linearization point satisfying
  $\uncover<4->{\Forall \var.}\hoare\prop\cdot\propB$
  \\~\\
  \pause
  $\var$ binds things which are only known at the linearization point
  \\ (e.g.\ the public state of the queue)
\end{frame}%



\begin{frame}%
  \frametitle{Sequentially consistent specification}
  \[
    \hoare
      {\TRUE}
      {\queuemake~\Unit}
      {\Lam \queue. \Exists \gqueue. \queueInv \ISEP \isQueueSC {[]}}
  \]
  \medskip
  \alt<3->{%
    \[
      \infer{%
      \lahoareV
        <\nbelems, \elemList*[]>
        {\isQueueSC \elemList}
        {\enqueue~\queue~\elem}
        {\Lam \Unit. \isQueueSC {\elemListSnoc \elemList \elem}}
      }{\queueInv}
    \]
    \bigskip
    \[
      \infer{%
      \lahoareV
        <\nbelems, \elemList*[]>
        {\isQueueSC \elemList}
        {\dequeue~\queue}
        {\Lam \elem. 1 \leq \nbelems \ISEP \elem = \elem_0 \ISEP \isQueueSC {\elemList<1>*}}
      }{\queueInv}
    \]
  }{%
    \\
    $\queueInv$:
    knowledge that \queue{} is a queue in \textit{some} state
    \uncover<2->{%
    \\ $\Longrightarrow$
    persistent (duplicable), freely copied among threads
    }
    \\~\\
    $\isQueueSC \elemList$:
    knowledge and ownership of the state
    \uncover<2->{%
    \\ $\Longrightarrow$
    exclusive (non-duplicable), shareable via an invariant
    }
    \\~\\
    $\gqueue$: ghost name for the public state
    \vspace{4cm}
  }
  \pause
\end{frame}%



\sectionframe{Concurrent queues in weak~memory}



\begin{frame}
  \frametitle{Example: sequential consistency \only<5->{\color{red}violated}}
  Consider this concurrent program:
  \br
  \qquad\texttt{\begin{tabular}{l||l}%
    \multicolumn{2}{c}{x := 0} \\
    \multicolumn{2}{c}{y := 0} \\
    x := 1\tikzPoint{T11}  &  \tikzPoint{T21}y := 1  \\
    A := y\tikzPoint{T12}  &  \tikzPoint{T22}B := x  \\
  \end{tabular}}%
  \begin{tikzpicture}[remember picture, overlay, ->, ultra thick]
    \only<2> \draw[blue] (T11) -- (T12) -- (T21) -- (T22);
    \only<3> \draw[blue] (T21) -- (T22) -- (T11) -- (T12);
    \only<4> \draw[blue] (T11) -- (T21) -- (T12) -- (T22);
    \only<5->\draw[red]  (T12) -- (T22) -- (T21) -- (T11);
  \end{tikzpicture}%
  \br
  Possible outcomes (A, B): \hspace{1ex}
    \textcolor<2>{blue}{(0, 1)},
    \textcolor<3>{blue}{(1, 0)},
    \textcolor<4>{blue}{(1, 1)}%
    \only<5->{, \textcolor{red}{(0, 0)}}.
  \\
  Observed {\tiny(M.OCaml on 2-core x86-64)}:
    \textcolor<2>{blue}{91\%},
    \textcolor<3>{blue}{9\%},
    \textcolor<4>{blue}{0.001\%}%
    \only<5->{, \textcolor{red}{0.1\%}}.
  \br
  \uncover<6->{\textcolor{red}{The compiler may reorder a write after a read.}} \\
  \uncover<6->{\textcolor{red}{(The processor too.)}}
\end{frame}



\begin{frame}%
  \frametitle{Weak memory models}%
  %SC is too costly to enforce
  %\\ $\Longrightarrow$
  %most systems give weaker guarantees on how a concurrent program behaves
  %\\~\\
  %SC is violated
  %\begin{itemize}%
  %  \item by processors (e.g.\ store buffering)
  %  \item by compilers (e.g.\ common sub-expression elimination)
  %\end{itemize}%
  %most systems (CPUs/compilers) give weaker guarantees on concurrent behaviors
  %\\~\\
  \emph{weak memory model}:
  each thread has its own \emph{view} of the state of the shared memory
  \begin{itemize}%
    \item examples: C11, \alt<2->{\emph{Multicore OCaml}}{Multicore OCaml}
  \end{itemize}%
  these models have been given an operational, interleaving semantics
  with a notion of views
  \\~\\
  \pause
  \pause
  \emph{Cosmo}: a program logic for M-OCaml based on this semantics
\end{frame}



\begin{frame}%
  \frametitle{Cosmo}%
  based on Iris
  (hence: separation logic, ghost state, \textcolor<2->{red}{invariants})
  \\~\\
  assertions are \emph{subjective}: their validity depends on the thread’s
  current view of memory (a~priori)
  \begin{itemize}%
    \item examples: $\pointsto\var{42}$
  \end{itemize}%
  \pause
  \textcolor{red}{issue:} invariants are available to all threads equally
  \pause
  \\ $\Longrightarrow$
  restricted to \emph{objective} assertions
  \\~\\
  \pause
  $\isQueueSC \elemList$ must be objective
  (part of the spec)
\end{frame}%



\begin{frame}%
  \frametitle{Synchronization through the queue}%
  how about the SC spec?
  \pause
  valid, usable in limited cases, but…
  \\~\\
  does not say anything about \emph{synchronization}
  \\ $\Longrightarrow$
  when transferring a complex value, the dequeuer needs to
  \\ \phantom{$\Longrightarrow$} know about all writes done by the enqueuer to that value
  \pause
  \\ $\Longrightarrow$
  i.e.\ it needs to acquire the enqueuer’s view of memory
\end{frame}%



\begin{frame}%
  \frametitle{Views in Cosmo}%
  $\view, \viewB, …$
  \\~\\
  views form a lattice (larger = more knowledge)
  \pause
  \\~\\
  subjective assertion: $\seen\view$ means “the current view contains $\view$\,”
\end{frame}%



\begin{frame}%
  \frametitle{Transferring views}%
  idea: store views in the queue!
  \[\isQueue{}{}{\!\!\!\elemViewList}\]
  \alt<1>{%
  the enqueuer puts its view alongside the enqueued value:
  \[
    \hspace{-1cm}
    \infer{%
    \lahoareV
      {\begin{array}{@{}l@{}}
          \lahoarebinder{\nbelems, \elemViewList*[]}
          \\  \hphantom{\Lam\Unit.}
              \isQueue{}{} {\!\!\!\elemViewList}
              \hphantom{, \pair\elem\view}
        \ISEP \textcolor{blue}{\seen\view}
      \end{array}}
      {\enqueue~\queue~\elem}
      {\Lam \Unit.
              \isQueue{}{} {\!\!\!\elemListSnoc \elemViewList {\pair\elem{\textcolor{blue}{\view}}}}
      }
    }{\queueInv}
  \]
  }{%
  the dequeuer acquires that view:
  \[
    \hspace{-1cm}
    \infer{%
    \lahoareV
      {\begin{array}{@{}l@{}}
          \lahoarebinder{\nbelems, \elemViewList*[]}
          \\  \hphantom{\Lam\elem.}
              \isQueue{}{} {\!\!\!\elemViewList}
      \end{array}}
      {\dequeue~\queue}
      {\Lam \elem.
              \isQueue{}{} {\!\!\!\elemViewList<1>*}
        \ISEP \textcolor{blue}{\seen \eview_0}
        \ISEP 1 \leq \nbelems
        \ISEP \elem = \elem_0
      }
    }{\queueInv}
  \]
  }
  \pause
\end{frame}%



\begin{frame}%
  \frametitle{Transferring more views}%
  we can also specify synchronization
  \begin{itemize}%
    \item from an enqueuer to the next enqueuer \uncover<2->{$\Longrightarrow$ view~$\hview$}
    \item from a dequeuer to the next dequeuer \uncover<2->{$\Longrightarrow$ view~$\tview$}
  \end{itemize}%
  \pause
  \pause
  \bigskip
  \[
    \hspace{-16mm}
    \footnotesize
    \infer{%
    \lahoareV
      {\begin{array}{@{}l@{}}
          \lahoarebinder{\tview, \hview, \nbelems, \elemViewList*[]}
          \\  \hphantom{\Lam\Unit.}
              \isQueue \tview {\hphantom(\hview\hphantom{{}\viewjoin\view)}} \elemViewList
              \hphantom{, \pair\elem\view}
        \ISEP \seen\view
      \end{array}}
      {\enqueue~\queue~\elem}
      {\Lam \Unit.
              \isQueue \tview {(\hview \sqcup \view)} {\elemListSnoc \elemViewList {\pair\elem\view}}
        \ISEP \seen\hview
      }
    }{\queueInv}
  \]
  \bigskip
  \[
    \hspace{-16mm}
    \footnotesize
    \infer{%
    \lahoareV
      {\begin{array}{@{}l@{}}
          \lahoarebinder{\tview, \hview, \nbelems, \elemViewList*[]}
          \\  \hphantom{\Lam\elem.}
              \isQueue {\hphantom(\tview\hphantom{{}\viewjoin\view)}} \hview \elemViewList
        \ISEP \seen\view
      \end{array}}
      {\dequeue~\queue}
      {\Lam \elem.
              \isQueue {(\tview \sqcup \view)} \hview {\elemViewList<1>*}
        \ISEP \seen \tview
        \ISEP \seen \eview_0
        \ISEP 1 \leq \nbelems
        \ISEP \elem = \elem_0
      }
    }{\queueInv}
  \]
\end{frame}%



\sectionframe{Conclusion}



\begin{frame}%
  \frametitle{Conclusion}%
  Logical atomicity + reasoning with views = powerful tools for specifying
  (and proving!) concurrent data~structures in weak memory.
\end{frame}%



\end{document}
