# 0. TITLE/INTRO

Hello, I’m Glen Mével and today I will show you how to specify 
a concurrent datastructure under weak memory.

Nowadays concurrency is everywhere, and all real-world shared 
memories have some counter-intuitive behaviors.
So not only do we want to verify concurrent datastructures, 
but we want to verify them against an ACTUAL model of memory 
—what we call a “weak” memory model.

Our paper’s contribution is a specification AND A PROOF of a, 
particular, fine-grained concurrent queue, in a weak memory 
model which is that of Multicore OCaml.

In this talk, we won’t cover the proof nor the implementation, 
but we’ll see how to write a usable spec for it.

CONCURRENCY poses a number of challenges.

We want to specify a datastructure, such that we allow sharing 
it between threads. So one challenge is with shared ownership, 
and as we’ll see, solving this involves some form of ↘atomicity. 
The operations of a concurrent datastructure must behave as if 
they were atomic, and it’s something we must specify, somehow.

Another challenge is with WEAK MEMORY. With weak memory, you 
need to be explicit about how threads synchronize. So specs must 
expose useful synchronizations. And often, fine-grained 
implementations offer less synchronizations than a lock-based 
one.

We will tackle these challenges by using Cosmo, which is our 
concurrent program logic for Multicore OCaml.

# 1. SEQ SPEC

Just to get familiar with notations, let’s start with 
a NON-concurrent specification.

We have 3 operations: ‘make’, ‘enqueue’, ‘dequeue’. We specify 
them using Hoare tri-ples. A tri-ple is made of a precondition 
formula, a program fragment and a postcondition formula.

We have a representation predicate ‘IsQueue‘, which asserts that 
q is a memory location storing a well-formed queue 
datastructure, whose items are, in order, the values in the 
given list.

We are in separation logic, so conjunction is denoted with an 
asterisk ‘∗’, and the assertion IsQueue is NOT duplicable; 
rather, it asserts the UNIQUE ownership of the queue.

# 2. CONCURRENCY

Now! Let’s enter concurrency. For now, let’s assume “sequential 
consistency”; that is, a program always behaves as some 
interleaving of its threads.

We want to specify a queue which can be used concurrently. Can 
we simply keep the spec that we just saw? Well, this spec is 
still valid; but to use it, a thread needs the unique ownership 
of the queue, so that effectively prevents concurrent uses.

And it makes sense, because there is no reason to believe that 
the datastructure is thread-safe… unless… we specify it.

# TRANSITION

So let’s consider a queue that IS thread-safe. To see how to 
reflect thread-safety in the spec, let’s adopt for a moment the 
point of view of the user.

# 3. INVARIANTS

As a USER of a concurrent datastructure, we want to share its 
ownership among threads. And in a concurrent separation logic, 
such as Iris, we can do it by putting it in an “invariant”.

An invariant is an assertion which holds at all times, and in 
which we can put whatever resource we want. Then, it is the 
invariant ITSELF which owns the resource and ANY thread can 
access it.

To access it, we “open” the invariant around a program fragment 
e, that is, before running e we obtain the contents of the 
invariant, and afterwards we return it.

Now the issue: for soundness in the face of concurrency, we can 
only open an invariant for AT MOST one step of execution.

This is a problem because the number of steps is a low-level 
implementation detail, and as a user, we have no idea whether 
the operations complete in one step — and it’s unlikely. So the 
user cannot use a queue-sharing invariant, around the specs that 
we have written earlier.

# TRANSITION

But doing so would be reasonable, because a concurrent queue has 
to be implemented in such a way that its operations LOOK atomic 
to the user.

# 4. LOGICAL ATOMICITY

For this, Iris provides us a notion of “logically atomic 
tri-ples”, which we’ll denote with angle brackets (instead of 
curly brackets). It’s something we can define in the logic, 
I won’t show how, but the key point is this couple of 
properties.

First, a logically atomic tri-ple entails the regular tri-ple.

Second, we can open invariants around it freely. This is a way 
of stating that e is “logically atomic”. By that, we mean more 
formally that there is a step in the execution of e, such that 
e behaves as if everything happened during that step.

It is this step which satisfies the Hoare tri-ple with P and Q. 
So P and Q need not hold at the start and end of e, but only 
just before and just after the atomic step.

Because of this, we need a binder to be able to name things 
which we only know during the atomic step, when we actually open 
the invariant. So here, x is bound in both P and Q.

# 5. SC SPECS

Fine! With logical atomicity, let’s strenghten our previous spec 
for concurrent uses.

The spec of ‘make’ does not change, because we do not need it to 
be atomic.

For ‘enqueue’ and ‘dequeue’, we do the following changes:
  - we replace the regular tri-ples with logically atomic 
    tri-ples;
  - we bind the queue state explicitely in the tri-ples;
  - and that’s it.

# TRANSITION

We now have a usable spec for a concurrent queue, but that was 
under sequential consistency. That’s unrealistic in the modern 
world, so we have to embrace weaker models of memory.

# 6. WEAK

A well-known one is that of C11, and here we are interested in 
that of Multicore-OCaml.

A way of describing such models is by saying that each thread 
has its own VIEW of the shared state.

For Multicore OCaml this has been made formal by people at 
Cambridge and, the reference is on the slide, and based on this, 
we have built Cosmo, a program logic for Multicore-OCaml.

# 7. COSMO

A few words about Cosmo. Cosmo is based on Iris, hence we 
inherit all of its features: separation logic and so on.

Its distinctive trait is that in general, assertions are 
“subjective”: we are reasoning under an implicit ambient view, 
which represents the current view of the current thread, and 
assertions may depend on it.

For instance, ‘x pointsto 42’ is subjective: maybe WE see that 
x is storing the value 42 because WE have written it, but our 
write has not been propagated to other threads yet, and THEY 
still see an earlier value for x.

With that said, a limitation with subjective assertions is that 
invariants are still available to ALL threads. So the assertions 
that they store canNOT depend on the view of a specific thread. 
They are restricted to “objective” assertions.

Back to our queue: recall that we want to share the queue in an 
invariant. So, its representation predicate must be objective. 
And we make this objectivity requirement a part of the specs.

# 8. SYNC IN THE QUEUE

Do we need any other modification to the previous spec? It is 
still usable in limited cases, but for the general case it lacks 
THREAD SYNCHRONIZATION.

Let’s consider an example. Thread A, writes to some memory 
region, then enqueues a pointer to that region. Thread B, 
dequeues the pointer. Now thread B expects to read the memory 
region in the state in which A has left it.

But remember that we are in weak memory. So it’s not because A, 
knows it has written 3 to ‘x bracket 1’, that B knows it too.

So we want the queue to guarantee that, when dequeuing, B learns 
about all writes known by A.

It’s a typical “release-acquire” pattern.

This is a side effect that we need, so it must be specified.

From the logical perspective, the user wants to transfer 
pointsto assertions, from A to B. But as I said, pointsto 
assertions are subjective, so the user cannot simply put them in 
an invariant. We need something more.

# 9. VIEWS IN COSMO

It’s time for me to explain how views and synchronization work 
in Cosmo. We have an abstract notion of views.

It comes equipped with an inclusion relation: the larger, the 
more up-to-date.

To manipulate views, we have new kinds of assertions.

First, ‘Uparrow V‘ means that the ambient view contains V. Thus 
it’s the archetypical subjective assertion.

Second, ‘P at V’ where P is any assertion, means “P, with the 
ambient view fixed to V”. ‘P at V’ is objective even if P is 
subjective.

These assertions satisfy several rules, in particular this one.

ANY assertion P can be decomposed into an assertion ‘Uparrow V’ 
and an assertion ‘P at V’, for some view V; and conversely.

That’s HOW we can transfer P between threads: we isolate all of 
its subjective knowledge into a component ‘Uparrow V’, so that 
what’s left is an objective assertion, ‘P at V’, which we can 
share in an invariant.

So, to achieve a transfer of P, we just have to transfer view 
assertions, and THIS, has a RUNTIME meaning: this is thread 
synchronization.

# 10. TRANSFER VIEWS QUEUE (aka SPEC WEAK)

Back to our queue again, we want to say that, for each item, 
there is a synchronization —that is, a view transfer— from the 
enqueuer to the dequeuer. The idea is to pretend that the queue 
stores the views being transferred. So there is now one view for 
each item, and the representation predicate now looks like this.

If we recall the previous spec, it is now augmented with views, 
and the view transfer happens like that.
  - The enqueuer PUSHES its current view into the queue, 
    alongside the enqueued item;
    in the pre, we use an assertion ‘Uparrow V’ for capturing 
    the current view —or in fact, any portion of it, as chosen 
    by the user of the spec.
  - Conversely, the dequeuer PULLS the view that comes with the 
    item.
    It gets that same ‘Uparrow V’, which means that now its 
    current view includes V.

So it’s a release-acquire pattern.

# 12. REFINEMENT

I’d like to draw your attention to something. You may wonder how 
this approach to specification compares with refinement.

In a typical refinement-based spec, you would say something 
like: “this queue can be used everywhere we can use a naïve 
queue protected by a lock.” But, in weak memory, this spec has 
a shortcoming: the lock induces synchronizations between ALL 
pairs of operations, even though many lock-free queues do not 
guarantee them. Because precisely, we try to reduce unnecessary 
synchronizations.

By contrast, our spec is weaker, for instance it does not 
guarantee any dequeuer-to-enqueuer synchronization. So we can 
specify more implementations.

# 13. CONCLUSION

To recap, I’ve shown several tools for reasoning about 
concurrent programs.

INVARIANTS are well-known already.

To use invariants to their full power, we need to pay attention 
to atomicity in specs. And for that, we have LOGICALLY ATOMIC 
TRI-PLES.

What’s new is that now, we want to support weak memory, and for 
that, we have VIEWS.

My claim is that the combination of all of these tools works, 
—logical atomicity is compatible with weak memory– and it’s 
expressive enough for specifying and proving fine-grained 
concurrent programs, while remaining quite natural to use.

I’ve only talked about specification, but there is more in the 
paper. We have a proof of the spec for a non-trivial lock-free 
queue. It demonstrates the benefit of our approach because this 
implementation has less synchronizations than a lock-based 
queue, so it’s not a refinement.

By the way, at the time we were working on that proof, other 
people were working on a refinement proof of a very similar 
datastructure, under sequential consistency, so you can have 
a look at their paper too.

We also have a proof of a simple client application which 
demonstrates that our spec is usable.

All of this takes place in Iris, a very powerful separation 
logic framework, and, last but not least, by the virtue of Coq 
everything is machine-checked.

Thank you for your attention.

# vim: set tw=64 et ts=2 fo+=taw:
