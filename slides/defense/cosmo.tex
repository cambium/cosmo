% ------------------------------------------------------------------------------
% Main slides.



%\sectionframe{Concurrent Separation Logic, \\ for sequential consistency}
\sectionframe{\mbox{Verifying SC concurrent programs} \\ with Concurrent Separation Logic}



\begin{frame}%
  \frametitle{Specifying
    \Alt<-1>
      {a program \slidebib{Hoare, 1969}}
      {with Separation Logic
      \!\slidebib{Reynolds\,1999; O\!’Hearn\,2007; Iris\,2015; ...}}
  }%
  \strong{Hoare triple}:
  %\begin{center}
    \(
      \hoare {Pre} e {Post}
    \)
  %\end{center}
  \begin{itemize}%
    \item $e$: program code
    \item $Pre$: precondition (logical assertion about the computer state)
    \item $Post$: postcondition (ditto)
  \end{itemize}%
  ``If we run $e$ from a state that satisfies~$Pre$,
  and if it terminates,
  \\ then it ends in a state that satisfies~$Post$.''
  \pause
  \br
  $Pre$ and $Post$ are stated in \strong{Separation Logic}:
  \begin{itemize}%
    \item an assertion represents the ownership of a resource
    \item the separating conjunction $P \isep Q$ asserts ownership
      of two \emph{distinct} resources $P$ and $Q$
    \item in general, $P \not\Rightarrow P \isep P$
  \end{itemize}%
\end{frame}%



\begin{frame}%
  \frametitle{Resources and locks}%
  Portions of memory are ownable \strong{resources}
  \begin{itemize}%
    \item example: $\pointsto a {\texttt{"azerty"}}$
      \\ \quad ``Movix account $a$, whose current password is $\texttt{"azerty"}$''
  \end{itemize}%
  %\br
  We can guard such resources by using a \strong{lock} (like the totem)
  \br
  Two operations for a lock~$lk$ guarding a resource~$R$:
  \begin{itemize}%
    \item \texttt{acquire $lk$}
      \\ \quad grants $R$:
      we become its unique owner
    \item \texttt{release $lk$}
      \\ \quad reclaims $R$:
      we give it back and stop owning it
  \end{itemize}%
  \pause
  Formal specification?
\end{frame}%



\begin{frame}%
  \frametitle{Specification of a lock}%
  \[
    \hspace{-10mm}
    \begin{array}{@{}l@{}}
      \uncover<2->{%
        \knowInv{}{\textsf{isLock}~lk~R}\vdash
      }
      \qquad\qquad\quad\text{if $lk$ is a lock that guards $R$, then}
    \\
      \quad\begin{cases}%
        \uncover<2->{%
          \hoare%
            {\TRUE}%
            {\texttt{acquire lk}}%
            {R}%
        }
        &\text{acquiring $lk$ assumes nothing and grants $R$}
        \\
        \uncover<2->{%
          \hoare%
            {R}%
            {\texttt{release lk}}%
            {\TRUE}%
        }
        &\text{releasing $lk$ reclaims $R$ and grants nothing}
      \end{cases}%
    \end{array}
  \]
  \pause
  \pause
  \ebr
  $\textsf{isLock}~lk~R$ is an assertion describing the internal layout of $lk$
  \\ $\Longrightarrow$ asserts unique ownership of $lk$
  \br
  $\knowInv{}{\textsf{isLock}~lk~R}$ is an Iris invariant containing $\textsf{isLock}~lk~R$
  \\ $\Longrightarrow$ shares $lk$ among all threads
\end{frame}%



\begin{frame}[fragile]
  \frametitle{The spin lock} % in SC
  A spin lock implements a lock using a Boolean reference:
  \begin{columns}[T]
    \begin{column}{0.1\textwidth}
    \end{column}
    \begin{column}{0.4\textwidth}
\begin{lstlisting}[language=camleff]
let release lk =
  lk := false
\end{lstlisting}
    \end{column}
    \begin{column}{0.5\textwidth}
%\begin{lstlisting}[language=camleff]
%let rec acquire lk =
%  if CAS lk false true
%  then ()
%  else acquire lk
%\end{lstlisting}
\begin{lstlisting}[language=camleff]
let try_acquire lk =
  CAS lk false true
\end{lstlisting}
    \end{column}
  \end{columns}
  \pause
  Described by this assertion:
  %\[
  %  \textsf{isLock}~lk~R
  %  \quad\eqdef\quad
  %  \pointsto {lk} \True
  %  \LOR
  %  (\pointsto {lk} \False \isep R)
  %\]
  \[
    \textsf{isLock}~lk~R
    \quad\eqdef\quad
    \Exists b.~%
    \pointsto {lk} b \ISEP (b = \False \Rightarrow R)
  \]
  \pause
  Specification of operations used by the spin lock:
  %\scriptsize
  \renewcommand\NA{} % just for this slide
  % NA write
  \[
    \hoareV%
      {\pointstoNA x v}%
      {\WriteNA x v'}%
      {%\lambda ().~
        \pointstoNA x {v'}\quad~~}%
  %\qquad
  %% NA read:
  %  \hoareV%
  %    {\pointstoNA x v}%
  %    {\ReadNA x}%
  %    {\lambda v'.~v' = v \isep \pointstoNA x v}%
  %\qquad
  %% successful CAS:
  %  \hoareV%
  %    {\pointstoNA x {v_1}}%
  %    {\CAS x {v_1} {v_2}}%
  %    {\lambda b.~b = \True \isep \pointstoNA x {v_2}}%
  %\qquad
  %% failed CAS:
  %  \hoareV%
  %    {\pointstoNA x v}%
  %    {\CAS x {v_1} {v_2}}%
  %    {\lambda b.~b = \False \isep \pointstoNA x v}%
  %  \text{ if $v \neq v_1$}
  % CAS, combined spec:
  \qquad\qquad
    \hoareV%
      {\pointstoNA x v}%
      {\CAS x {v_1} {v_2}}%
      {\begin{array}{@{}l@{}}
        %\lambda b.~b \in \{ \False, \True \} \\
        %  \isep
          \ITE{ret}
            {\pointstoNA x {v_2} \ISEP v=v_1}
            {\pointstoNA x v}
      \end{array}}%
  \]
\end{frame}



\begin{frame}%
  \frametitle{Verifying the spin lock in Concurrent Separation Logic \slidebib{Iris,\,2015}}%
  \renewcommand\NA{} % just for this slide
  \vspace{-7mm}%
  \[
    \textsf{isLock}~lk~R
    \quad\eqdef\quad
    \Exists b.~%
    \pointsto {lk} b \ISEP (b = \False \Rightarrow R)
  \]%
  \vspace{-4mm}%
  \begin{columns}[T]
    \hspace{-5mm}
    \begin{column}{0.46\textwidth}
      \uncover<-1>{%
        \(\color{green!40!black}\knowInv{}{\textsf{isLock}~lk~R}\vdash\)
      }
      \\
      \(\begin{codeblock}[]%
        \codecomment{release:} \\
        \hoareProp{\only<2->{\textsf{isLock}~lk~R \ISEP} R} \\
        \uncover<3->{\hoareProp{\pointsto {lk} \_~~~~~\;\!\ISEP R}} \\
        \WriteNA {lk} \False \\
        \uncover<3->{\hoareProp{\pointsto {lk} \False \ISEP R}} \\
        \hoareProp{\alt<2->{\textsf{isLock}~lk~R}{\TRUE}} \\
      \end{codeblock}\)%
    \end{column}%
    %\vrule%
    \begin{column}{0.54\textwidth}
      \uncover<-1>{%
        \(\color{green!40!black}\knowInv{}{\textsf{isLock}~lk~R}\vdash\)
      }
      \\
      \(\begin{codeblock}[]%
        \codecomment{try\_acquire:} \\
        \hoareProp{\alt<2->{\textsf{isLock}~lk~R}{\TRUE}} \\
        \uncover<3->{\hoareProp{%
          \pointsto {lk} b \ISEP (b = \False \Rightarrow R)
        }} \\
        \CAS{lk}{\False}{\True} \\
        \uncover<3->{\!\hoareProp{\!\ITE{ret}
          {\pointsto {lk} \True \ISEP R}
          {\pointsto {lk} b \ISEP (b = \False \Rightarrow R)}
        \!}} \\
        \uncover<3->{\!\hoareProp{\!
          \ITE{ret}
            {\textsf{isLock}~lk~R \ISEP R}
            {\textsf{isLock}~lk~R}
        \!}} \\
        \hoareProp{%
          \only<2->{\textsf{isLock}~lk~R \ISEP}
          (ret = \True \Rightarrow R)
        } \\
      \end{codeblock}\)%
    \end{column}
  \end{columns}
\end{frame}%



%\sectionframe{Cosmo: a concurrent separation logic for Multicore OCaml}
\sectionframe{\mbox{Verifying Multicore OCaml programs} \\ with Cosmo}



\begin{frame}
  \frametitle{
    \alt<-2>
      {Using a lock}
      {Passing a message}
    \alt<-12>
      {} % in SC
      {in Multicore OCaml}
  }
  \Alt<-2>
    {Example of using a lock~$lk$ to guard accesses to $pw$:}
    {Passing a write to~$pw$ from the left thread to the right thread:}
  \br
  \begin{center}%
    \texttt{\begin{tabular}{l||l}%
          \multicolumn{2}{c}{\textsf{initially, }pw = 0}
      \\  \multicolumn{2}{c}{\textsf{initially, }lk = \Alt<-2>{\False}{\True}}
      \\  \uncover<-2>{\color<2->{gray}{acquire lk}}
        &
      \\  pw := 1\hphantom{\texttt{alse}}\tikzPoint{T11}
        & \Alt<-3>{acquire lk}{\tikzPoint{T21}A := !lk}
      \\  \Alt<-3>{release lk}{lk := false\tikzPoint{T12}}
        & \tikzPoint{T22}B := !pw
      \\
        & \uncover<-2>{\color<2->{gray}{release lk}}
    \end{tabular}}%
  \end{center}%
  \begin{tikzpicture}[remember picture, overlay, ->, ultra thick]
    \only<6>  \draw[blue] (T11) -- (T12) -- (T21) -- (T22);
    \only<7>  \draw[blue] (T21) -- (T22) -- (T11) -- (T12);
    \only<8>  \draw[blue] (T11) -- (T21) -- (T12) -- (T22);
    \only<9>  \draw[blue] (T21) -- (T11) -- (T22) -- (T12);
    \only<10> \draw[blue] (T11) -- (T21) -- (T22) -- (T12);
    \only<11> \draw[blue] (T21) -- (T11) -- (T12) -- (T22);
    %\only<12->\draw[red]  (T12) -- (T21) -- (T22) -- (T11);
  \end{tikzpicture}%
  \pause
  \pause
  \pause
  \pause
  %\ebr
  Possible (A, B): \quad
  %\begin{center}
    \textcolor<8-11>{blue}{(\True, 1)},
    \textcolor<7>{blue}{(\True, 0)},
    \textcolor<6>{blue}{(\False, 1)}%
    \uncover<13->{, \textcolor{red}{(\False, 0)}}
  %\end{center}
  \pause
  \br
  %Sequential consistency%
  Traditional model of concurrency%
  \alt<-12>{: \textcolor{blue}{interleavings}}{\ \textcolor{red}{violated}}
  \br
  \uncover<14->{%
  %\color{red}
  %Reasons:
  \begin{itemize}%
    \color{red}
    \item hardware optimizations (e.g.\ buffering writes)
    \item compiler optimizations (e.g.\ reordering independent writes)
  \end{itemize}%
  }
\end{frame}



\begin{frame}%
  \frametitle{The essence of weak memory: subjectivity}
  Weak memory:
  each thread has its own \strong{view} of memory
  \slidebib{Dolan et al}
  \br
  In Cosmo:
  \slidebib{ICFP~2020}
  \br
  Some assertions are \strong{subjective}:
  they depend on the thread’s view
    \begin{itemize}%
      \item example: $\pointsto {pw} {\texttt{"azerty"}}$
    \end{itemize}%
  \ebr
  %What breaks in the previous proof?
  %\br
  \pause
  Invariants are \strong{objective}:
  \\  \quad because they are available to all threads
  \\  \quad they cannot share subjective assertions
  \pause
  \br
  The lock’s resource~$R$ might be subjective,
  \\ so cannot be put in an invariant as we did
\end{frame}%



\begin{frame}%
  \frametitle{Synchronization through atomic references}%
  The left thread must transmit its view to the right thread
  \\ $\Longrightarrow$ need for a \emph{synchronization} mechanism
  \pause
  \br
  In \mocaml:
  \strong{atomic references}
  \slidebib{Dolan et al}
  \br
  \begin{center}%
    \texttt{\begin{tabular}{l||l}%
      \multicolumn{2}{c}{\textsf{initially, }pw := 0} \\
      \multicolumn{2}{c}{\textsf{initially, }lk :=\{at\} \True} \\
      pw := 1                 &  A := !\{at\} lk  \\
      lk :=\{at\} \False  &  B := !pw
    \end{tabular}}%
  \end{center}%
  \pause
  Specification of atomic references?
\end{frame}%



%\begin{frame}%
%  \frametitle{Key idea: modeling subjectivity with views}%
%  Materialize views as mathematical objects:
%  \br
%  A thread knows a subset~$\vv$ of all writes to the memory
%  \begin{itemize}%
%    \item Affects how the thread interacts with memory
%    \item $\vv$ is the thread’s current \strong{view}
%    \item Lattice structure: the larger, the more up-to-date
%  \end{itemize}%
%  \ebr
%  New assertions:
%  \begin{itemize}%
%    \item
%      \(\alert<1>{\seen\vv}\)
%      :
%      %the thread’s view contains~$\vv$.
%      %the thread knows about all writes in~$\vv$.
%      %the thread has seen~$\vv$, i.e.\ it knows about all writes in~$\vv$.
%      %we \emph{have seen}~$\vv$, i.e.\ we know all writes in~$\vv$.
%      “the ambient view contains $\vv$\,”
%      \\ i.e.\ we know all writes in~$\vv$
%    \item
%      \(\alert<1>{\prop \opat \vv}\)
%      :
%      %$\prop$ holds in the eyes of any thread which has seen~$\vv$.
%      %$\prop$ holds for any thread which has seen~$\vv$.
%      %$\prop$ holds objectively for any thread which has seen~$\vv$.
%      %$\prop$ holds \emph{objectively} if we have seen~$\vv$.
%      %$\prop$ holds as long as the thread has seen~$\vv$.
%      %$\prop$ holds as long as the memory is seen with view~$\vv$.
%      %having seen~$\vv$ is \emph{objectively} enough for $\prop$ to hold.
%      “$\prop$ where the ambient view has been fixed to $\vv$”
%  \end{itemize}%
%\end{frame}%



%\begin{frame}%
%  \frametitle{Key idea: modeling subjectivity with views (bis)}%
%  %$\view, \viewB, …$
%  %\br
%  %views form a lattice (larger = more up-to-date)
%  a lattice of views (larger = more up-to-date)
%  \pause
%  \br
%  new assertions:
%  \begin{itemize}%
%    \item[\alert<5>{$\seen\view$}~]
%      “the ambient view contains $\view$\,”
%      $\Longrightarrow$ subjective
%      \\  \uncover<5->{\alert<5>{transferred via thread synchronization}}
%    \item[\alert<4>{$\prop \opat \view$}~]
%      “$\prop$ where the ambient view has been fixed to $\view$”
%      $\Longrightarrow$ objective
%      \\  \uncover<4->{\alert<4>{shareable via an invariant}}
%  \end{itemize}%
%  \pause
%  \ebr
%  splitting rule:
%  \[
%    \prop
%    \iequiv
%    \Exists \view.(
%      \alert<5>{\seen \view}  \isep
%      \alert<4>{\prop \opat \view}
%    )
%  \]
%\end{frame}%



%\begin{frame}%
%  \frametitle{Key idea: decomposing subjective assertions}
%  Decompose subjective assertions:
%  (\textbf{Splitting Rule})
%  \begin{center} \(
%    \prop \Longleftrightarrow
%    \exists \vv.~\underbrace{\prop \opat \vv}_\text{objective}
%    \;\isep\!\!\! \underbrace{\seen \vv}_\text{subjective}
%  \) \end{center}
%  Share parts via distinct mechanisms:
%  \begin{itemize}%
%    \item
%      \(\prop \opat \vv\)
%      :
%      via an objective \emph{invariant}, as before
%    \item
%      \(\seen\vv\)
%      :
%      %via \strong{``atomic'' accesses}.
%      %via synchronization primitives of the language.
%      via \emph{synchronization} offered by the memory model
%  \end{itemize}%
%  %\pause
%  %Remark: we also have:
%  %\begin{center} \(
%  %  \prop \opat \vv
%  %  \Alt<2>{\>{}\Longrightarrow{}}{{}\Longleftrightarrow{}}
%  %  {\color<2>{white}\objectively ({\color{black}\seen \vv \wand \prop})}
%  %\) \end{center}
%  %\pause
%  %where \(
%  %  \objectively \propB
%  %  \Longleftrightarrow
%  %  (\forall \vv.~\propB \opat \vv)
%  %  \Longleftrightarrow
%  %  \propB \opat \emptyview
%  %\)
%\end{frame}%



%\sectionframe{Our program logic}



%\begin{frame}
%  \frametitle{Features of the logic}
%  Inherited from Iris:
%  \begin{itemize}
%    \item affine separation logic
%    \item \strong{objective} invariants
%    \item Iris-style ghost state
%  \end{itemize}
%  We add:
%  \begin{itemize}
%    \item views
%    \item ownership assertion $\pointstoAT x {(v, \vv)}$ (objective)
%    \item ownership assertion $\pointstoNA x v$ (subjective)
%    \item weakest preconditions
%  \end{itemize}
%\end{frame}



%\begin{frame}
%  \frametitle{Rules of atomic locations, simplified}
%  $\pointstoAT x v$
%  :
%  the \emph{atomic} location~$x$ \strong{stores} the value~$v$.
%  \begin{itemize}%
%    \item \strong{Sequentially consistent}.
%    \item \strong{Objective}.
%    \item Standard rules:
%  \end{itemize}%
%  \begin{columns}
%    \begin{column}{0.3\textwidth}
%      %\emph{Atomic write:}
%      \[
%        \hoareV%
%          {\pointstoAT x v}%
%          {x \WriteAT{} v'}%
%          {\lambda ().~\pointstoAT x {v'}}%
%      \]
%    \end{column}
%    \begin{column}{0.5\textwidth}
%      %\emph{Atomic read:}
%      \[
%        \hoareV%
%          {\pointstoAT x v}%
%          {\ReadAT x}%
%          {\lambda v'.~v' = v \isep \pointstoAT x v}%
%      \]
%    \end{column}
%  \end{columns}
%\end{frame}%LOL



%\begin{frame}
%  \frametitle{Rules of non-atomic locations}
%  $\pointstoNA x v$
%  :
%  \strong{we know} the latest value~$v$ of the \emph{non-atomic} location~$x$.
%  \begin{itemize}%
%    \item \strong{Relaxed}.
%    \item \strong{Subjective} --- cannot appear in an invariant.
%    \item Standard rules too!
%  \end{itemize}%
%  \begin{columns}
%    \begin{column}{0.3\textwidth}
%      %\emph{Non-atomic write:}
%      \[
%        \hoareV%
%          {\pointstoNA x v}%
%          {\WriteNA x v'}%
%          {\lambda ().~\pointstoNA x {v'}}%
%      \]
%    \end{column}
%    \begin{column}{0.5\textwidth}
%      %\emph{Non-atomic read:}
%      \[
%        \hoareV%
%          {\pointstoNA x v}%
%          {\ReadNA x}%
%          {\lambda v'.~v' = v \isep \pointstoNA x v}%
%      \]
%    \end{column}
%  \end{columns}
%\end{frame}



%\begin{frame}%
%  \frametitle{Example: transferring an assertion through a spin lock}
%  \begin{columns}
%    \begin{column}{0.1\textwidth}
%    \end{column}
%    \begin{column}{0.3\textwidth}
%      \texttt{\begin{tabular}{l}%
%        \uncover<2->{\hoareProp{\prop}} \\
%        \uncover<4->{\hoareProp{\exists \vv.~%
%          \alert<5>{\prop \opat \vv} \isep \alert<6->{\seen \vv}
%        }} \\
%        \uncover<1->{\codecomment{release lock:}} \\
%        \uncover<1->{lock \WriteAT{}\tikzPoint{SpinRel} \False} \\
%        \\
%        \\
%        \\
%        \\
%        \\
%        \\
%      \end{tabular}}%
%    \end{column}
%    \vrule\,\vrule
%    \begin{column}{0.6\textwidth}
%      \texttt{\begin{tabular}{l}%
%        \\
%        \\
%        \\
%        \\
%        \uncover<1->{\codecomment{acquire lock:}} \\
%        \uncover<-2>{\codekw{while} }%
%        \tikzPoint{SpinAcq}%
%        \uncover<1->{\CAS{lock}{\False}{\True}}%
%        \uncover<-2>{\ = \False} \\
%        \only<-2>{\codekw{do} () \codekw{done}}%
%        \uncover<3->{\codecomment{CAS succeeds}} \\
%        \uncover<4->{\hoareProp{\exists \vv.~%
%          \alert<5>{\prop \opat \vv} \isep \alert<6->{\seen \vv}
%        }} \\
%        \uncover<2->{\hoareProp{\prop}} \\
%      \end{tabular}}%
%    \end{column}
%  \end{columns}
%  \begin{tikzpicture}[remember picture, overlay, ->, >=stealth', ultra thick, red]%
%    \only<7-> \draw (SpinRel) to[out=-60,in=175] node[midway,below=2mm,left=0mm,fill=white,inner sep=0,outer sep=0] {happens before} (SpinAcq);
%  \end{tikzpicture}%
%  \begin{itemize}%
%    \item<5>
%      \(\prop \opat \vv\)
%      :
%      transferred via \emph{objective invariants}, as usual.
%    \item<6->
%      \(\seen\vv\)
%      :
%      transferred via \alt<6>
%        {synchronization}
%        {\strong{``atomic'' accesses}}.
%  \end{itemize}%
%\end{frame}



\begin{frame}<2->
  \frametitle{Atomic references\only<1>{, simplified}}
  Semantics of an atomic reference: \slidebib{Dolan et al}
  \begin{enumerate}%
    \item stores a value on which all threads agree at any point in time
    \item<2-> achieves \strong{release/acquire} synchronization
  \end{enumerate}%
  \pause
  \pause
  In Cosmo: \slidebib{ICFP~2020}
  \begin{itemize}%
    \item<3->
      $\pointstoAT x {\RevealViewAT<2->{v}{\vv}}$
      \\ \quad ``$x$ stores the value~$v$
      \uncover<2->{\strong{and a view} (at least)~$\vv$}''
    \item<3-> this assertion is objective
    \item<3-> reasoning rules (sample):
  \end{itemize}%
  \uncover<3->{%
      \[
      \quad
        % atomic read:
        \hoareV%
          {%
            \pointstoAT x {\revealViewAT<2->{v}{\vv}}
            \only<2->{{}\isep \alert{\seen \vv'%
              \tikzPoint{ReleaseFrom}%
            }}
          }%
          {\WriteAT x v'}%
          {%\lambda ().~%
            \pointstoAT x {\revealViewAT<2->{v'}{\alert{\vv'%
              \tikzPoint{ReleaseTo}%
            }}}
          }%
      %\quad
      %  % atomic read:
      %  \hoareV%
      %    {\pointstoAT x {\revealViewAT<2->{v}{\alert\vv%
      %        \tikzPoint{AcquireFrom}%
      %    }}}%
      %    {\ReadAT x}%
      %    {\lambda v'.~%
      %      v' = v
      %      \isep \pointstoAT x {\revealViewAT<2->{v}{\vv}}
      %      \only<2->{{}\isep \tikzPoint{AcquireTo}%
      %        \alert{\seen \vv}%
      %      }
      %    }%
      \qquad
        % successful CAS:
        \hoareV%
          {\pointstoAT x {\revealViewAT<2->{v_1}{\alert\vv%
              \tikzPoint{CASFrom}%
          }}}%
          {\CAS x {v_1} {v_2}}%
          {%\lambda b.~b = \True
            ret = \True
            \isep \pointstoAT x {\revealViewAT<2->{v_2}{\vv}}
            \only<2->{{}\isep \tikzPoint{CASTo}%
              \alert{\seen \vv}%
            }
          }%
      \]
  }
%
  \MzAbsoluteBubbles{%
    \MzAbsoluteRawArrowLessBubble{85mm}{47mm}{55mm}{views}{%
      views are mathematical objects;
      \\ they enjoy a lattice structure
    }{4}
    %
    \MzAbsoluteRawArrowLessBubble{85mm}{28mm}{55mm}{current view}{%
      $\seen\vv$ : ``the current view contains $\vv$''
    }{6-}
  }
%
  \begin{tikzpicture}[remember picture, overlay, ->, >=stealth', ultra thick, red]%
    \only<7> \draw (ReleaseFrom) to[out=-60,in=30]  node[midway,right] {\textbf{release}} (ReleaseTo);
    \only<8> \draw (CASFrom) to[out=-60,in=135] node[midway,above] {\textbf{acquire}} (CASTo);
  \end{tikzpicture}%
\end{frame}



%\sectionframe{Case study: the spin lock}



\begin{frame}[fragile]
  \frametitle{The spin lock in Multicore OCaml}
  A spin lock implements a lock using an \emph{atomic} Boolean reference:
  \begin{columns}[T]
    \begin{column}{0.1\textwidth}
    \end{column}
    \begin{column}{0.4\textwidth}
\begin{lstlisting}[language=camleff]
let release lk =
  lk :={at} false
\end{lstlisting}
    \end{column}
    \begin{column}{0.5\textwidth}
%\begin{lstlisting}[language=camleff]
%let rec acquire lk =
%  if CAS lk false true
%  then ()
%  else acquire lk
%\end{lstlisting}
\begin{lstlisting}[language=camleff]
let try_acquire lk =
  CAS lk false true
\end{lstlisting}
    \end{column}
  \end{columns}
  Described by this assertion:
  %\[
  %  \textsf{isLock}~lk~R
  %  \quad\eqdef\quad
  %  \pointstoAT {lk} \True
  %  \qquad\lor\qquad \left(
  %    \uncover<3->{\alert{\exists \vv.~}}
  %    \pointstoAT {lk} {\RevealViewAT<3->{\False}{\alert\vv}}
  %    \isep R \only<3->{\alert{{}\opat\vv}}
  %  \right)
  %\]
  \[
    \textsf{isLock}~lk~R
    \quad\eqdef\quad
    \Exists b \uncover<3->{, \alert\vv}.~%
    \pointstoAT {lk} {\RevealViewAT<3->{b}{\alert\vv}}
    \ISEP (b = \False \Rightarrow R \uncover<3->{\alert{{}\opat\vv}})
  \]
  \uncover<2->{$R$ may be subjective!}
%
  \MzAbsoluteBubbles{%
    \MzAbsoluteRawArrowLessBubble{40mm}{-5mm}{75mm}{objective part of an assertion}{%
      $R \opat \vv$ :
      ``$R$ where the current view is fixed to $\vv$''
      %``$R$ holds in the eyes of whoever has (at least) the view $\vv$''
      %``if my view contains $\vv$ then I have $R$''
    }{3-}
  }
%
  %\begin{tikzpicture}[remember picture, overlay]%
  %  %\node<4> [cloud, cloud ignores aspect, cloud puffs=17,
  %  \node<4> [starburst, starburst points=17, starburst point height=7.5mm,
  %            fill=white, draw=orange, line width=2pt, rotate=-20, align=center]
  %    at ($(current page.south east)+(-30mm,20mm)$)
  %    {\strong{\!\!\!\!\!\!Sprinkle views over\!\!\!\!\!\!} \\
  %     \strong{\!\!\!\!\!\!your invariants!\!\!\!\!\!\!} };
  %\end{tikzpicture}%
\end{frame}



\begin{frame}%
  \frametitle{Verifying the spin lock in Multicore OCaml}%
  \vspace{-7mm}%
  \[
    \textsf{isLock}~lk~R
    \quad\eqdef\quad
    \Exists b, \vv.~%
    \pointstoAT {lk} (b,\vv) \ISEP (b = \False \Rightarrow R \opat \vv)
  \]%
  \vspace{-4mm}%
  \begin{columns}[T]
    \hspace{-5mm}
    \begin{column}{0.46\textwidth}
      \(\begin{codeblock}[]%
        \codecomment{release:} \\
        \hoareProp{\hphantom{\exists\vv.~}\textsf{isLock}~lk~R \isep R} \\
        \hoareProp{\hphantom{\exists\vv.~}\pointstoAT {lk} \_ \quad\:\!\isep R} \\
        \!\hoareProp{\!\!\:\exists\vv.~\pointstoAT {lk} \_
          \quad\:\!\isep \overbrace{\seen \vv \isep R \opat \vv}\!} \\
        \WriteAT {lk} \False \\
        \hoareProp{\hphantom{\exists\vv.~}\pointstoAT {lk} (\False,\vv) \isep R \opat \vv} \\
        \hoareProp{\hphantom{\exists\vv.~}\textsf{isLock}~lk~R} \\
      \end{codeblock}\)%
    \end{column}%
    %\vrule%
    \begin{column}{0.54\textwidth}
      \(\begin{codeblock}[]%
        \codecomment{try\_acquire:} \\
        \hoareProp{\textsf{isLock}~lk~R} \\
        \hoareProp{%
          \pointstoAT {lk} (b,\vv) \isep (b = \False \Rightarrow R \opat \vv)
        } \\
        \CAS{lk}{\False}{\True} \\
        \!\hoareProp{\!\ITE{ret}
          {\pointstoAT {lk} (\True, \vv) \isep \underbrace{\seen \vv \isep R \opat \vv}}
          {…}
        \!} \\
        \!\hoareProp{\!
          \ITE{ret}
            {\textsf{isLock}~lk~R \ISEP R}
            {…}
        \!} \\
        \hoareProp{%
          \textsf{isLock}~lk~R
          \ISEP (ret = \True \Rightarrow R)
        } \\
      \end{codeblock}\)%
    \end{column}
  \end{columns}
%
  \MzAbsoluteBubbles{%
    \MzAbsoluteRawArrowLessBubble{20mm}{10mm}{50mm}{splitting rule}{%
      \vspace{-7mm}%
      \[
        \prop \Longleftrightarrow
        \exists \vv.~\underbrace{\prop \opat \vv}_\text{objective}
        \;\isep\!\!\! \underbrace{\seen \vv}_\text{subjective}
      \]%
      \vspace{-3mm}%
    }{1-}
  }
\end{frame}%



\begin{frame}%
  \frametitle{Methodology}%
  A method for proving correctness under weak memory:%
  \begin{enumerate}%
    \item Start with the invariant under sequential consistency;
    \item Identify how information flows between threads;
      \begin{itemize}%
        \item i.e.\ where are the synchronization points;
      \end{itemize}%
    \item Refine the invariant with corresponding views.
  \end{enumerate}%
\end{frame}%
