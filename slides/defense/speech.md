# TITLE

Hello! and thank you for coming, some of you from overseas, thank you for
joining me in the defense of my PhD. The subject is: a concurrent separation
logic for the weak memory model of MOCaml.

Hopefully, by the end of this presentation you should be able to understand
most of these words. For now, let’s explain what weak memory is, with a small
analogy.

# 1. MOVIX

Let’s consider Family Cosmo. Family Cosmo loves watching series. Family Cosmo
has an account of that very big video-on-demand company… Movix.

Here is Alice Cosmo, and here is Bob Cosmo, watching movies in their respective
rooms.

On the other hand, Movix doesn’t like that a given account is used by several
persons at the same time. It doesn’t like it at all. So, if an account is
accessed from two distinct computers at the same time, Movix cancels the
account.

Luckily, Family Cosmo knows better. Family Cosmo has a technique to prevent that
situation from happening. They have a totem in the living room, that any family
member can borrow momentarily. And this totem grants the right to watch Movix:
it’s only when you own the totem, that you are allowed to watch Movix.

# 2: MOVIX MUTUAL EXCL

This achieves what we call mutual exclusion: because the totem is unique, not
two persons can own the totem at the same time, so the account is never accessed
simultaneously.

And that works great.

# 3. MOVIX PASSWORD

… Well, until someone changes the password.

Movix says: you should change your password from time to time. And that led
Family Cosmo to a terrible situation just last month. One morning, Bob saw the
totem in the living room, he took it, he tried to log in, and then… The family’s
IP address got blocked by Movix.

Why? Because the day before, Alice had changed the password, but didn’t met Bob
to tell him. So Bob had the wrong password. And it turns out Movix has a drastic
security measure: if you enter a wrong password, they block you.

# 4. MOVIX VIEWS

To recap, Alice and Bob SHARE something, they share a Movix account, for which
they need mutual exclusion, but that’s not enough:

The key idea is that Alice and Bob also have diverging VIEWS on what the state
of their shared resource is; their view being the last password THEY PERSONALLY
remember.

Alice is the last person who used Movix so has the most up-to-date view, so she
MUST transmit her knowledge to the next person to watch Movix---whoever that is.

<!-- But she can’t rely on meeting with Bob, because he might be busy, he might
be asleep, he might be away… -->

Then, a simple method to achieve just that is to write the password on the totem
before you return it. In other words, the totem transmits the view.

[5:00 / 6:00]

# 5. WEAK MEM

<!--
This analogy illustrates what weak-memory concurrency is.

CONCURRENCY is a situation in a computer where there are several… processors,
say, or several threads in a program, that run on a common time frame and have
access to a SHARED resource. And typically, the shared resource is memory.

In our analogy, the shared memory is the password, which at any time has some
value.
-->

Now, let’s look at computers. In a multicore computer we don’t have members of
the Cosmo family, but we have several processors. At the software level, a given
program may consist of several threads that run concurrently.

All the processors, all the threads act on a shared resource which is MEMORY.
But, as Alice and Bob Cosmo, they each have their OWN VIEW on what state the
shared memory is in. Some threads might be unaware of the latest modification.

This is what we call, a WEAK model of memory.

<!-- By "memory model", we mean a mathematical description of the behavior of
accesses to the shared memory: what you get when you read, what happens when you
try to modify, and so on. -->

And nowadays it is commonplace. Several modern programming languages strive to
take profit from multicore architectures, to offer concurrent programming, and
they do so with a weak memory model.

Among the most notable programming languages are C11 and Java. In both cases,
their memory model has been described formally (mathematically). And in this
thesis, I am interested in Multicore OCaml, which is what the name suggests: an
extension of the OCaml language with support for multicore programming. MOCaml
has been described in a paper by Dolan and coworkers, in 2018, the title is
"Bounding data races in space and time". So Dolan et al give a semantics to that
language, and in particular they describe its memory model, and it turns out
that it is a weak memory model.

So what I did with MOCaml in my thesis is designing a tool for verifying MOCaml
programs under the weak memory model. What does that mean and why would we want
to do that?

# 5. PRGM VERIF

As we all bitterly know, programming is error-prone. Just look at your phone.
There are so many bugs, so many mistakes that human programmers make. And not
all of these bugs are trivial.

Concurrency creates more opportunities for bugs; and for pretty subtle ones. And
weak memory leads to even more bugs, as we have seen with Bob typing the wrong
password.

Bugs are bad: they can have catastrophic consequences. Like, losing a paid
Movix account. Or, killing people. The Therac-25, that’s an actual example: it
was a radiotherapy machine that was used in Canada in the eighties. In certain
circumstances, because of a concurrency bug, the machine cast a high-energy beam
onto the patient without the adequate protection being set.

And of course there are many more examples, so the question is: how to trust
software? There are several approaches to improving confidence in software. The
approach I adopt here is that of functional correctness.

First, we specify the program, by which we mean that we state formally, in
mathematical terms, what the program is expected to do.

And that alone is already helpful, because the specification can serve as
documentation, and because the mere task of writing a specification incites us
to think straight about what our program should do.

But we can do much more: second, we verify the program against this
specification. That is, we prove mathematically that what the program really
does matches what we want it to do.

# 6. AIM, CONTRIB

So my aim in this thesis was to allow verification of fine-grained concurrent
programs in the setting of the weak memory model of MOCaml.

Towards this goal, I have developed a program logic for MOCaml, that is to say,
a formal proof system that allows specifying and verifying programs.

Then, to demonstrate the usability of my program logic, whose nickname is Cosmo,
I have run several case studies: specifying and verifying various concurrent
datastructures. Various kinds of locks, and also, a non-trivial concurrent
queue.

Perhaps I should mention that these contributions appeared in two papers,
published at ICFP.

The remainder of this presentation is structured as follows:

- First I will explain how to verify concurrent programs, when not in
  a weak-memory setting; and that’s not my contribution, that’s the state of the
  art; or existing knowledge, at least.
- Then we will enter the world of weak memory, and I will present Cosmo.
- And finally, to illustrate the use of Cosmo, I will touch a word on writing
  a specification for the concurrent queue that I mentioned earlier.

[11:30]

# 7. HOARE, SEPLOG

I said, we want to specify programs. How to do that? A traditional way of
writing a formal specification, since at least the seventies, is the following:

<!-- We have seen that, with weak memory, mutual exclusion is not enough: you
also need synchronization, by which I mean that the participants need to
communicate their knowledge about the shared resource. -->

...
