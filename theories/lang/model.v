From Coq Require Export ssreflect QArith.Qcanon.
From stdpp Require Export prelude gmap numbers.
From cosmo.base Require Export base lattice_rational.
Set Default Proof Using "Type".

(*
   ICFP20: In this file we define the memory model, independently of the
   programming language. This corresponds to Section 2.1,
   Figure 1 “Semantic objects”
   and Figure 2 “Operational behavior of the memory subsystem”.
*)

(** Locations. **)

(* Any (countably) infinite set will do. *)
Notation location := positive.
Notation offset := Z (only parsing).
Record locoff : Type := Locoff {
  locoff_loc : location ;
  locoff_off : offset ;
}.

Definition locoff_to_pair '(Locoff ℓ i) := (ℓ, i).
Definition pair_to_locoff '(ℓ, i) := Locoff ℓ i.
Instance pair_to_locoff_inj : Inj eq eq pair_to_locoff.
  Proof. by intros [] [] [= -> ->]. Qed.
Instance locoff_to_pair_inj : Inj eq eq locoff_to_pair.
  Proof. by intros [] [] [= -> ->]. Qed.
Instance locoff_eq_dec : EqDecision locoff.
  Proof. eapply (inj_eq_dec locoff_to_pair). Defined.
Instance locoff_countable : Countable locoff.
  Proof. apply (inj_countable' locoff_to_pair pair_to_locoff). by intros []. Qed.
Definition location_to_locoff ℓ := Locoff ℓ 0.

Coercion location_to_locoff : location >-> locoff.
Notation "ℓ .[ i ]" := (Locoff ℓ i) (at level 9, format "ℓ .[ i ]").

(** Timestamps. **)

(* A timestamp is either 0 (None) or a positive rational number q (Some q). *)
Notation timestamp := (option Qp).

(** Views. **)

(* A view is a (partial) mapping from (location,offset) pairs to non-zero timestamps.
   Locations which are not bound by this mapping are implicitly bound to the
   timestamp 0. *)
Notation view := (gmap locoff Qp).

Definition view_Lat : latticeT := gmap_Lat locoff Qp_Lat.

Section operational.
  Context {val expr : Type}.

  (** Histories. **)

  Definition history : Type := gmap timestamp val.

  (** Stores. **)

  (* A store element is either the contents of an atomic location (store_elt_at)
     or the contents of a non-atomic location (store_elt_na). *)
  Inductive store_elt : Type :=
    | store_elt_at (v : val) (V : view)
    | store_elt_na (h : history).
  Definition store : Type := gmap location (list store_elt).

  (* A store is essentially a map from locations and offsets to stored elements,
   * with the invariant that the set of offsets for a given location is an
   * interval starting at zero. The keys of this map are curried and we use a
   * list, so that the invariant is enforced automatically and the length of a
   * block is easily computed. However we also provide uncurried accessors. *)
  Global Instance store_lookup : Lookup locoff store_elt store :=
    λ '(Locoff ℓ i) σ,
      if bool_decide (0 ≤ i)%Z then
        σ !! ℓ ≫= λ (blk : list _), blk !! Z.to_nat i
      else None.
  Global Instance store_insert : Insert locoff store_elt store :=
    λ '(Locoff ℓ i) s σ,
      if bool_decide (0 ≤ i)%Z then
        match σ !! ℓ with
        | None     => σ
        | Some blk => <[ℓ := <[Z.to_nat i := s]> blk]> σ
        end
      else σ.

  Lemma store_lookup_eq (σ : store) (ℓ : location) (i : offset) :
    σ !! ℓ.[i] =
      if bool_decide (0 ≤ i)%Z then
        σ !! ℓ ≫= λ (blk : list _), blk !! Z.to_nat i
      else None.
  Proof. reflexivity. Qed.

  Lemma store_lookup_insert (σ : store) (ℓi : locoff) (s0 s : store_elt) :
    σ !! ℓi = Some s0 →
    <[ℓi := s]> σ !! ℓi = Some s.
  Proof.
    destruct ℓi as [ℓ i]. rewrite /lookup /insert /=.
    case_bool_decide ; last done.
    case_match ; last done. cbn=>?.
    rewrite lookup_insert /= list_lookup_insert//. by eapply lookup_lt_Some.
  Qed.

  Lemma store_lookup_insert_ne (σ : store) (ℓi ℓi' : locoff) (s : store_elt) :
    ℓi ≠ ℓi' →
    <[ℓi := s]> σ !! ℓi' = σ !! ℓi'.
  Proof.
    destruct ℓi as [ℓ i], ℓi' as [ℓ' i']. rewrite /lookup /insert /=.
    intros ?. case_bool_decide ; last done.
    case_bool_decide ; last done.
    destruct (σ !! ℓ) as [blk|] eqn:Hσℓ ; last done.
    destruct (decide (ℓ = ℓ')) as [<-|].
    - destruct (decide (i = i')) as [<-|] ; first done.
      assert (Z.to_nat i ≠ Z.to_nat i') by lia.
      by rewrite Hσℓ lookup_insert /= list_lookup_insert_ne.
    - by rewrite lookup_insert_ne.
  Qed.

  (** Access modes. **)

  Inductive atomicity := Atomic | NonAtomic.

  Global Instance atomicity_eq_dec : EqDecision atomicity.
  Proof. solve_decision. Defined.
  Global Instance atomicity_countable : Countable atomicity.
  Proof.
    refine (inj_countable' (λ a, bool_decide (a = Atomic))
                           (λ b, if b then Atomic else NonAtomic) _).
    by intros [].
  Qed.

  (** Memory semantics. **)

  Implicit Types (v : val) (e : expr) (ℓ : location) (ℓi : locoff) (n : Z).
  Implicit Types (t : timestamp) (V Vℓ : view) (h : history) (blk : list store_elt) (σ : store).
  Implicit Types (α : atomicity).

  (*
     ICFP20: Below is the memory semantics as described in Figure 2
     “Operational behavior of the memory subsystem”. Transitions are labelled
     by memory events, whose definition follows.
     
     The proposition “mem_step σ W me σ' W'” is what the paper denotes as
     “σ;W →^m σ';W'” in the paper. It means that the memory can transition
     from the global state σ and thread-local view W, to the global state σ'
     and thread-local view W', under the action of the memory event me.
     
     In reading the formulas below, here are a few notations:
         m!!x is the map lookup, what is denoted by m(x) in the paper. It
            returns None when x is unbound in m.
         <[x:=y]>m is the map insertion/update, what is denoted by m[x:=y]
             in the paper.
         {[None:=v]} is the singleton map which binds None (the timestamp 0)
             to the value v.
  *)

  Inductive mem_event : Type :=
    | mem_event_read  (α : atomicity) (ℓi : locoff) (v : val)
    | mem_event_write_na              (ℓi : locoff) (v : val)
    | mem_event_update                (ℓi : locoff) (v0 v1 : val)
    | mem_event_length                (ℓ : location) (n : Z)
    | mem_event_alloc (α : atomicity) (ℓ : location) (n : Z) (v : val).

  Inductive mem_step : store → view → mem_event → store → view → Prop :=
    | mem_step_read_na σ V ℓi h t t' v :
        σ !! ℓi = Some (store_elt_na h) →
        V !! ℓi = t →
        (t ⊑ t') →
        h !! t' = Some v →
        mem_step
          σ V
          (mem_event_read NonAtomic ℓi v)
          σ V
    | mem_step_write_na σ V ℓi h t (t' : Qp) v :
        σ !! ℓi = Some (store_elt_na h) →
        V !! ℓi = t →
         (* The operational model in Dolan et al.’s paper says t < t'. But this
            is equivalent to t ≤ t', because a write must have been
            performed at timestamp t. *)
        (t ⊑ Some t') →
        h !! Some t' = None →
        mem_step
          σ V
          (mem_event_write_na ℓi v)
          (<[ℓi := store_elt_na $ <[Some t' := v]> h]> σ)
          (<[ℓi := t']> V)
    | mem_step_read_at σ V ℓi Vℓ v :
        σ !! ℓi = Some (store_elt_at v Vℓ) →
        mem_step
          σ V
          (mem_event_read Atomic ℓi v)
          σ (Vℓ ⊔ V)
    | mem_step_update σ V ℓi Vℓ v0 v1 :
        σ !! ℓi = Some (store_elt_at v0 Vℓ) →
        mem_step
          σ V
          (mem_event_update ℓi v0 v1)
          (<[ℓi := store_elt_at v1 (Vℓ ⊔ V)]> σ) (Vℓ ⊔ V)
    | mem_step_length σ V ℓ blk n :
        σ !! ℓ = Some blk →
        Z.of_nat (length blk) = n →
        mem_step
          σ V
          (mem_event_length ℓ n)
          σ V
    | mem_step_alloc_na σ V ℓ n v :
        σ !! ℓ = None →
        (0 ≤ n)%Z →
        mem_step
          σ V
          (mem_event_alloc NonAtomic ℓ n v)
          (<[ℓ := replicate (Z.to_nat n) $ store_elt_na {[ None := v ]} ]> σ) V
    | mem_step_alloc_at σ V ℓ n v :
        σ !! ℓ = None →
        (0 ≤ n)%Z →
        mem_step
          σ V
          (mem_event_alloc Atomic ℓ n v)
          (<[ℓ := replicate (Z.to_nat n) $ store_elt_at v V]> σ) V
  .

End operational.
